/*image pyramid 를 4level로 만들고 각각의 image pyramid로부터 fast feature를 찾아낸다
이후 FREAK 를 이용하여 각각의 descriptor을 검색한다.*/
#ifndef IMAGE_DATA_H
#define IMAGE_DATA_H

#define WEBCAM 0

#include <iostream>
#include <algorithm>

#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\features2d\features2d.hpp"
#include "opencv2\xfeatures2d\include\opencv2\xfeatures2d.hpp"
#include "opencv2\xfeatures2d\include\opencv2\xfeatures2d\nonfree.hpp"

#include "DEFINE.h"


using namespace std;

class ImageData{
public:
	ImageData();
	~ImageData();
	void run(cv::Mat color, cv::Mat gray, cv::Mat depth, bool isTrack);//isTrack == 1일 경우 Fast 에서 nonMaxSuppre~ = false
	void Fast(bool isTrack);
	void Sift();
	void Brisk();	
	void setFastRGB();
	void setTrackCornerIdx(vector<int> worldIdx, vector<int> trackIdx, int resultPointSize);
	void setUsingPoint(vector<int> usingMatch, bool point2f);		//point2f == true  trackFastCorner 수정, false == trackFastCornerDepth수정
	void setUsingPoint(vector<int> usingMatch, vector<cv::DMatch> matches, bool track); //track == true using train,  track == false using query
	void setAddPoint(vector<int> nonUsingMatch);
	void setResultAddPoint(vector<int> resultAddPoint); 
	cv::Mat getDescriptor();
	cv::Mat getTrackDescriptor();
	cv::Mat getGrayMat();
	cv::Mat getDepthMat();
	cv::Mat getTrackFastCornerDepthMat();
	vector<int> getDescriptorIdx();
	vector<cv::KeyPoint> getfastCorner();
	vector<cv::Point2f> getTrackFastCorner();
	vector<cv::Point3f> getTrackFastCornerDepth();
	vector<cv::Point3i> getfastRGB();
	vector<cv::Point3i> getTrackRGB();
	vector<cv::Point2f> getAddPoint();
	vector<cv::Point3i> getAddRGB();
	cv::Mat getAddPointDepthMat();
	cv::Mat getAddDescriptor();
	cv::Mat getKeyPoint();
	cv::Mat getKeyPointDepth();
	vector<int> getKeyPointIdx();

	cv::Mat getPyrDescriptor();
	vector<cv::KeyPoint> getPyrfastCorner();
	void setPyrUsingPoint(vector<int> usingMatch, bool point2f);
	void setPyrUsingPoint(vector<int> usingMatch, vector<cv::DMatch> matches, bool track);
	vector<int> getPyrDescriptorIdx();
	
private:
	void init();
	void drawMatch();
	float getDepth(float row, float col);
	cv::Mat colorMat;
	cv::Mat GrayMat;
	cv::Mat DepthMat;

	vector<int> descriptorIdx;

	vector<cv::KeyPoint> fastCorner;	//처음 찾아지는 애들
	
	cv::Mat trackFastCornerDepthMat;
	vector<cv::Point3f> trackFastCornerDepth; //setUsingPoint 로 한번 거른 match 된 애들 bundle adjustment 할때도 이 set을 사용할거임.
	vector<cv::Point2f> trackFastCorner;
	vector<cv::Point3i> trackRGB;
	vector<int> track_world_CornerIdx[2];	//world에서의 track된 corner index (bundle adjustment 때문에 있어야됌)
											//0이 track 인덱스 1이 그에 해당하는 월드 인덱스
	cv::Mat trackDescriptor;
	int resultMapPointSize;

	vector<cv::Point2f> addPoint;
	cv::Mat addPointDepthMat;

	vector<cv::Point3i> addRGB;
	cv::Mat addDescriptor;

	cv::Mat keyPointMat;
	vector<int> keyPointIdx;

	vector<cv::Point3f> fastCornerDepth;		//매칭과 관련없이 모든 애들
	vector<cv::Point3i> fastRGB;
	cv::Mat descriptor;

	cv::Ptr<cv::BRISK> brisk;
	cv::Ptr<cv::xfeatures2d::SIFT> sift;
	cv::Ptr<cv::xfeatures2d::SURF> surf;


	cv::Mat pyrImage;
	vector<cv::KeyPoint> pyrFastCorner;
	cv::Mat pyrDescriptor;
	vector<int> pyrDescriptorIdx;
	vector<cv::Point2f> trackPyrFastCorner;
	vector<cv::Point3f> trackPyrFastCornerDepth;
	cv::Mat trackPyrDescriptor;

};
#endif