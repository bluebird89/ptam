#include "ImagePyramid.h"

GPyramid::GPyramid(){
}
#if !WEBCAM
void GPyramid::run(cv::Mat input, cv::Mat depth){
	cv::Mat inputGray;
	cv::Mat inputDepth;
	input.copyTo(inputGray);
	makeImagePyramid(inputGray);
	depth.copyTo(inputDepth);
	setDepthMat(inputDepth);
	Fast();
	Freak();
	//Brisk();
}
#endif
#if WEBCAM
void GPyramid::run(cv::Mat input){
	cv::Mat inputGray;
	input.copyTo(inputGray);
	makeImagePyramid(inputGray);
	Fast();
	Freak();
	//Brisk();
}
#endif
void GPyramid::setDepthMat(cv::Mat inputDepth){
	tempDepthMat[0] = inputDepth;
	for (int i = 0; i < 3; i++){
		cv::pyrDown(tempDepthMat[i], tempDepthMat[i + 1], cv::Size(tempDepthMat[i].cols / 2, tempDepthMat[i].rows / 2));
	}
}
void GPyramid::makeImagePyramid(cv::Mat input){
	//cout << "make Pyramind" << endl;
	tempGrayMat[0] = input;
	for (int i = 0; i < 3; i++){
		cv::pyrDown(tempGrayMat[i], tempGrayMat[i + 1], cv::Size(tempGrayMat[i].cols / 2, tempGrayMat[i].rows / 2));
	}
}
void GPyramid::Fast(){
	/*cout << "detect corner" << endl;
	double t = (double)cv::getTickCount();					//0.001 s*/
	for (int i = 0; i < 4; i++){
		cv::FASTX(tempGrayMat[i], fastCorner[i], 30, true, cv::FastFeatureDetector::TYPE_9_16);
	}
	/*t = ((double)cv::getTickCount() - t) / cv::getTickFrequency();
	cout << t << endl;*/
}
void GPyramid::Freak(){
	/*cout << "Freak" << endl;
	double t = (double)cv::getTickCount();					//0.001~4 s*/
	for (int i = 0; i < 4; i++){
		freak.compute(tempGrayMat[i], fastCorner[i], descriptor[i]);
	}
	/*t = ((double)cv::getTickCount() - t) / cv::getTickFrequency();
	cout << t << endl;*/
}
void GPyramid::Brisk(){
	for (int i = 0; i < 4; i++){
		brisk.compute(tempGrayMat[i], fastCorner[i], descriptor[i]);
	}
}
void GPyramid::setFastCorner(vector<cv::KeyPoint> setVector, int i){
	fastCorner[i] = setVector;
}
cv::Mat GPyramid::getGrayMat(int i){
	return tempGrayMat[i];
}
cv::Mat GPyramid::getDepthMat(int i){
	return tempDepthMat[i];
}
cv::Mat GPyramid::getDescriptor(int i){
	return descriptor[i];
}
vector<cv::KeyPoint> GPyramid::getfastCorner(int i){
	return fastCorner[i];
}