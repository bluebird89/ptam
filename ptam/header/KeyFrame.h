#ifndef KEY_FRAME_H
#define KEY_FRAME_H

#define SBA_MAX_REPROJ_ERROR    4.0 // max motion only reprojection error

#define BA_NONE                 -1
#define BA_MOTSTRUCT            0
#define BA_MOT                  1
#define BA_STRUCT               2
#define BA_MOT_MOTSTRUCT        3
#define FULLQUATSZ			    4

#define CLOCKS_PER_MSEC (CLOCKS_PER_SEC/1000.0)

#define MAXITER         100
#define MAXITER2        150

#define _MK_QUAT_FRM_VEC(q, v){                                     \
	(q)[1] = (v)[0]; (q)[2] = (v)[1]; (q)[3] = (v)[2];                      \
	(q)[0] = sqrt(1.0 - (q)[1] * (q)[1] - (q)[2] * (q)[2] - (q)[3] * (q)[3]);  \
}
#define MAXSTRLEN  2048 /* 2K */
/* get rid of the rest of a line upto \n or EOF */
#define SKIP_LINE(f){                                                       \
	char buf[MAXSTRLEN];                                                        \
while (!feof(f))                                                           \
if (!fgets(buf, MAXSTRLEN - 1, f) || buf[strlen(buf) - 1] == '\n') break;      \
}
#define NOCOV     0
#define FULLCOV   1
#define TRICOV    2

#include "opencv2\opencv.hpp"
#include "sba.h"

#include "DEFINE.h"

using namespace std;

class KeyFrame{
public:
	KeyFrame();
	~KeyFrame();
	void setImageData(cv::Mat inputR, cv::Mat inputT, cv::Mat inputCorner3D, cv::Mat inputCorner2D, vector<int> inputCornerIdx);
	int runSBA(); 
	void setCameraMat(cv::Mat);
	cv::Mat getSbaR(int colum);


	cv::Mat sbaCorner3D;
	cv::Mat sbaR;
	cv::Mat sbaT;


	cv::Mat corner3D;
private:
	int sba_driver(vector<cv::Mat> inputR, vector<cv::Mat> inputT, cv::Mat inputCorner3D, vector<cv::Mat> inputCorner2D,
		vector<vector<int>>inputCornerIdx, cv::Mat inputCameraMat, int cnp, int pnp, int mnp, int filecnp, char* refcamsfname, char* refptsfname);
	void readInitialSBAEstimate(vector<cv::Mat> inputR, vector<cv::Mat> inputT, cv::Mat inputCorner3D, vector<cv::Mat> inputCorner2D,
		vector<vector<int>>inputCornerIdx, cv::Mat inputCameraMat, int cnp, int pnp, int mnp, int filecnp, int* ncams, int* n3Dpts, int* n2Dprojs,
		double** moststruct, double** initrot, double** imgpts, double** covimgpts, char** vmask);
	void readPointParamsAndProjections(cv::Mat inputCorner3D, vector<cv::Mat> inputCorner2D, vector<vector<int>> inputCornerIdx, double* params,
		int pnp, double* projs, double* covprojs, int havecov, int mnp, char* vmask, int ncams);
	void readCameraParams(vector<cv::Mat> inputR, vector<cv::Mat> inputT, cv::Mat inputCameraMat, int cnp, double* params, double* initrot);
	void readNprojections(vector<vector<int>> inputCornerIdx, int* projs);
	void setSBAStructureData(double* motstruct, int ncams, int n3Dpts, int cnp);
	void setSBAMotionData(double* motstruct, int ncams, int cnp);
	int readNInts(vector<vector<int>> inputCornerIdx, int* vals, int* presentPoint, int* countFrame);
	int countNDoubles(vector<vector<int>> inputCornerIdx);
	int findNcameras(vector<vector<int>> inputCornerIdx);
	cv::Mat rvecTo3Quat(cv::Mat rvec);
	//void img_projsKRTS_jac_x(double* p, struct sba_crsm* idxij, int *rcidxs, int *rcsubs, double *jac, void *adata);
	//void img_projsKRTS_x(double *p, struct sba_crsm *idxij, int *rcidxs, int *rcsubs, double *hx, void *adata);
	/*void calcImgProjJacKRTS(double a[5], double qr0[4], double v[3], double t[3],
		double M[3], double jacmKRT[2][11], double jacmS[2][3]);
	void calcImgProjFullR(double a[5], double qr0[4], double t[3], double M[3], double n[2]);
	void quatMultFast(double q1[FULLQUATSZ], double q2[FULLQUATSZ], double p[FULLQUATSZ]);*/

	float* sbaRPointer;

	vector<cv::Mat> R;
	vector<cv::Mat> T;

	//cv::Mat corner3D;

	cv::Mat cameraMat;

	vector<cv::Mat> corner2D;
	vector<vector<int>> cornerIdx;
};

#endif


//void quat2vec(double *inp, int nin, double *outp, int nout)
//{
//	double mag, sg;
//	register int i;
//
//	/* intrinsics & distortion */
//	if (nin>7) // are they present?
//	for (i = 0; i<nin - 7; ++i)
//		outp[i] = inp[i];
//	else
//		i = 0;
//
//	/* rotation */
//	/* normalize and ensure that the quaternion's scalar component is non-negative;
//	* if not, negate the quaternion since two quaternions q and -q represent the
//	* same rotation
//	*/
//	mag = sqrt(inp[i] * inp[i] + inp[i + 1] * inp[i + 1] + inp[i + 2] * inp[i + 2] + inp[i + 3] * inp[i + 3]);
//	sg = (inp[i] >= 0.0) ? 1.0 : -1.0;
//	mag = sg / mag;
//	outp[i] = inp[i + 1] * mag;
//	outp[i + 1] = inp[i + 2] * mag;
//	outp[i + 2] = inp[i + 3] * mag;
//	i += 3;
//
//	/* translation*/
//	for (; i<nout; ++i)
//		outp[i] = inp[i + 1];
//}


//-4.78677 - 2.57599 3.33125
/*q[1] = -4.78677;  20.0585
q[2] = -2.57599;  6.63572
q[3] = 3.33125;   11.09723
q[0] = */