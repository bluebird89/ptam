#include "Map.h"

Map::Map(){
	initFrame = false;
	track = false;
	matchNum = 0;
#if WEBCAM
	cameraMat = *(cv::Mat_<float>(4, 4) <<
		541.0f, 0.0f, 327.0f, 0.0f,
		0.0f, 541.0f, 222.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);
	cv::invert(cameraMat, inverseCameraMat);
#endif
#if !WEBCAM
	cameraMat = *(cv::Mat_<float>(4, 4) <<
		518.4f, 0.0f, 312.9f, 0.0f,
		0.0f, 518.4f, 263.1f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);
	cv::invert(cameraMat, inverseCameraMat);
	calibR = *(cv::Mat_<float>(3, 3) <<
		1.0f, -0.02f, 0.014f,
		0.02f, 1.0f, -0.01f,
		-0.011f, 0.011f, 1.0f);
	calibT = *(cv::Mat_<float>(3, 1) <<
		0.0253f,
		0.00037f,
		-0.00252f);
#endif
}
Map::~Map(){
	initPyramid[0].~GPyramid();
	initPyramid[1].~GPyramid();
}
#if !WEBCAM
void Map::initMap(cv::Mat grayImage, cv::Mat depthImage){
	track = false;
	if (initFrame == false){
		initFrame = true;
		initPyramid[0].run(grayImage, depthImage);
	}
	else{
		initPyramid[1].run(grayImage, depthImage);
		double t = (double)cv::getTickCount();		//0.003~0.01s
		{
			int query = 0;
			int train = 0;
			matchNum = 0;
			for (int i = 0; i < 4; i++){
				if (initPyramid[0].getDescriptor(i).size>0 && initPyramid[1].getDescriptor(i).size > 0
					&& initPyramid[0].getfastCorner(i).size() > 0 && initPyramid[1].getfastCorner(i).size() > 0){
					matcher.match(initPyramid[0].getDescriptor(i), initPyramid[1].getDescriptor(i), matches[i]);

					vector <cv::KeyPoint> temp0 = initPyramid[0].getfastCorner(i);
					vector <cv::KeyPoint> temp1 = initPyramid[1].getfastCorner(i);
					vector <cv::KeyPoint> queryFastRest;

					int matchIdx[100] = { NULL };

					for (int k = 0; k < matches[i].size(); k++){
						query = matches[i][k].queryIdx;
						train = matches[i][k].trainIdx;
						if (abs(temp0[query].pt.y - temp1[train].pt.y) < 30.0f){
							trackSuccessFeature[0][i].push_back(cv::Point2f(temp0[query].pt.x, temp0[query].pt.y));
							trackSuccessFeature[1][i].push_back(cv::Point2f(temp1[train].pt.x, temp1[train].pt.y));

							queryFastRest.push_back(temp0[query]);
							matchNum++;

							track = true;
						}
					}
					initPyramid[0].setFastCorner(queryFastRest, i);
				}
			}
			initPyramid[0].Freak();
			if (matchNum < 30){
				initFrame = false;
			}
		}
		cv::imshow("check", initPyramid[0].getGrayMat(0));
	}
}
#endif
#if WEBCAM
void Map::initMap(cv::Mat grayImage){
	track = false;
	if (initFrame == false){
		initFrame = true;
		initPyramid[0].run(grayImage);
	}
	else{
		initPyramid[1].run(grayImage);
		/*vector<cv::Mat> freakimage;				//freak select pairs 해서 한거 너무느림 1~2초정도
		freakimage.push_back(initPyramid[0].getGrayMat(0));
		freakimage.push_back(initPyramid[1].getGrayMat(0));
		vector<vector<cv::KeyPoint>> keypoint;
		keypoint.push_back(initPyramid[0].getfastCorner(0));
		keypoint.push_back(initPyramid[1].getfastCorner(0));

		k.NB_SCALES;
		double t = (double)cv::getTickCount();
		k.selectPairs(freakimage, keypoint, 5, false);
		t = ((double)cv::getTickCount() - t) / cv::getTickFrequency();
		cout << "select Pairs  " << t << endl;*/



		double t = (double)cv::getTickCount();		//0.003~0.01s
		{
			int query = 0;
			int train = 0;
			matchNum = 0;
			for (int i = 0; i < 4; i++){
				if (initPyramid[0].getDescriptor(i).size>0 && initPyramid[1].getDescriptor(i).size > 0
					&& initPyramid[0].getfastCorner(i).size() > 0 && initPyramid[1].getfastCorner(i).size() > 0){
					matcher.match(initPyramid[0].getDescriptor(i), initPyramid[1].getDescriptor(i), matches[i]);

					vector <cv::KeyPoint> temp0 = initPyramid[0].getfastCorner(i);
					vector <cv::KeyPoint> temp1 = initPyramid[1].getfastCorner(i);
					vector <cv::KeyPoint> queryFastRest;
					int matchIdx[100] = { NULL };

					for (int k = 0; k < matches[i].size(); k++){
						query = matches[i][k].queryIdx;
						train = matches[i][k].trainIdx;
						if (abs(temp0[query].pt.y - temp1[train].pt.y) < 50.0f){
							trackSuccessFeature[0][i].push_back(cv::Point2f(temp0[query].pt.x, temp0[query].pt.y));
							trackSuccessFeature[1][i].push_back(cv::Point2f(temp1[train].pt.x, temp1[train].pt.y));

							queryFastRest.push_back(temp0[query]);
							matchNum++;

							track = true;
						}
					}
					initPyramid[0].setFastCorner(queryFastRest, i);
				}
			}
			initPyramid[0].Freak();
			if (matchNum < 30){
				initFrame = false;
			}
		}

		t = ((double)cv::getTickCount() - t) / cv::getTickFrequency();
		cout << t << endl;

		cv::imshow("check", initPyramid[0].getGrayMat(0));
	}
}
#endif
#if !WEBCAM
void Map::genMap(){
	for (int i = 0; i < 1; i++){				//일단 하나만하자.
		cv::Mat initPoint3f = cv::Mat(4, trackSuccessFeature[0][i].size(), CV_32F);					//여기까지 init point3f Mat 형태로 받아옴
		cv::Mat initDepthMat = initPyramid[0].getDepthMat(i);

		float temp = 0.0f;
		int depth = 0;
		cout << trackSuccessFeature[0][i].size() << endl;
		for (int j = 0; j < trackSuccessFeature[0][i].size(); j++){
			depth = getDepth(initDepthMat, trackSuccessFeature[0][i][j].y, trackSuccessFeature[0][i][j].x);
			//cout << "depth" << depth << endl;
			temp = 320.0f*(1.0f - depth/204.0f) + 80.0f;
			initPoint3f.at<float>(0, j) = trackSuccessFeature[0][i][j].x * temp;
			initPoint3f.at<float>(1, j) = trackSuccessFeature[0][i][j].y * temp;
			initPoint3f.at<float>(2, j) = temp;
			initPoint3f.at<float>(3, j) = 1;
		}

		cv::Mat trackPoint3f = cv::Mat(4, trackSuccessFeature[1][i].size(), CV_32F);					//여기까지 track point3f Mat 형태로 받아옴
		cv::Mat trackDepthMat = initPyramid[1].getDepthMat(i);

		for (int j = 0; j < trackSuccessFeature[1][i].size(); j++){
			depth = getDepth(trackDepthMat, trackSuccessFeature[1][i][j].y, trackSuccessFeature[1][i][j].x);
			//cout << "depth" << depth << endl;
			temp = 320.0f*(1.0f - depth / 204.0f) + 80.0f;
			trackPoint3f.at<float>(0, j) = trackSuccessFeature[1][i][j].x * temp;
			trackPoint3f.at<float>(1, j) = trackSuccessFeature[1][i][j].y * temp;
			trackPoint3f.at<float>(2, j) = temp;
			trackPoint3f.at<float>(3, j) = 1;
		}
		
		cv::Mat worldPoint3f = inverseCameraMat * initPoint3f;
		result = worldPoint3f;
		cv::Mat addPoint3f = inverseCameraMat * trackPoint3f;

		cv::Mat inverseAddPoint3f;

		for (int j = 0; j < trackSuccessFeature[0][i].size(); j++)
			cv::circle(initDepthMat, trackSuccessFeature[0][i][j],3, cv::Scalar(0, 0, 255), 2);
		cv::imshow("aa", initDepthMat);
		/*cv::invert(addPoint3f, inverseAddPoint3f, CV_SVD);

		cv::Mat RT = worldPoint3f * inverseAddPoint3f;

		cout << "RT" << endl;
		for(int i = 0; i < 4; i++){
			for( int j = 0; j < 4; j++){
				cout << RT.at<float>(i, j) << "  ";
			}
			cout << endl;
		}*/
		/*cout << "worldPoint" << endl;
		cout << worldPoint3f << endl;
		cout << "addPoint" << endl;
		cout << addPoint3f << endl;*/
		for (int k = 0; k < 10; k++){
			calculateCoarseCamPose(worldPoint3f, addPoint3f);
		}
	}

}
#endif
#if WEBCAM
void Map::genMap(){
}
#endif
void Map::clearSuccessFeature(){
	for (int i = 0; i < 4; i++){
		trackSuccessFeature[0][i].clear();
		trackSuccessFeature[1][i].clear();
	}
}
int Map::getMatchNum(){
	return matchNum;
}
vector<cv::DMatch> Map::getMatches(int i){
	return matches[i];
}
vector<cv::Point2f> Map::getSuccessFeature(int i, int j){
	return trackSuccessFeature[i][j];
}
cv::Mat Map::getResult(){
	return result;
}
bool Map::getTrack(){
	return track;
}


//private
cv::Point3f Map::calUnitVector(cv::Point3f input){
	cv::Point3f result;
	float length = 0.0f;
	length = sqrtf(input.x * input.x + input.y * input.y + input.z * input.z);
	result.x = input.x / length;
	result.y = input.y / length;
	result.z = input.z / length;

	return result;
}
cv::Point3f Map::calNormal3Point(cv::Point3f inputCenter, cv::Point3f input2, cv::Point3f input3){
	/*cout << "inputCenter" << inputCenter << endl;
	cout << "input2" << input2 << endl;
	cout << "input3" << input3 << endl;*/

	cv::Point3f cross1(input2 - inputCenter);
	cv::Point3f cross2(input3 - inputCenter);

	//cout << "cross1" << cross1 << endl;
	//cout << "cross2" << cross2 << endl;
	

	cv::Point3f crossResult = cross1.cross(cross2);

	//cout << "cross1" << crossResult << endl;
	cout << "crossResult" << calUnitVector(crossResult) << endl;
	return calUnitVector(crossResult);
}
vector<int> Map::random6PointSelect(int cols){
	int max = cols;
	vector<int> select;
	int ran = 0;
	for (int i = 0; i < 6; i++){
		ran = rand() % max - 1;
		if (ran < 0)
			ran = 0;
		select.push_back(ran);
	}
	return select;
}
cv::Mat Map::random6PointSelectMat(vector<int> select, cv::Mat inputInit){
	cv::Mat result = *(cv::Mat_<float>(3, 6) <<
		inputInit.at<float>(0, select[0]), inputInit.at<float>(0, select[1]), inputInit.at<float>(0, select[2]), inputInit.at<float>(0, select[3]), inputInit.at<float>(0, select[4]), inputInit.at<float>(0, select[5]),
		inputInit.at<float>(1, select[0]), inputInit.at<float>(1, select[1]), inputInit.at<float>(1, select[2]), inputInit.at<float>(1, select[3]), inputInit.at<float>(1, select[4]), inputInit.at<float>(1, select[5]),
		inputInit.at<float>(2, select[0]), inputInit.at<float>(2, select[1]), inputInit.at<float>(2, select[2]), inputInit.at<float>(2, select[3]), inputInit.at<float>(2, select[4]), inputInit.at<float>(2, select[5]));
	return result;
}
void Map::calculateCoarseCamPose(cv::Mat inputInit, cv::Mat inputLater){
	vector<int> select = random6PointSelect(inputInit.cols);
	cv::Mat init = random6PointSelectMat(select, inputInit);
	cv::Mat later = random6PointSelectMat(select, inputLater);
	/*cout << "init" << endl;
	cout << init << endl;
	cout << "later" << endl;
	cout << later << endl;*/
	cv::Mat A = *(cv::Mat_<float>(18, 6) <<
		0, init.at<float>(2, 0), -init.at<float>(1, 0), 1, 0, 0,
		-init.at<float>(2, 0), 1, init.at<float>(0, 0), 0, 1, 0,
		init.at<float>(1, 0), -init.at<float>(0, 0), 1, 0, 0, 1,
		0, init.at<float>(2, 1), -init.at<float>(1, 1), 1, 0, 0,
		-init.at<float>(2, 1), 1, init.at<float>(0, 1), 0, 1, 0,
		init.at<float>(1, 1), -init.at<float>(0, 1), 1, 0, 0, 1,
		0, init.at<float>(2, 2), -init.at<float>(1, 2), 1, 0, 0,
		-init.at<float>(2, 2), 1, init.at<float>(0, 2), 0, 1, 0,
		init.at<float>(1, 2), -init.at<float>(0, 2), 1, 0, 0, 1,
		0, init.at<float>(2, 3), -init.at<float>(1, 3), 1, 0, 0,
		-init.at<float>(2, 3), 1, init.at<float>(0, 3), 0, 1, 0,
		init.at<float>(1, 3), -init.at<float>(0, 3), 1, 0, 0, 1,
		0, init.at<float>(2, 4), -init.at<float>(1, 4), 1, 0, 0,
		-init.at<float>(2, 4), 1, init.at<float>(0, 4), 0, 1, 0,
		init.at<float>(1, 4), -init.at<float>(0, 4), 1, 0, 0, 1,
		0, init.at<float>(2, 5), -init.at<float>(1, 5), 1, 0, 0,
		-init.at<float>(2, 5), 1, init.at<float>(0, 5), 0, 1, 0,
		init.at<float>(1, 5), -init.at<float>(0, 5), 1, 0, 0, 1 );
	cv::Mat B = *(cv::Mat_<float>(18, 1) <<
		later.at<float>(0, 0) - init.at<float>(0, 0),
		later.at<float>(1, 0) - init.at<float>(1, 0),
		later.at<float>(2, 0) - init.at<float>(2, 0),
		later.at<float>(0, 1) - init.at<float>(0, 1),
		later.at<float>(1, 1) - init.at<float>(1, 1),
		later.at<float>(2, 1) - init.at<float>(2, 1),
		later.at<float>(0, 2) - init.at<float>(0, 2),
		later.at<float>(1, 2) - init.at<float>(1, 2),
		later.at<float>(2, 2) - init.at<float>(2, 2),
		later.at<float>(0, 3) - init.at<float>(0, 3),
		later.at<float>(1, 3) - init.at<float>(1, 3),
		later.at<float>(2, 3) - init.at<float>(2, 3), 
		later.at<float>(0, 4) - init.at<float>(0, 4),
		later.at<float>(1, 4) - init.at<float>(1, 4),
		later.at<float>(2, 4) - init.at<float>(2, 4),
		later.at<float>(0, 5) - init.at<float>(0, 5),
		later.at<float>(1, 5) - init.at<float>(1, 5),
		later.at<float>(2, 5) - init.at<float>(2, 5));

	cv::Mat inverseA;
	cv::invert(A, inverseA, CV_SVD);
	cv::Mat result;
	result = inverseA*B;
	cout << "result" << endl;
	cout << result << endl;
}
void Map::calculateCoarseCamPoseICP(cv::Mat init, cv::Mat later){	
	//ICP 계산			오류있는걸로 추정됨.
	vector <cv::Point3f> d, n, s;
	for (int i = 0; i < 6; i++){
		s.push_back(cv::Point3f(init.at<float>(0, i), init.at<float>(1, i), init.at<float>(2, i)));
	}
	for (int i = 1; i < 7; i++){
		
		d.push_back(cv::Point3f(later.at<float>(0, i), later.at<float>(1, i), later.at<float>(2, i)));
	}
	cv::Point3f n1 = calNormal3Point(d[0], d[1], d[2]);
	cv::Point3f n2 = n1;
	cv::Point3f n3 = n1;
	cv::Point3f n4 = calNormal3Point(d[3], d[4], d[5]);
	cv::Point3f n5 = n4;
	cv::Point3f n6 = n4;
	cout << "s"<<s << endl;
	cout << "d"<<d << endl;
	n.push_back(n1);
	n.push_back(n2);
	n.push_back(n3);
	n.push_back(n4);
	n.push_back(n5);
	n.push_back(n6);

	cv::Mat A = *(cv::Mat_<float>(6, 6) <<
		n[0].z*s[0].y - n[0].y*s[0].z, n[0].x*s[0].z - n[0].z*s[0].x, n[0].y*s[0].x - n[0].x*s[0].y, n[0].x, n[0].y, n[0].z,
		n[1].z*s[1].y - n[1].y*s[1].z, n[1].x*s[1].z - n[1].z*s[1].x, n[1].y*s[1].x - n[1].x*s[1].y, n[1].x, n[1].y, n[1].z,
		n[2].z*s[2].y - n[2].y*s[2].z, n[2].x*s[2].z - n[2].z*s[2].x, n[2].y*s[2].x - n[2].x*s[2].y, n[2].x, n[2].y, n[2].z,
		n[3].z*s[3].y - n[3].y*s[3].z, n[3].x*s[3].z - n[3].z*s[3].x, n[3].y*s[3].x - n[3].x*s[3].y, n[3].x, n[3].y, n[3].z,
		n[4].z*s[4].y - n[4].y*s[4].z, n[4].x*s[4].z - n[4].z*s[4].x, n[4].y*s[4].x - n[4].x*s[4].y, n[4].x, n[4].y, n[4].z,
		n[5].z*s[5].y - n[5].y*s[5].z, n[5].x*s[5].z - n[5].z*s[5].x, n[5].y*s[5].x - n[5].x*s[5].y, n[5].x, n[5].y, n[5].z);

	cv::Mat B = *(cv::Mat_<float>(6, 1) << 
		n[0].x*d[0].x + n[0].y*d[0].y + n[0].z*d[0].z - n[0].x*s[0].x - n[0].y*s[0].y - n[0].z*s[0].z,
		n[1].x*d[1].x + n[1].y*d[1].y + n[1].z*d[1].z - n[1].x*s[1].x - n[1].y*s[1].y - n[1].z*s[1].z,
		n[2].x*d[2].x + n[2].y*d[2].y + n[2].z*d[2].z - n[2].x*s[2].x - n[2].y*s[2].y - n[2].z*s[2].z,
		n[3].x*d[3].x + n[3].y*d[3].y + n[3].z*d[3].z - n[3].x*s[3].x - n[3].y*s[3].y - n[3].z*s[3].z,
		n[4].x*d[4].x + n[4].y*d[4].y + n[4].z*d[4].z - n[4].x*s[4].x - n[4].y*s[4].y - n[4].z*s[4].z,
		n[5].x*d[5].x + n[5].y*d[5].y + n[5].z*d[5].z - n[5].x*s[5].x - n[5].y*s[5].y - n[5].z*s[5].z);
	cv::Mat inverseA;
	cv::invert(A, inverseA, CV_SVD);

	cv::Mat result = inverseA*B;

	cout << "result" << endl;
	for (int i = 0; i < 6; i++){
		for (int j = 0; j < 1; j++){
			cout << result.at<float>(i, j) << "  ";
		}
		cout << endl;
	}
}
float Map::getDepth(cv::Mat depth, float row, float col){
	float temp[5] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
	cv::imshow("????", depth);
	cout << "row :" << row << endl;
	cout << "col: " << col << endl;
	temp[0] = depth.at<uchar>(row, col);
	//	1
	//2 0 3
	//  4

	int rad = 1;

	if (temp[0] > 203){
		temp[0] = 204;
		float row1 = row - rad;
		float row2 = row;
		float row3 = row;
		float row4 = row + rad;
		if (row1 < 0){
			row1 = row;
		}
		if (row4 > 479){
			row4 = row;
		}
		float col1 = col;
		float col2 = col - rad;
		float col3 = col + rad;
		float col4 = col;
		if (col2 < 0){
			col2 = col;
		}
		if (col4 > 639){
			col4 = col;
		}
		temp[1] = depth.at<uchar>(row1, col1);
		temp[2] = depth.at<uchar>(row2, col2);
		temp[3] = depth.at<uchar>(row3, col3);
		temp[4] = depth.at<uchar>(row4, col4);
		
		float min = 0.0f;
		min = (temp[1] < temp[2]) ? temp[1] : temp[2];
		min = (min < temp[3]) ? min : temp[3];
		min = (min < temp[4]) ? min : temp[4];
		temp[0] = (temp[0] < min) ? temp[0] : min;
		temp[0] = min;
		if (temp[0]>204){
			rad = 2;
			float row1 = row - rad;
			float row2 = row;
			float row3 = row;
			float row4 = row + rad;
			if (row1 < 0){
				row1 = row;
			}
			if (row4 > 479){
				row4 = row;
			}
			float col1 = col;
			float col2 = col - rad;
			float col3 = col + rad;
			float col4 = col;
			if (col2 < 0){
				col2 = col;
			}
			if (col4 > 639){
				col4 = col;
			}
			temp[1] = depth.at<uchar>(row1, col1);
			temp[2] = depth.at<uchar>(row2, col2);
			temp[3] = depth.at<uchar>(row3, col3);
			temp[4] = depth.at<uchar>(row4, col4);

			float min = 0.0f;
			min = (temp[1] < temp[2]) ? temp[1] : temp[2];
			min = (min < temp[3]) ? min : temp[3];
			min = (min < temp[4]) ? min : temp[4];
			temp[0] = (temp[0] < min) ? temp[0] : min;
		}
	}
	return temp[0];
}