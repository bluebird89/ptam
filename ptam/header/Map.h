#ifndef MAP_H
#define MAP_H

#include <opencv2\opencv.hpp>
#include <opencv2\stitching\detail\motion_estimators.hpp>

#include "ImageData.h"
#include "resultMap.h"
#include "KeyFrame.h"
#include "DEFINE.h"

class Map{
public:
	Map();
	~Map();
	void setProperty(cv::Mat, cv::Mat);
	void initMap(cv::Mat color, cv::Mat gray, cv::Mat depth);
	void genMap();
	void updateMap(cv::Mat color, cv::Mat gray, cv::Mat depth);
	void runSBA();
	void clearSuccessFeature();
	int getMatchNum();
	vector<cv::DMatch> getMatches();

	//앞의 int는 init과 next, 뒤의int는 pyramid level
	vector<cv::Point2f> getSuccessFeature(int);
	cv::Mat getResult();
	cv::Mat getResultR();
	cv::Mat getResultT();
	cv::Mat getResultRGB();
	cv::Mat getInitPoint3fDepth();
	vector<cv::Point2f> getTrackPoint3f();
	cv::Mat getSuccessCalculateMapPoint();
	cv::Mat getCorner3D();
	cv::Mat getSba3D();
	cv::Mat getSbaR(int colum);
	cv::Mat getSbaT();
	bool getTrack();
	bool getInitFrame();
private:
	cv::Point3f calUnitVector(cv::Point3f);
	cv::Point3f calNormal3Point(cv::Point3f, cv::Point3f, cv::Point3f);
	vector<int> random6PointSelect(int);
	cv::Mat random6PointSelectMat(vector<int>, cv::Mat);
	vector<int> ransacMatchPoint(vector<cv::KeyPoint> queryPoint, vector<cv::KeyPoint> trainPoint,	//사실 지금은 ransac을 쓰지않지..제목은훼이크
		vector<cv::DMatch> match, int num_of_point = 60);
	bool checkPoint(cv::Mat, cv::Mat, int, float threshold = 200.0f);

	void calculateCamPoseSolvePNP(vector<cv::Point3f>, vector<cv::Point2f>);
	void calculateCoarseCamPose(cv::Mat, cv::Mat); 
	void calculateCoarseCamPoseICP(cv::Mat, cv::Mat);
	int predictPoint();
	void loseCamPose();
	//float trackCamYaw(vector<cv::Point3f> orgPts, vector<cv::Point2f> curPts, quat& q, cv::Point3f& t);
	
	int matchNum;
	ImageData initImage;
	ImageData trackImage;

	KeyFrame keyFrame;
	bool initFrame;
	bool track;
	
	cv::BFMatcher* matcher;
	vector<cv::DMatch> matches;

	ResultMap resultMap;

	vector<cv::Point2f> trackSuccessFeature[2];
	vector<cv::Point3i> cornerColor;

	cv::Mat trackSuccessDescriptor[2];

	cv::Mat initPoint3fDepth;
	cv::Mat trackPoint3f;

	cv::Mat cameraMat;
	cv::Mat cameraCoff;
	cv::Mat inverseCameraMat;
	cv::Mat calibR;
	cv::Mat calibT;

	cv::Mat resultCamPose;
	cv::Mat resultR;
	cv::Mat resultT;
	cv::Mat prevR;
	cv::Mat prevT;
	int resultRansacCount;
	float resultRansacError;

	cv::Mat successCalculateMatPoint;
	cv::Mat successCalculateDescriptor;
	vector<int> successCalculateMatPointArray;
	vector<cv::Point3i> successCalculatePointColor;

	vector<cv::KeyPoint> addPoint;
	cv::Mat addPointDescriptor;
	vector<cv::Point3i> addPointColor;

	bool tracking3Frame;
};
#endif