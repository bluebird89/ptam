#ifndef RESULT_MAP_H
#define RESULT_MAP_H

#include <iostream>
#include <opencv2\opencv.hpp>

#include "DEFINE.h"

using namespace std;

class ResultMap{
public:
	ResultMap();
	~ResultMap();
	void setProperty(cv::Mat camera, cv::Mat inCamera); 
	void setRT(cv::Mat R, cv::Mat T);
	void setPoint(cv::Mat Point, cv::Mat descriptor, vector<cv::Point3i> RGB);
	bool setPredictPoint();
	void setAddPoint(cv::Mat addPointDepth, vector<cv::Point2f> addPoint, vector<cv::Point3i> addRGB, cv::Mat addPointDescriptor, bool tracking3FrameBool);
	void setSBAData(cv::Mat sbaData);
	void tracking3Frame(bool tracking3FrameBool);
	void refineMap(cv::Mat trackPointDepth, vector<int> trackPointIdx);
	void printMap(bool);

	cv::Mat getPredictDescriptor();
	cv::Mat getResultPoint();
	cv::Mat getResultRGB();
	cv::Mat getResultDescriptor();
	cv::Mat getPredictPointMat();
	vector<cv::Point3f> getPredictPoint();
	vector<cv::KeyPoint> getTempPredictPoint();
	vector<int> getPredictPointIdx();
	vector<int> getAddPointIdx();
	vector<int> getPointIdx();
	vector<int> getTrackingScore();
	int getPointSize();

	void clear();
private:
	bool checkPoint(cv::Mat pointArray, cv::Mat checkPoint, int checkPointNum, float threshold = 200.f);
	bool checkDescriptor(vector<cv::DMatch> match, int i, float threshold = 100.0f);
	vector<int> matchPoint(vector<cv::Point2f> pointA, vector<cv::Point2f> pointB, vector<cv::DMatch> matches);

	cv::Mat cameraMat;
	cv::Mat inverseCameraMat;

	cv::Mat prevR;
	cv::Mat prevT;
	cv::Mat resultR;
	cv::Mat resultT;

	int maxScore;
	int nframes;
	vector<int> resultPointTrackScore;
	cv::Mat resultPoint;
	cv::Mat resultDescriptor;
	vector<cv::Mat_<float>> resultMultDescriptor;
	cv::Mat resultRGB;

	cv::Mat tracking3FramePointDepth[3];			//3frame 동안 유지되는 point 체크하기 위해 만듬.
	cv::Mat tracking3FrameDescriptor[3];
	cv::Mat tracking3FrameRGB[3];
	vector<cv::Point2f> tracking3FramePoint[3];

	vector<cv::KeyPoint> tempPredictPointVector;
	cv::Mat predictPoint;
	cv::Mat predictDescriptor;
	vector<cv::Mat_<float>> predictMultDescriptor;
	cv::Mat predictRGB;
	vector <int> predictPointIdx;
	vector <int> addPointIdx;
	vector<int> pointIdx;

	cv::BFMatcher* matcher;

	float tempMean;
};
#endif