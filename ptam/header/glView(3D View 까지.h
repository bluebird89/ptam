#include <iostream>
#include <vector>

#include <glew.h>
#include <GLFW\glfw3.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

#include <opencv2\highgui.hpp>

#include "../common\shader.hpp"

using namespace std;

class glView{
public:
	glView();
	~glView();
	void run();
	void setVertex(cv::Mat cvVertex, cv::Mat colorImage, cv::Mat extrinsicMatR, cv::Mat extrinsicMatT, cv::Mat colorCalib);
private:
	void computeMatricesFromInputs();
	glm::mat4 getViewMatrix();
	glm::mat4 getProjectionMatrix();

	GLFWwindow* window;
	GLuint VertexArrayID;
	GLuint programID;
	GLuint MatrixID;
	GLuint TextureID;

	GLuint vertexbuffer;
	GLuint colorbuffer;
	GLuint depthbuffer;
	GLuint texture;

	glm::mat4 ViewMatrix;
	glm::mat4 ProjectionMatrix;

	glm::vec3 position;
	float horizontalAngle;
	float verticalAngle;
	float initialFoV;
	float speed;
	float positionSpeed;
};