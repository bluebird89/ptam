#include <iostream>
#include <vector>

#include <glew.h>
#include <GLFW\glfw3.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

#include <DepthSense.hxx>

#include <opencv2\highgui.hpp>
#include <opencv2\calib3d.hpp>

#include "../common\shader.hpp"
#include "DEFINE.h"

using namespace std;

class glView{
public:
	glView();
	~glView();
	void run();
	void runMapping();
	
	void setVertexDepthSense(const DepthSense::Vertex* senzImage);
	void calDepthToColor(const DepthSense::Vertex* senzImage, cv::Mat colorImage, float* extrinsicR, float* extrinsicT, float* calibArray, cv::Mat* outputDepthImage);
	void setVertexFastFeature(cv::Mat feature, cv::Mat cameraExtrincR, cv::Mat cameraExtincT, cv::Mat RGB);
	void setColor(const DepthSense::Vertex* senzImage, cv::Mat colorImage, cv::Mat extrinsicMatR, cv::Mat extrinsicMatT, cv::Mat colorCalib);
private:
	void drawCamera(cv::Mat cameraExtrincR, cv::Mat cameraExtrincT);
	void setCamera();
	void setGrid();
	void computeMatricesFromInputs();
	glm::mat4 getViewMatrix();
	glm::mat4 getProjectionMatrix();

	GLFWwindow* window;
	GLuint VertexArrayID;
	GLuint programID;
	GLuint MatrixID;
	GLuint TextureID;

	GLuint vertexbuffer;
	GLuint colorbuffer;
	GLuint depthbuffer;
	GLuint texture;

	GLuint cameraVertexbuffer;
	GLuint cameraColorbuffer;
	cv::Mat cameraVertex;
	GLfloat cameraColor[6 * 3];

	GLuint gridVertexbuffer;
	GLuint gridColorbuffer;
	GLfloat gridVertex[1200];			//100*100
	GLfloat gridColor[1200];

	glm::mat4 ViewMatrix;
	glm::mat4 ProjectionMatrix;

	glm::vec3 position;
	float horizontalAngle;
	float verticalAngle;
	float initialFoV;
	float speed;
	float positionSpeed;

	GLfloat color[320 * 240 * 3];

	cv::Mat getColor;

	int numOfVertex;
};