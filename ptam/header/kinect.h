
#include "ImagePyramid.h"



class Kinect{
public:
	Kinect();
	void getKinectDepthData(cv::Mat);

private:
	RGBQUAD m_rgbWk[640 * 480];
	RGBQUAD Nui_ShortToQuad_Depth(USHORT s);

	HANDLE depthStreamHandle;
	HANDLE rgbStreamHandle;
	HANDLE nextDepthFrameEvent;
	IplImage* rgbImage;
	IplImage* DepthImage;
};