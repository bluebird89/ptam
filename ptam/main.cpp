#include <iostream>
#include <vector>

#include <opencv2\opencv.hpp>
#include <opencv2\videostab.hpp>

#include <DepthSense.hxx>

#include "header\Map.h"
#include "header\glView.h"


using namespace std;

DepthSense::Context g_context;
DepthSense::DepthNode g_dnode;
DepthSense::ColorNode g_cnode;

uint32_t g_cFrames = 0;
uint32_t g_dFrames = 0;

int keyboardInput = 0;
int viewState = 0;

int f = 0;

bool g_bDeviceFound = false;
bool messageOnce = false;

DepthSense::ProjectionHelper* g_pProjHelper = NULL;
DepthSense::StereoCameraParameters g_scp;

cv::Mat colorImage = cv::Mat(480, 640, CV_8UC3);
cv::Mat depthImage = cv::Mat(240, 320, CV_32FC1);
cv::Mat calibDepthImage = cv::Mat(480, 640, CV_32F);
cv::Mat depthVertex = cv::Mat(240, 320, CV_16SC3);
cv::Mat extrinsicMatR = cv::Mat(3, 3, CV_32F);
cv::Mat inverseExtrinsicMatR = cv::Mat(3, 3, CV_32F);
cv::Mat extrinsicMatT = cv::Mat(3, 1, CV_32F);
cv::Mat colorCalib = cv::Mat(3, 3, CV_32F);
cv::Mat depthCalib = cv::Mat(3, 3, CV_32F);
cv::Mat invColorCalib = cv::Mat(3, 3, CV_32F);
cv::Mat invDepthCalib = cv::Mat(3, 3, CV_32F);
cv::Mat colorDistort = cv::Mat(1, 4, CV_32F); 
cv::Mat depthDistort = cv::Mat(1, 4, CV_32F);
cv::Mat depthMatTemp = cv::Mat(3, 240 * 320, CV_32F);

float extrinsicRArray[9];
float extrinsicTArray[3];
float colorCalibArray[9];

Map* mapProcess;		////calculate my process (ptam)
glView* glview;

void depthImageInit(){
	cv::invert(depthCalib, invDepthCalib);
	cv::invert(extrinsicMatR, inverseExtrinsicMatR);

	float* depthMatTempPointer = (float*)depthMatTemp.data;

	for (int i = 0; i < depthMatTemp.cols; i++){
		depthMatTempPointer[0 * depthMatTemp.cols + i] = i % 320;
		depthMatTempPointer[1 * depthMatTemp.cols + i] = i / 320;
		depthMatTempPointer[2 * depthMatTemp.cols + i] = 1;
	}
}
/*--------------------------------------------------------------------------------*/
//depth 이미지와 color image 캘리브레이션
void calDepthImageToColorImage(cv::Mat depthImage, cv::Mat colorImage, cv::Mat* outputDepth){
	cv::Mat temp = depthMatTemp.clone();
	float* tempPointer = (float*)temp.data;
	short* depthImagePointer = (short*)depthImage.data;
	//cout << depthImage << endl;
	cv::Mat result;
	result = invDepthCalib * temp;
	float* resultPointer = (float*)result.data;
	for (int i = 0; i < temp.cols; i++){
		tempPointer[0 * temp.cols + i] = resultPointer[0 * temp.cols + i] / resultPointer[2 * temp.cols + i] * (float)depthImagePointer[i];
		tempPointer[1 * temp.cols + i] = resultPointer[1 * temp.cols + i] / resultPointer[2 * temp.cols + i] * (float)depthImagePointer[i];
		tempPointer[2 * temp.cols + i] = resultPointer[2 * temp.cols + i] / resultPointer[2 * temp.cols + i] * (float)depthImagePointer[i];
	}

	temp.row(0) = temp.row(0) - extrinsicMatT.at<float>(0, 0);
	temp.row(1) = temp.row(1) + extrinsicMatT.at<float>(1, 0);
	temp.row(2) = temp.row(2) - extrinsicMatT.at<float>(2, 0);
	cv::Mat tempExtrinsicR = inverseExtrinsicMatR.clone();
	tempExtrinsicR.row(1) = -tempExtrinsicR.row(1);
	result = tempExtrinsicR * temp;
	temp = colorCalib * result;
	result.row(0) = temp.row(0) / temp.row(2);
	result.row(1) = temp.row(1) / temp.row(2);
	result.row(2) = temp.row(2) / temp.row(2);
	//cout << result << endl;
	cv::Mat resultDraw = cv::Mat::zeros(480, 640, CV_32F);
	float* resultDrawPointer = (float*)resultDraw.data;
	int x = -52;
	for (int i = 0; i < 240 * 320; i++){
		if ((int)resultPointer[0 * result.cols + i] < (640 + x) && (int)resultPointer[0 * result.cols + i] > x && (int)resultPointer[1 * result.cols + i] < 480 && (int)resultPointer[1 * result.cols + i] > 0){
			resultDrawPointer[(int)resultPointer[1 * result.cols + i] * 640 + (int)resultPointer[0 * result.cols + i] - x] = (float)depthImagePointer[i];
		}
	}
	resultDraw.copyTo(*outputDepth);
}
/*----------------------------------------------------------------------------*/
// senz RGB image convert to opencv RGB image
cv::Mat senz3DToCVRGB(const uint8_t* senzImage)
{
	cv::Mat opencvImage = cv::Mat(480, 640, CV_8UC3, (void*)senzImage);
	// Pointer 에 대한 주소를 할당해서 제대로 값을 얻어오지 못하는 것이었음.
	// 이전에 그냥 aa만 할당할 경우 에러가 생겼던 것은 할당 형식이 (void*)였기 때문이었음
	// 그래서 단순하게 포인터를 (void*)형 으로 변환하여 생성하면 됨
	//color = color.reshape(3, 480);
											//color image draw check
	cv::Mat copyImage = opencvImage.clone();
	return copyImage;
}
/*----------------------------------------------------------------------------*/
// senz Depth image convert to opencv Depth image
cv::Mat senzDepthImageToCVDepth(const int16_t* senzImage)
{
	cv::Mat opencvImage = cv::Mat(240, 320, CV_16U, (void*)senzImage);
	cv::Mat copyImage = opencvImage.clone();
	return copyImage;
}
/*----------------------------------------------------------------------------*/
// get Calib, distort, extrinsic Mat
void getPropertyMat(DepthSense::StereoCameraParameters parameter){
	extrinsicMatR.at<float>(0, 0) = extrinsicRArray[0] = parameter.extrinsics.r11;
	extrinsicMatR.at<float>(0, 1) = extrinsicRArray[1] = parameter.extrinsics.r12;
	extrinsicMatR.at<float>(0, 2) = extrinsicRArray[2] = parameter.extrinsics.r13;
	extrinsicMatR.at<float>(1, 0) = extrinsicRArray[3] = parameter.extrinsics.r21;
	extrinsicMatR.at<float>(1, 1) = extrinsicRArray[4] = parameter.extrinsics.r22;
	extrinsicMatR.at<float>(1, 2) = extrinsicRArray[5] = parameter.extrinsics.r23;
	extrinsicMatR.at<float>(2, 0) = extrinsicRArray[6] = parameter.extrinsics.r31;
	extrinsicMatR.at<float>(2, 1) = extrinsicRArray[7] = parameter.extrinsics.r32;
	extrinsicMatR.at<float>(2, 2) = extrinsicRArray[8] = parameter.extrinsics.r33;
	extrinsicMatT.at<float>(0, 0) = extrinsicTArray[0] = 0;//parameter.extrinsics.t1*1000;
	extrinsicMatT.at<float>(1, 0) = extrinsicTArray[1] = 0;//parameter.extrinsics.t2*1000;
	extrinsicMatT.at<float>(2, 0) = extrinsicTArray[2] = 0;//parameter.extrinsics.t3*1000;
	colorCalib.at<float>(0, 0) = colorCalibArray[0]= parameter.colorIntrinsics.fx;
	colorCalib.at<float>(0, 1) = colorCalibArray[1]= 0;
	colorCalib.at<float>(0, 2) = colorCalibArray[2]= parameter.colorIntrinsics.cx;
	colorCalib.at<float>(1, 0) = colorCalibArray[3]= 0;
	colorCalib.at<float>(1, 1) = colorCalibArray[4]= parameter.colorIntrinsics.fy; 
	colorCalib.at<float>(1, 2) = colorCalibArray[5]= parameter.colorIntrinsics.cy;
	colorCalib.at<float>(2, 0) = colorCalibArray[6]= 0;
	colorCalib.at<float>(2, 1) = colorCalibArray[7]= 0;
	colorCalib.at<float>(2, 2) = colorCalibArray[8]= 1;
	depthCalib.at<float>(0, 0) = parameter.depthIntrinsics.fx;
	depthCalib.at<float>(0, 1) = 0;
	depthCalib.at<float>(0, 2) = parameter.depthIntrinsics.cx;
	depthCalib.at<float>(1, 0) = 0;
	depthCalib.at<float>(1, 1) = parameter.depthIntrinsics.fy;
	depthCalib.at<float>(1, 2) = parameter.depthIntrinsics.cy;
	depthCalib.at<float>(2, 0) = 0;
	depthCalib.at<float>(2, 1) = 0;
	depthCalib.at<float>(2, 2) = 1;
	colorDistort.at<float>(0, 0) = parameter.colorIntrinsics.k1;
	colorDistort.at<float>(0, 1) = parameter.colorIntrinsics.k2;
	colorDistort.at<float>(0, 2) = parameter.colorIntrinsics.p1;
	colorDistort.at<float>(0, 3) = parameter.colorIntrinsics.p2;
	depthDistort.at<float>(0, 0) = parameter.depthIntrinsics.k1;
	depthDistort.at<float>(0, 1) = parameter.depthIntrinsics.k2;
	depthDistort.at<float>(0, 2) = parameter.depthIntrinsics.p1;
	depthDistort.at<float>(0, 3) = parameter.depthIntrinsics.p2;
	//cout << extrinsicMatR << endl;
	/*colorCalib.at<float>(0, 0) = 589.291;
	colorCalib.at<float>(0, 1) = 0;
	colorCalib.at<float>(0, 2) = 302.741;
	colorCalib.at<float>(1, 0) = 0;
	colorCalib.at<float>(1, 1) = 589.846;
	colorCalib.at<float>(1, 2) = 182.349;
	colorCalib.at<float>(2, 0) = 0;
	colorCalib.at<float>(2, 1) = 0;
	colorCalib.at<float>(2, 2) = 1;*/
	cout << "color" << colorCalib << endl;
	cout << "color distort" << colorDistort << endl;
	//cout << "distort" << colorDistort << endl;
	depthImageInit();
	mapProcess->setProperty(colorCalib, colorDistort);
}
/*----------------------------------------------------------------------------*/
// New color sample event handler
void onNewColorSample(DepthSense::ColorNode node, DepthSense::ColorNode::NewSampleReceivedData data)
{
	colorImage = senz3DToCVRGB(data.colorMap.operator const uint8_t *());
	//printf("C#%u: %d\n", g_cFrames, data.colorMap.size());
	g_cFrames++;
}
/*----------------------------------------------------------------------------*/
// New depth sample event handler
void onNewDepthSample(DepthSense::DepthNode node, DepthSense::DepthNode::NewSampleReceivedData data)
{
	if (!g_pProjHelper)
	{
		g_pProjHelper = new DepthSense::ProjectionHelper(data.stereoCameraParameters);
		g_scp = data.stereoCameraParameters;
		getPropertyMat(g_scp);
	}
	else if (g_scp != data.stereoCameraParameters)
	{
		g_pProjHelper->setStereoCameraParameters(data.stereoCameraParameters);
		g_scp = data.stereoCameraParameters;
	}

	depthImage = senzDepthImageToCVDepth(data.depthMap.operator const int16_t *());
	cv::Mat undistortDepth;
	cv::Mat undistortColor;
	cv::undistort(depthImage, undistortDepth, depthCalib, depthDistort);
	cv::undistort(colorImage, undistortColor, colorCalib, colorDistort);
	calDepthImageToColorImage(depthImage, colorImage, &calibDepthImage);

	cv::Mat grayImage;
	cv::cvtColor(undistortColor, grayImage, cv::COLOR_BGR2GRAY);
	if (mapProcess->getTrack()){
		mapProcess->initMap(undistortColor, grayImage, calibDepthImage);
		vector<cv::Point2f> trackPoint = mapProcess->getTrackPoint3f();
		for (int i = 0; i < trackPoint.size(); i++){
			cv::circle(undistortColor, trackPoint[i], 2, cv::Scalar(0, 0, 255));
		}
	}
	g_dFrames++;

	// Quit the main loop when tap the esc key
	keyboardInput = cv::waitKey(1);
	char color[100], depth[100];
	vector<int> image_params;
	image_params.push_back(cv::IMWRITE_PNG_COMPRESSION);
	image_params.push_back(0);

	switch (keyboardInput)
	{
	case 49:				//1번 view
		messageOnce = false;
		viewState = 0;
		break;
	case 50:				//mapping view
		messageOnce = false;
		viewState = 1;
		break;
	case 51:				//sba view & mapping
		messageOnce = false;
		cout << "run sba" << endl;
		viewState = 2;
		break;
	case 27:				//esc 누르면 꺼짐
		cout << "quit" << endl;
		viewState = 99;
		break;
	case 48:				//현재 이미지 저장
		sprintf_s(color, "colorImage%d.png", f);
		sprintf_s(depth, "depthImage%d.png", f);
		cv::imwrite(color, colorImage, image_params);
		cv::imwrite(depth, depthImage * 255, image_params);
		cout << color << endl;
		f++;
		break;
	case 32:				//1번 view에서 space bar 누르면 tracking 시작
		if (!mapProcess->getInitFrame()|| !mapProcess->getTrack()){
			mapProcess->initMap(colorImage, grayImage, calibDepthImage);
			cout << mapProcess->getInitFrame() << endl;
			cout << mapProcess->getTrack() << endl;
			cout << mapProcess->getMatchNum() << endl;
		}
		else if(mapProcess->getInitFrame()&&mapProcess->getTrack()){
			cout << "generate map" << endl;
			mapProcess->genMap();
			if (mapProcess->getTrack()){
				mapProcess->updateMap(colorImage, grayImage, calibDepthImage);
			}
		}
		break;
	}

	switch (viewState){
	case 0:
		if (messageOnce == false){
			cout << "color Image & depth Image" << endl;
			messageOnce = true;
		}
		cv::imshow("opencv view", undistortColor);
		cv::imshow("depth view", calibDepthImage / 10000);
		break;
	case 1:
		if (messageOnce == false){
			cout << "mapping start" << endl;
			messageOnce = true;
		}
		cv::imshow("opencv view", undistortColor);
		cv::imshow("depth view", calibDepthImage / 10000);
		mapProcess->updateMap(undistortColor, grayImage, calibDepthImage);
		glview->setVertexFastFeature(mapProcess->getCorner3D(), mapProcess->getResultR(), mapProcess->getResultT(), mapProcess->getResultRGB());
		glview->runMapping();
		break;
	case 2:
		if (messageOnce == false){
			cout << "sba Point print" << endl;
			mapProcess->runSBA();
			messageOnce = true;
		}
		cv::imshow("opencv view", undistortColor);
		cv::imshow("depth view", calibDepthImage / 10000);
		mapProcess->updateMap(undistortColor, grayImage, calibDepthImage);
		glview->setVertexFastFeature(mapProcess->getSba3D(), mapProcess->getResultR(), mapProcess->getResultT(), mapProcess->getResultRGB());
		glview->runMapping();
		break;
	case 99:
		mapProcess->~Map();
		g_context.quit();
		break;
	}
}
/*----------------------------------------------------------------------------*/
void configureDepthNode()
{
	g_dnode.newSampleReceivedEvent().connect(&onNewDepthSample);

	DepthSense::DepthNode::Configuration config = g_dnode.getConfiguration();
	config.frameFormat = DepthSense::FRAME_FORMAT_QVGA;
	config.framerate = 25;
	config.mode = DepthSense::DepthNode::CAMERA_MODE_CLOSE_MODE;
	config.saturation = true;

	g_dnode.setEnableDepthMap(true); 

	g_context.requestControl(g_dnode, 0);

	g_dnode.setConfiguration(config);
}
/*----------------------------------------------------------------------------*/
void configureColorNode()
{
	// connect new color sample handler
	g_cnode.newSampleReceivedEvent().connect(&onNewColorSample);

	DepthSense::ColorNode::Configuration config = g_cnode.getConfiguration();
	config.frameFormat = DepthSense::FRAME_FORMAT_VGA;
	config.compression = DepthSense::COMPRESSION_TYPE_MJPEG;
	config.powerLineFrequency = DepthSense::POWER_LINE_FREQUENCY_50HZ;
	config.framerate = 25;

	g_cnode.setEnableColorMap(true);
	g_context.requestControl(g_cnode, 0);
	g_cnode.setConfiguration(config);
}
/*----------------------------------------------------------------------------*/
void configureNode(DepthSense::Node node)
{
	if ((node.is<DepthSense::DepthNode>()) && (!g_dnode.isSet()))
	{
		g_dnode = node.as<DepthSense::DepthNode>();
		configureDepthNode();
		g_context.registerNode(node);
	}
	if ((node.is<DepthSense::ColorNode>()) && (!g_cnode.isSet()))
	{
		g_cnode = node.as<DepthSense::ColorNode>();
		configureColorNode();
		g_context.registerNode(node);
	}
}
/*----------------------------------------------------------------------------*/
void onNodeConnected(DepthSense::Device device, DepthSense::Device::NodeAddedData data)
{
	configureNode(data.node);
}
/*----------------------------------------------------------------------------*/
void onNodeDisconnected(DepthSense::Device device, DepthSense::Device::NodeRemovedData data)
{
	if (data.node.is<DepthSense::ColorNode>() && (data.node.as<DepthSense::ColorNode>() == g_cnode))
		g_cnode.unset();
	if (data.node.is<DepthSense::DepthNode>() && (data.node.as<DepthSense::DepthNode>() == g_dnode))
		g_dnode.unset();
	printf("Node disconnected\n");
}
/*----------------------------------------------------------------------------*/
void onDeviceConnected(DepthSense::Context context, DepthSense::Context::DeviceAddedData data)
{
	if (!g_bDeviceFound)
	{
		data.device.nodeAddedEvent().connect(&onNodeConnected);
		data.device.nodeRemovedEvent().connect(&onNodeDisconnected);
		g_bDeviceFound = true;
	}
}
/*----------------------------------------------------------------------------*/
void onDeviceDisconnected(DepthSense::Context context, DepthSense::Context::DeviceRemovedData data)
{
	g_bDeviceFound = false;
	printf("Device disconnected\n");
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	mapProcess = new Map;
	glview = new glView;
	g_context = DepthSense::Context::create("localhost");

	g_context.deviceAddedEvent().connect(&onDeviceConnected);
	g_context.deviceRemovedEvent().connect(&onDeviceDisconnected);

	// Get the list of currently connected devices
	vector<DepthSense::Device> da = g_context.getDevices();

	// We are only interested in the first device
	if (da.size() >= 1)
	{
		g_bDeviceFound = true;

		da[0].nodeAddedEvent().connect(&onNodeConnected);
		da[0].nodeRemovedEvent().connect(&onNodeDisconnected);

		vector<DepthSense::Node> na = da[0].getNodes();

		printf("Found %u nodes\n", na.size());

		for (int n = 0; n < (int)na.size(); n++)
			configureNode(na[n]);
	}

	g_context.startNodes();

	g_context.run();

	g_context.stopNodes();

	if (g_cnode.isSet()) g_context.unregisterNode(g_cnode);
	if (g_dnode.isSet()) g_context.unregisterNode(g_dnode);

	if (g_pProjHelper)
		delete g_pProjHelper;
	
	delete glview;
	delete mapProcess;
	return 0;
}
