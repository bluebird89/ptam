#include "ImageData.h"

ImageData::ImageData(){
	brisk = cv::BRISK::create();
	sift = cv::xfeatures2d::SIFT::create();
	surf = cv::xfeatures2d::SURF::create();
}
ImageData::~ImageData(){
}
void ImageData::run(cv::Mat input, cv::Mat gray, cv::Mat depth, bool isTrack){
	init();
	colorMat = input.clone();
	GrayMat = gray.clone();
	cv::GaussianBlur(gray, GrayMat, cv::Size(0, 0), 0.95);
	DepthMat = depth.clone();
#if PYR ==1
	cv::resize(GrayMat, pyrImage, cv::Size(colorMat.cols / 4, colorMat.rows / 4));
	cv::imshow("pyr", pyrImage);
#endif
#if BRISK_SIFT == 1
	Fast(isTrack);
	Brisk();
#endif
#if BRISK_SIFT == 0
	Sift();
#endif
	setFastRGB();
}
void ImageData::Fast(bool isTrack){
	cv::FAST(GrayMat, fastCorner, 20, !isTrack, cv::FastFeatureDetector::TYPE_9_16);
#if PYR ==1
	cv::FAST(pyrImage, pyrFastCorner, 20, !isTrack, cv::FastFeatureDetector::TYPE_9_16);
#endif
}
void ImageData::Sift(){
	cv::Mat mask;
	//surf->detectAndCompute(GrayMat, mask, fastCorner, descriptor);
	//cout << descriptor.type() << endl;
	//cout << descriptor.rows << " " << descriptor.cols << endl;
	sift->detectAndCompute(GrayMat, mask, fastCorner, descriptor);
}
void ImageData::Brisk(){
	brisk->compute(GrayMat, fastCorner, descriptor);
#if PYR ==1
	brisk->compute(pyrImage, pyrFastCorner, pyrDescriptor);
#endif
}
void ImageData::setFastRGB(){
	cv::Vec3b* colorMatPointer = (cv::Vec3b*) colorMat.data;
	for (int j = 0; j < fastCorner.size(); j++){
		fastRGB.push_back(cv::Point3i(colorMatPointer[(int)fastCorner[j].pt.y * 640 + (int)fastCorner[j].pt.x][0],
			colorMatPointer[(int)fastCorner[j].pt.y * 640 + (int)fastCorner[j].pt.x][1],
			colorMatPointer[(int)fastCorner[j].pt.y * 640 + (int)fastCorner[j].pt.x][2]));
	}
}
void ImageData::setTrackCornerIdx(vector<int> worldIdx, vector<int> trackIdx, int resultPointSize){
	track_world_CornerIdx[0] = trackIdx;
	track_world_CornerIdx[1] = worldIdx;
	resultMapPointSize = resultPointSize;
	drawMatch();
}
void ImageData::setUsingPoint(vector<int> usingMatch, bool point2f){
	if (point2f == true){
		vector<cv::Point2f> temp;
		for (int i = 0; i < usingMatch.size(); i++){
			temp.push_back(trackFastCorner[usingMatch[i]]);
		}
		trackFastCorner.clear();
		trackFastCorner = temp;
	}
	else{
		vector<cv::Point3f> temp2;
		for (int i = 0; i < usingMatch.size(); i++){
			temp2.push_back(trackFastCornerDepth[usingMatch[i]]);
		}
		trackFastCornerDepth = temp2;
	}
}
void ImageData::setUsingPoint(vector<int> usingMatch, vector<cv::DMatch> matches, bool track){
	//track == true 일경우 descriptor은 건드리지않고 2DPoint 만 걸러냄 3D point 로는 받기만 함.
	//track == false 일경우 descriptor까지 3D point로 depth가 있는 아이들만 걸러냄

#if BRISK_SIFT == 0
	float* descriptorPointer = (float*)descriptor.data;
#endif
#if BRISK_SIFT == 1
	uchar* descriptorPointer = (uchar*)descriptor.data;
#endif
	float depth = 0.0f;
	
	if (track == true){
		descriptorIdx.clear();
		trackFastCorner.clear();
		for (int i = 0; i < usingMatch.size(); i++){
			descriptorIdx.push_back(i);
			trackFastCorner.push_back(fastCorner[matches[usingMatch[i]].trainIdx].pt);
		}
	}
	else{
		descriptorIdx.clear();
		trackFastCornerDepth.clear();
		trackRGB.clear();
		for (int i = 0; i < usingMatch.size(); i++){
			depth = getDepth(fastCorner[matches[usingMatch[i]].queryIdx].pt.y, fastCorner[matches[usingMatch[i]].queryIdx].pt.x);
			if (depth != 32000.f&&depth > 500){
				descriptorIdx.push_back(i);
				trackFastCornerDepth.push_back(cv::Point3f(
					fastCorner[matches[usingMatch[i]].queryIdx].pt.x * depth,
					fastCorner[matches[usingMatch[i]].queryIdx].pt.y * depth,
					depth));
				trackRGB.push_back(fastRGB[i]);
			}
		}
#if BRISK_SIFT == 0
		trackDescriptor = cv::Mat(descriptorIdx.size(), DES_SIZE, CV_32F);
		float* trackDescriptorPointer = (float*)trackDescriptor.data;
#endif
#if BRISK_SIFT == 1
		trackDescriptor = cv::Mat(descriptorIdx.size(), DES_SIZE, CV_8U);
		uchar* trackDescriptorPointer = (uchar*)trackDescriptor.data;
#endif
		for (int i = 0; i < descriptorIdx.size(); i++){
			for (int j = 0; j < DES_SIZE; j++){
				trackDescriptorPointer[DES_SIZE * i + j] = descriptorPointer[DES_SIZE * matches[usingMatch[descriptorIdx[i]]].queryIdx + j];
			}
		}
	}
}
void ImageData::setPyrUsingPoint(vector<int> usingMatch, bool point2f){
	if (point2f == true){
		vector<cv::Point2f> temp;
		for (int i = 0; i < usingMatch.size(); i++){
			temp.push_back(trackPyrFastCorner[usingMatch[i]]);
		}
		trackPyrFastCorner.clear();
		trackPyrFastCorner = temp;
	}
	else{
		vector<cv::Point3f> temp2;
		for (int i = 0; i < usingMatch.size(); i++){
			temp2.push_back(trackPyrFastCornerDepth[usingMatch[i]]);
		}
		trackPyrFastCornerDepth = temp2;
	}
}
void ImageData::setPyrUsingPoint(vector<int> usingMatch, vector<cv::DMatch> matches, bool track){
	uchar* pyrDescriptorPointer = (uchar*)pyrDescriptor.data;
	float depth = 0.0f;

	if (track == true){
		pyrDescriptorIdx.clear();
		trackPyrFastCorner.clear();
		for (int i = 0; i < usingMatch.size(); i++){
			pyrDescriptorIdx.push_back(i);
			trackPyrFastCorner.push_back(pyrFastCorner[matches[usingMatch[i]].trainIdx].pt);
		}
	}
	else{
		pyrDescriptorIdx.clear();
		trackPyrFastCornerDepth.clear();

		for (int i = 0; i < usingMatch.size(); i++){
			depth = getDepth(pyrFastCorner[matches[usingMatch[i]].queryIdx].pt.y * 4, pyrFastCorner[matches[usingMatch[i]].queryIdx].pt.x * 4);
			if (depth != 32000.f&&depth > 500){
				pyrDescriptorIdx.push_back(i);
				trackPyrFastCornerDepth.push_back(cv::Point3f(
					pyrFastCorner[matches[usingMatch[i]].queryIdx].pt.x * depth,
					pyrFastCorner[matches[usingMatch[i]].queryIdx].pt.y * depth,
					depth));
			}
		}
		trackPyrDescriptor = cv::Mat(pyrDescriptorIdx.size(), DES_SIZE, CV_8U);
		uchar* trackDescriptorPointer = (uchar*)trackPyrDescriptor.data;
		for (int i = 0; i < pyrDescriptorIdx.size(); i++){
			for (int j = 0; j < DES_SIZE; j++){
				trackDescriptorPointer[DES_SIZE * i + j] = pyrDescriptorPointer[DES_SIZE * matches[usingMatch[pyrDescriptorIdx[i]]].queryIdx + j];
			}
		}
	}
}
vector<int> ImageData::getPyrDescriptorIdx(){
	return pyrDescriptorIdx;
}
void ImageData::setAddPoint(vector<int> nonUsingMatch){
	sort(nonUsingMatch.begin(), nonUsingMatch.end());
	vector<int> usingMatch;
	vector<float> usingDepth;
	float depth = 0.0f;
	
	//addDescriptor = cv::Mat(fastCorner.size() - nonUsingMatch.size(), 64, CV_8U);
	for (int i = 0, k = 0; i < fastCorner.size(); i++){
		if (i != nonUsingMatch[k]){
			depth = getDepth(fastCorner[i].pt.y, fastCorner[i].pt.x);
			if (depth != 32000.f&&depth >500){
				usingDepth.push_back(depth);
				usingMatch.push_back(i);
			}
		}
		else{
			if (k < nonUsingMatch.size() - 1){
				k++;
			}
		}
	}
#if BRISK_SIFT == 0
	addDescriptor = cv::Mat(usingMatch.size(), DES_SIZE, CV_32F);
	float* addDescriptorPointer = (float*)addDescriptor.data;
	float* descriptorPointer = (float*)descriptor.data;
#endif
#if BRISK_SIFT == 1
	addDescriptor = cv::Mat(usingMatch.size(), DES_SIZE, CV_8U);
	uchar* addDescriptorPointer = (uchar*)addDescriptor.data;
	uchar* descriptorPointer = (uchar*)descriptor.data;
#endif

	addPointDepthMat = cv::Mat(3, usingMatch.size(), CV_32F);
	
	float* addPointDepthPointer = (float*)addPointDepthMat.data;
	
	addPoint.clear();
	for (int i = 0; i < usingMatch.size(); i++){
		addPoint.push_back(fastCorner[usingMatch[i]].pt);
		addPointDepthPointer[usingMatch.size() * 0 + i] = fastCorner[usingMatch[i]].pt.x * usingDepth[i];
		addPointDepthPointer[usingMatch.size() * 1 + i] = fastCorner[usingMatch[i]].pt.y * usingDepth[i];
		addPointDepthPointer[usingMatch.size() * 2 + i] = usingDepth[i];

		addRGB.push_back(fastRGB[usingMatch[i]]);
		for (int j = 0; j < DES_SIZE; j++){
			addDescriptorPointer[DES_SIZE * i + j] = descriptorPointer[DES_SIZE * usingMatch[i] + j];
		}
	}
}
void ImageData::setResultAddPoint(vector<int> resultAddPoint){
	float* tempPointer = (float*)addPointDepthMat.data;
	keyPointMat = cv::Mat(2, resultAddPoint.size() + track_world_CornerIdx[0].size(), CV_32F);
	float* keyPointMatPointer = (float*)keyPointMat.data;
	keyPointIdx.clear();
	int i = 0;
	
	//cout << "0  : " << track_world_CornerIdx[0].size() << "\n1  : " << track_world_CornerIdx[1].size() << endl;
	for (; i < track_world_CornerIdx[0].size(); i++){
		keyPointIdx.push_back(track_world_CornerIdx[1][i]);
		keyPointMatPointer[keyPointMat.cols * 0 + i] = fastCorner[track_world_CornerIdx[0][i]].pt.x;
		keyPointMatPointer[keyPointMat.cols * 1 + i] = fastCorner[track_world_CornerIdx[0][i]].pt.y;
	}
	for (int j = 0; j < resultAddPoint.size(); j++, i++){
		keyPointMatPointer[keyPointMat.cols * 0 + i] = addPoint[resultAddPoint[j]].x;
		keyPointMatPointer[keyPointMat.cols * 1 + i] = addPoint[resultAddPoint[j]].y;
		keyPointIdx.push_back(resultMapPointSize + j);
	}
	//descirptor는 안해도될거같은데///
}
cv::Mat ImageData::getGrayMat(){
	return GrayMat;
}
cv::Mat ImageData::getDepthMat(){
	return DepthMat;
}
cv::Mat ImageData::getDescriptor(){
	return descriptor;
}
cv::Mat ImageData::getTrackDescriptor(){
	return trackDescriptor;
}
cv::Mat ImageData::getTrackFastCornerDepthMat(){
	float depth = 0.0f;
	trackFastCornerDepthMat = cv::Mat(3, fastCorner.size(), CV_32F);
	float* trackFastCornerDepthMatPointer = (float*)trackFastCornerDepthMat.data;
	for (int i = 0; i < fastCorner.size(); i++){
		depth = getDepth(fastCorner[i].pt.y, fastCorner[i].pt.x);
		trackFastCornerDepthMatPointer[0 * fastCorner.size() + i] = fastCorner[i].pt.x * depth;
		trackFastCornerDepthMatPointer[1 * fastCorner.size() + i] = fastCorner[i].pt.y * depth,
			trackFastCornerDepthMatPointer[2 * fastCorner.size() + i] = depth;
	}
	return trackFastCornerDepthMat;
}
vector<int> ImageData::getDescriptorIdx(){
	return descriptorIdx;
}
vector<cv::KeyPoint> ImageData::getfastCorner(){
	return fastCorner;
}
vector<cv::Point2f> ImageData::getTrackFastCorner(){
	return trackFastCorner;
}
vector<cv::Point3f> ImageData::getTrackFastCornerDepth(){
	return trackFastCornerDepth;
}
vector<cv::Point3i> ImageData::getfastRGB(){
	return fastRGB;
}
vector<cv::Point3i> ImageData::getTrackRGB(){
	return trackRGB;
}
vector<cv::Point2f> ImageData::getAddPoint(){
	return addPoint;
}
vector<cv::Point3i> ImageData::getAddRGB(){
	return addRGB;
}
cv::Mat ImageData::getAddPointDepthMat(){
	return addPointDepthMat;
}
cv::Mat ImageData::getAddDescriptor(){
	return addDescriptor;
}
cv::Mat ImageData::getKeyPoint(){
	return keyPointMat;
}
cv::Mat ImageData::getKeyPointDepth(){
	cv::Mat keyPointDepth = cv::Mat(3, keyPointMat.cols, CV_32F);
	float* keyPointDepthPointer = (float*)keyPointDepth.data;
	float* keyPointPointer = (float*)keyPointMat.data;
	float depth = 0.0f;
	for (int i = 0; i < keyPointMat.cols; i++){
		depth = getDepth(keyPointPointer[1 * keyPointMat.cols + i], keyPointPointer[0 * keyPointMat.cols + i]);
		keyPointDepthPointer[0 * keyPointMat.cols + i] = keyPointPointer[0 * keyPointMat.cols + i] * depth;
		keyPointDepthPointer[1 * keyPointMat.cols + i] = keyPointPointer[1 * keyPointMat.cols + i] * depth;
		keyPointDepthPointer[2 * keyPointMat.cols + i] = depth;
	}
	return keyPointDepth;
}
vector<int> ImageData::getKeyPointIdx(){
	return keyPointIdx;
}
cv::Mat ImageData::getPyrDescriptor(){
	return pyrDescriptor;
}
vector<cv::KeyPoint> ImageData::getPyrfastCorner(){
	return pyrFastCorner;
}
float ImageData::getDepth(float row, float col){
	float temp[5] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
	float* depthData = (float*)DepthMat.data;
	int depthPosition[5];
	depthPosition[0] = DepthMat.rows*row + col;
	temp[0] = depthData[depthPosition[0]];//3000.0f * (1.0f - depthData[depthPosition[0]]);
	//	1
	//2 0 3
	//  4
	//cout << depthData[depthPosition[0]] << endl;

	int rad = 1;

	if (temp[0] > 3000.0f){
		temp[0] = 32000.0f;
		float row1 = row - rad;
		float row2 = row;
		float row3 = row;
		float row4 = row + rad;
		if (row1 < 0){
			row1 = row;
		}
		if (row4 > 479){
			row4 = row;
		}
		float col1 = col;
		float col2 = col - rad;
		float col3 = col + rad;
		float col4 = col;
		if (col2 < 0){
			col2 = col;
		}
		if (col4 > 639){
			col4 = col;
		}
		depthPosition[1] = DepthMat.rows*row1 + col1;
		temp[1] = depthData[depthPosition[1]];
		depthPosition[2] = DepthMat.rows*row2 + col2;
		temp[2] = depthData[depthPosition[2]];
		depthPosition[3] = DepthMat.rows*row3 + col3;
		temp[3] = depthData[depthPosition[3]];
		depthPosition[4] = DepthMat.rows*row4 + col4;
		temp[4] = depthData[depthPosition[4]];

		for (int i = 0; i < 5; i++){
			if (temp[i] > 3000.0f){
				temp[i] = 32000.0f;
			}
		}

		float min = 0.0f;
		min = (temp[1] < temp[2]) ? temp[1] : temp[2];
		min = (min < temp[3]) ? min : temp[3];
		min = (min < temp[4]) ? min : temp[4];
		temp[0] = (temp[0] < min) ? temp[0] : min;
		temp[0] = min;
		if (temp[0] == 32000.0f){
			rad = 2;
			float row1 = row - rad;
			float row2 = row;
			float row3 = row;
			float row4 = row + rad;
			if (row1 < 0){
				row1 = row;
			}
			if (row4 > 479){
				row4 = row;
			}
			float col1 = col;
			float col2 = col - rad;
			float col3 = col + rad;
			float col4 = col;
			if (col2 < 0){
				col2 = col;
			}
			if (col4 > 639){
				col4 = col;
			}
			depthPosition[1] = DepthMat.rows*row1 + col1;
			temp[1] = depthData[depthPosition[1]];
			depthPosition[2] = DepthMat.rows*row2 + col2;
			temp[2] = depthData[depthPosition[2]];
			depthPosition[3] = DepthMat.rows*row3 + col3;
			temp[3] = depthData[depthPosition[3]];
			depthPosition[4] = DepthMat.rows*row4 + col4;
			temp[4] = depthData[depthPosition[4]];

			for (int i = 0; i < 5; i++){
				if (temp[i] > 3000.0f){
					temp[i] = 32000.0f;
				}
			}

			float min = 0.0f;
			min = (temp[1] < temp[2]) ? temp[1] : temp[2];
			min = (min < temp[3]) ? min : temp[3];
			min = (min < temp[4]) ? min : temp[4];
			temp[0] = (temp[0] < min) ? temp[0] : min;
		}
	}
	return temp[0];
}
void ImageData::init(){
	colorMat.empty();
	GrayMat.empty();
	DepthMat.empty();
	fastCorner.clear();
	trackFastCornerDepth.clear();
	trackFastCorner.clear();
	trackRGB.clear();
	track_world_CornerIdx[0].clear();
	track_world_CornerIdx[1].clear();
	trackDescriptor.empty();
	resultMapPointSize = 0;
	addPoint.clear();
	addPointDepthMat.empty();
	addRGB.clear();
	addDescriptor.empty();
	keyPointMat.empty();
	keyPointIdx.clear();
	fastCornerDepth.clear();
	fastRGB.clear();
	descriptor.empty();
}
void ImageData::drawMatch(){
	cv::Mat tempMat = colorMat.clone();
	
	for (int i = 0; i < track_world_CornerIdx[0].size(); i++){
		char a;
		cv::circle(tempMat, fastCorner[track_world_CornerIdx[0][i]].pt, 1.0, cv::Scalar(22, 219, 29), 2);
		cv::putText(tempMat, to_string(track_world_CornerIdx[1][i]), fastCorner[track_world_CornerIdx[0][i]].pt,
			cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(22, 219, 29));
	}
	cv::imshow("drawMatch", tempMat);
}