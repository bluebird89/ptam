#include "KeyFrame.h"
#include <time.h>
#include <fstream>

#define MOTSTRUCT 0
#define PRINTDATA 0

/* pointers to additional data, used for computed image projections and their jacobians */
struct globs_{
	double *rot0params; /* initial rotation parameters, combined with a local rotation parameterization */
	double *intrcalib; /* the 5 intrinsic calibration parameters in the order [fu, u0, v0, ar, skew],
					   * where ar is the aspect ratio fv/fu.
					   * Used only when calibration is fixed for all cameras;
					   * otherwise, it is null and the intrinsic parameters are
					   * included in the set of motion parameters for each camera
					   */
	int nccalib; /* number of calibration parameters that must be kept constant.
				 * 0: all parameters are free
				 * 1: skew is fixed to its initial value, all other parameters vary (i.e. fu, u0, v0, ar)
				 * 2: skew and aspect ratio are fixed to their initial values, all other parameters vary (i.e. fu, u0, v0)
				 * 3: meaningless
				 * 4: skew, aspect ratio and principal point are fixed to their initial values, only the focal length varies (i.e. fu)
				 * 5: all intrinsics are kept fixed to their initial values
				 * >5: meaningless
				 * Used only when calibration varies among cameras
				 */

	int ncdist; /* number of distortion parameters in Bouguet's model that must be kept constant.
				* 0: all parameters are free
				* 1: 6th order radial distortion term (kc[4]) is fixed
				* 2: 6th order radial distortion and one of the tangential distortion terms (kc[3]) are fixed
				* 3: 6th order radial distortion and both tangential distortion terms (kc[3], kc[2]) are fixed [i.e., only 2nd & 4th order radial dist.]
				* 4: 4th & 6th order radial distortion terms and both tangential distortion ones are fixed [i.e., only 2nd order radial dist.]
				* 5: all distortion parameters are kept fixed to their initial values
				* >5: meaningless
				* Used only when calibration varies among cameras and distortion is to be estimated
				*/
	int cnp, pnp, mnp; /* dimensions */

	double *ptparams; /* needed only when bundle ad0justing for camera parameters only */
	double *camparams; /* needed only when bundle adjusting for structure parameters only */
} globs;
/*
* fast multiplication of the two quaternions in q1 and q2 into p
* this is the second of the two schemes derived in pg. 8 of
* T. D. Howell, J.-C. Lafon, The complexity of the quaternion product, TR 75-245, Cornell Univ., June 1975.
*
* total additions increase from 12 to 27 (28), but total multiplications decrease from 16 to 9 (12)
*/
void quatMultFast(double q1[FULLQUATSZ], double q2[FULLQUATSZ], double p[FULLQUATSZ])
{
	double t1, t2, t3, t4, t5, t6, t7, t8, t9;
	//double t10, t11, t12;

	t1 = (q1[0] + q1[1])*(q2[0] + q2[1]);
	t2 = (q1[3] - q1[2])*(q2[2] - q2[3]);
	t3 = (q1[1] - q1[0])*(q2[2] + q2[3]);
	t4 = (q1[2] + q1[3])*(q2[1] - q2[0]);
	t5 = (q1[1] + q1[3])*(q2[1] + q2[2]);
	t6 = (q1[1] - q1[3])*(q2[1] - q2[2]);
	t7 = (q1[0] + q1[2])*(q2[0] - q2[3]);
	t8 = (q1[0] - q1[2])*(q2[0] + q2[3]);

#if 0
	t9 = t5 + t6;
	t10 = t7 + t8;
	t11 = t5 - t6;
	t12 = t7 - t8;

	p[0] = t2 + 0.5*(-t9 + t10);
	p[1] = t1 - 0.5*(t9 + t10);
	p[2] = -t3 + 0.5*(t11 + t12);
	p[3] = -t4 + 0.5*(t11 - t12);
#endif

	/* following fragment it equivalent to the one above */
	t9 = 0.5*(t5 - t6 + t7 + t8);
	p[0] = t2 + t9 - t5;
	p[1] = t1 - t9 - t6;
	p[2] = -t3 + t9 - t8;
	p[3] = -t4 + t9 - t7;
}
void calcImgProjJacS(double a[5], double qr0[4], double v[3], double t[3],
	double M[3], double jacmS[2][3])
{
	double t1;
	double t10;
	double t101;
	double t102;
	double t103;
	double t106;
	double t108;
	double t115;
	double t12;
	double t122;
	double t123;
	double t125;
	double t128;
	double t129;
	double t14;
	double t145;
	double t148;
	double t16;
	double t18;
	double t19;
	double t2;
	double t24;
	double t25;
	double t3;
	double t30;
	double t31;
	double t37;
	double t38;
	double t4;
	double t41;
	double t42;
	double t45;
	double t46;
	double t48;
	double t49;
	double t5;
	double t50;
	double t53;
	double t56;
	double t57;
	double t59;
	double t6;
	double t60;
	double t62;
	double t64;
	double t69;
	double t7;
	double t74;
	double t79;
	double t82;
	double t83;
	double t9;
	double t97;
	{
		t1 = a[0];
		t2 = v[0];
		t3 = t2*t2;
		t4 = v[1];
		t5 = t4*t4;
		t6 = v[2];
		t7 = t6*t6;
		t9 = sqrt(1.0 - t3 - t5 - t7);
		t10 = qr0[1];
		t12 = qr0[0];
		t14 = qr0[3];
		t16 = qr0[2];
		t18 = -t9*t10 - t12*t2 - t4*t14 + t6*t16;
		t19 = t18*t18;
		t24 = t9*t12 - t2*t10 - t16*t4 - t6*t14;
		t25 = t24*t24;
		t30 = t9*t14 + t12*t6 + t16*t2 - t4*t10;
		t31 = -t30;
		t37 = -t9*t16 - t12*t4 - t6*t10 + t2*t14;
		t38 = t37*t37;
		t41 = a[4];
		t42 = t18*t37;
		t45 = t24*t31;
		t46 = 2.0*t42 + t24*t30 - t45;
		t48 = a[1];
		t49 = t31*t18;
		t50 = t24*t37;
		t53 = t49 + 2.0*t50 - t30*t18;
		t56 = -t18;
		t57 = M[0];
		t59 = -t37;
		t60 = M[1];
		t62 = M[2];
		t64 = -t56*t57 - t59*t60 - t30*t62;
		t69 = t24*t62 + t56*t60 - t59*t57;
		t74 = t24*t57 + t59*t62 - t30*t60;
		t79 = t24*t60 + t30*t57 - t56*t62;
		t82 = t64*t31 + t24*t69 + t74*t37 - t18*t79 + t[2];
		t83 = 1 / t82;
		t97 = t64*t37 + t24*t79 + t69*t18 - t74*t31 + t[1];
		t101 = t82*t82;
		t102 = 1 / t101;
		t103 = (t1*(t64*t18 + t24*t74 + t79*t31 - t69*t37 + t[0]) + t41*t97 + t48*t82)*t102;
		jacmS[0][0] = (t1*(t19 + t25 + t30*t31 - t38) + t41*t46 + t48*t53)*t83 - t103*t53;
		t106 = t1*a[3];
		t108 = a[2];
		t115 = (t106*t97 + t108*t82)*t102;
		jacmS[1][0] = (t106*t46 + t108*t53)*t83 - t115*t53;
		t122 = t31*t31;
		t123 = t38 + t25 + t56*t18 - t122;
		t125 = t31*t37;
		t128 = t24*t18;
		t129 = 2.0*t125 + t24*t56 - t128;
		jacmS[0][1] = (t1*(t42 + 2.0*t45 - t56*t37) + t123*t41 + t48*t129)*t83 - t103*t129;
		jacmS[1][1] = (t106*t123 + t108*t129)*t83 - t129*t115;
		t145 = t125 + 2.0*t128 - t59*t31;
		t148 = t122 + t25 + t59*t37 - t19;
		jacmS[0][2] = (t1*(2.0*t49 + t24*t59 - t50) + t41*t145 + t148*t48)*t83 - t103*t148;
		jacmS[1][2] = (t106*t145 + t148*t108)*t83 - t115*t148;
		return;
	}
}
void calcImgProjFullR(double a[5], double qr0[4], double t[3], double M[3],
	double n[2])
{
	double t1;
	double t11;
	double t13;
	double t17;
	double t2;
	double t22;
	double t27;
	double t3;
	double t38;
	double t46;
	double t49;
	double t5;
	double t6;
	double t8;
	double t9;
	{
		t1 = a[0];
		t2 = qr0[1];
		t3 = M[0];
		t5 = qr0[2];
		t6 = M[1];
		t8 = qr0[3];
		t9 = M[2];
		t11 = -t3*t2 - t5*t6 - t8*t9;
		t13 = qr0[0];
		t17 = t13*t3 + t5*t9 - t8*t6;
		t22 = t6*t13 + t8*t3 - t9*t2;
		t27 = t13*t9 + t6*t2 - t5*t3;
		t38 = -t5*t11 + t13*t22 - t27*t2 + t8*t17 + t[1];
		t46 = -t11*t8 + t13*t27 - t5*t17 + t2*t22 + t[2];
		t49 = 1 / t46;
		n[0] = (t1*(-t2*t11 + t13*t17 - t22*t8 + t5*t27 + t[0]) + a[4] * t38 + a[1] * t46)*t49;
		n[1] = (t1*a[3] * t38 + a[2] * t46)*t49;
		return;
	}
}
void calcImgProjJacKRTS(double a[5], double qr0[4], double v[3], double t[3],
	double M[3], double jacmKRT[2][11], double jacmS[2][3])
{
	double t1;
	double t102;
	double t107;
	double t109;
	double t11;
	double t114;
	double t116;
	double t120;
	double t129;
	double t13;
	double t131;
	double t140;
	double t148;
	double t149;
	double t15;
	double t150;
	double t152;
	double t154;
	double t161;
	double t164;
	double t167;
	double t17;
	double t170;
	double t172;
	double t174;
	double t177;
	double t18;
	double t182;
	double t187;
	double t189;
	double t194;
	double t196;
	double t2;
	double t208;
	double t218;
	double t229;
	double t232;
	double t235;
	double t237;
	double t239;
	double t24;
	double t242;
	double t247;
	double t25;
	double t252;
	double t254;
	double t259;
	double t261;
	double t273;
	double t283;
	double t295;
	double t296;
	double t298;
	double t3;
	double t301;
	double t304;
	double t305;
	double t307;
	double t308;
	double t31;
	double t311;
	double t32;
	double t326;
	double t327;
	double t329;
	double t332;
	double t333;
	double t34;
	double t349;
	double t35;
	double t352;
	double t4;
	double t41;
	double t45;
	double t5;
	double t50;
	double t51;
	double t56;
	double t57;
	double t6;
	double t60;
	double t66;
	double t67;
	double t68;
	double t74;
	double t76;
	double t78;
	double t79;
	double t8;
	double t81;
	double t83;
	double t85;
	double t87;
	double t89;
	double t9;
	double t91;
	double t93;
	double t95;
	double t97;
	{
		t1 = v[0];
		t2 = t1*t1;
		t3 = v[1];
		t4 = t3*t3;
		t5 = v[2];
		t6 = t5*t5;
		t8 = sqrt(1.0 - t2 - t4 - t6);
		t9 = qr0[1];
		t11 = qr0[0];
		t13 = qr0[3];
		t15 = qr0[2];
		t17 = t8*t9 + t11*t1 + t13*t3 - t5*t15;
		t18 = M[0];
		t24 = t8*t15 + t3*t11 + t5*t9 - t13*t1;
		t25 = M[1];
		t31 = t8*t13 + t5*t11 + t1*t15 - t3*t9;
		t32 = M[2];
		t34 = -t17*t18 - t24*t25 - t31*t32;
		t35 = -t17;
		t41 = t11*t8 - t1*t9 - t3*t15 - t5*t13;
		t45 = t41*t18 + t24*t32 - t31*t25;
		t50 = t41*t25 + t31*t18 - t17*t32;
		t51 = -t31;
		t56 = t41*t32 + t17*t25 - t24*t18;
		t57 = -t24;
		t60 = t34*t35 + t41*t45 + t50*t51 - t56*t57 + t[0];
		t66 = t34*t51 + t41*t56 + t45*t57 - t50*t35 + t[2];
		t67 = 1 / t66;
		jacmKRT[0][0] = t60*t67;
		t68 = a[3];
		t74 = t34*t57 + t41*t50 + t56*t35 - t45*t51 + t[1];
		jacmKRT[1][0] = t68*t74*t67;
		jacmKRT[0][1] = 1.0;
		jacmKRT[1][1] = 0.0;
		jacmKRT[0][2] = 0.0;
		jacmKRT[1][2] = 1.0;
		jacmKRT[0][3] = 0.0;
		t76 = a[0];
		jacmKRT[1][3] = t76*t74*t67;
		jacmKRT[0][4] = t74*t67;
		jacmKRT[1][4] = 0.0;
		t78 = 1 / t8;
		t79 = t78*t9;
		t81 = -t79*t1 + t11;
		t83 = t78*t15;
		t85 = -t83*t1 - t13;
		t87 = t78*t13;
		t89 = -t87*t1 + t15;
		t91 = -t81*t18 - t85*t25 - t89*t32;
		t93 = -t81;
		t95 = t78*t11;
		t97 = -t95*t1 - t9;
		t102 = t97*t18 + t85*t32 - t89*t25;
		t107 = t97*t25 + t89*t18 - t81*t32;
		t109 = -t89;
		t114 = t97*t32 + t81*t25 - t85*t18;
		t116 = -t85;
		t120 = a[4];
		t129 = t91*t57 + t34*t116 + t97*t50 + t41*t107 + t114*t35 + t56*t93 - t102*t51 - t45*t109
			;
		t131 = a[1];
		t140 = t91*t51 + t34*t109 + t97*t56 + t41*t114 + t102*t57 + t45*t116 - t107*t35 - t50*t93
			;
		t148 = t66*t66;
		t149 = 1 / t148;
		t150 = (t76*t60 + t120*t74 + t131*t66)*t149;
		jacmKRT[0][5] = (t76*(t91*t35 + t34*t93 + t97*t45 + t41*t102 + t107*t51 + t50*t109 -
			t114*t57 - t56*t116) + t129*t120 + t131*t140)*t67 - t150*t140;
		t152 = t76*t68;
		t154 = a[2];
		t161 = (t152*t74 + t154*t66)*t149;
		jacmKRT[1][5] = (t152*t129 + t154*t140)*t67 - t161*t140;
		t164 = -t79*t3 + t13;
		t167 = -t83*t3 + t11;
		t170 = -t87*t3 - t9;
		t172 = -t164*t18 - t167*t25 - t170*t32;
		t174 = -t164;
		t177 = -t95*t3 - t15;
		t182 = t177*t18 + t167*t32 - t170*t25;
		t187 = t177*t25 + t170*t18 - t164*t32;
		t189 = -t170;
		t194 = t177*t32 + t164*t25 - t167*t18;
		t196 = -t167;
		t208 = t172*t57 + t34*t196 + t177*t50 + t41*t187 + t194*t35 + t56*t174 - t182*t51 - t45*
			t189;
		t218 = t172*t51 + t34*t189 + t177*t56 + t41*t194 + t182*t57 + t45*t196 - t187*t35 - t50*
			t174;
		jacmKRT[0][6] = (t76*(t172*t35 + t34*t174 + t177*t45 + t41*t182 + t187*t51 + t50*t189
			- t194*t57 - t56*t196) + t120*t208 + t131*t218)*t67 - t150*t218;
		jacmKRT[1][6] = (t152*t208 + t154*t218)*t67 - t161*t218;
		t229 = -t79*t5 - t15;
		t232 = -t83*t5 + t9;
		t235 = -t87*t5 + t11;
		t237 = -t229*t18 - t232*t25 - t235*t32;
		t239 = -t229;
		t242 = -t95*t5 - t13;
		t247 = t242*t18 + t232*t32 - t235*t25;
		t252 = t242*t25 + t235*t18 - t229*t32;
		t254 = -t235;
		t259 = t242*t32 + t229*t25 - t232*t18;
		t261 = -t232;
		t273 = t237*t57 + t261*t34 + t242*t50 + t41*t252 + t259*t35 + t56*t239 - t247*t51 - t45*
			t254;
		t283 = t237*t51 + t34*t254 + t242*t56 + t41*t259 + t247*t57 + t45*t261 - t252*t35 - t50*
			t239;
		jacmKRT[0][7] = (t76*(t237*t35 + t34*t239 + t242*t45 + t41*t247 + t252*t51 + t50*t254
			- t259*t57 - t56*t261) + t120*t273 + t131*t283)*t67 - t150*t283;
		jacmKRT[1][7] = (t152*t273 + t154*t283)*t67 - t161*t283;
		jacmKRT[0][8] = t76*t67;
		jacmKRT[1][8] = 0.0;
		jacmKRT[0][9] = t120*t67;
		jacmKRT[1][9] = t152*t67;
		jacmKRT[0][10] = t131*t67 - t150;
		jacmKRT[1][10] = t154*t67 - t161;
		t295 = t35*t35;
		t296 = t41*t41;
		t298 = t57*t57;
		t301 = t35*t57;
		t304 = t41*t51;
		t305 = 2.0*t301 + t41*t31 - t304;
		t307 = t35*t51;
		t308 = t41*t57;
		t311 = t307 + 2.0*t308 - t31*t35;
		jacmS[0][0] = (t76*(t295 + t296 + t31*t51 - t298) + t120*t305 + t131*t311)*t67 - t150*
			t311;
		jacmS[1][0] = (t152*t305 + t154*t311)*t67 - t161*t311;
		t326 = t51*t51;
		t327 = t298 + t296 + t17*t35 - t326;
		t329 = t57*t51;
		t332 = t41*t35;
		t333 = 2.0*t329 + t41*t17 - t332;
		jacmS[0][1] = (t76*(t301 + 2.0*t304 - t17*t57) + t120*t327 + t131*t333)*t67 - t150*
			t333;
		jacmS[1][1] = (t152*t327 + t154*t333)*t67 - t161*t333;
		t349 = t329 + 2.0*t332 - t24*t51;
		t352 = t326 + t296 + t24*t57 - t295;
		jacmS[0][2] = (t76*(2.0*t307 + t41*t24 - t308) + t120*t349 + t131*t352)*t67 - t150*
			t352;
		jacmS[1][2] = (t152*t349 + t154*t352)*t67 - t161*t352;
		return;
	}
}
void calcImgProjJacRTS(double a[5], double qr0[4], double v[3], double t[3],
	double M[3], double jacmRT[2][6], double jacmS[2][3])
{
	double t1;
	double t10;
	double t107;
	double t109;
	double t11;
	double t118;
	double t12;
	double t126;
	double t127;
	double t14;
	double t141;
	double t145;
	double t146;
	double t147;
	double t15;
	double t150;
	double t152;
	double t159;
	double t16;
	double t162;
	double t165;
	double t168;
	double t170;
	double t172;
	double t175;
	double t18;
	double t180;
	double t185;
	double t187;
	double t19;
	double t192;
	double t194;
	double t2;
	double t206;
	double t21;
	double t216;
	double t22;
	double t227;
	double t23;
	double t230;
	double t233;
	double t235;
	double t237;
	double t240;
	double t245;
	double t25;
	double t250;
	double t252;
	double t257;
	double t259;
	double t27;
	double t271;
	double t28;
	double t281;
	double t293;
	double t294;
	double t296;
	double t299;
	double t3;
	double t30;
	double t302;
	double t303;
	double t305;
	double t306;
	double t309;
	double t324;
	double t325;
	double t327;
	double t330;
	double t331;
	double t347;
	double t35;
	double t350;
	double t37;
	double t4;
	double t43;
	double t49;
	double t5;
	double t51;
	double t52;
	double t54;
	double t56;
	double t6;
	double t61;
	double t65;
	double t7;
	double t70;
	double t75;
	double t76;
	double t81;
	double t82;
	double t87;
	double t88;
	double t9;
	double t93;
	double t94;
	double t98;
	{
		t1 = a[0];
		t2 = v[0];
		t3 = t2*t2;
		t4 = v[1];
		t5 = t4*t4;
		t6 = v[2];
		t7 = t6*t6;
		t9 = sqrt(1.0 - t3 - t5 - t7);
		t10 = 1 / t9;
		t11 = qr0[1];
		t12 = t11*t10;
		t14 = qr0[0];
		t15 = -t12*t2 + t14;
		t16 = M[0];
		t18 = qr0[2];
		t19 = t18*t10;
		t21 = qr0[3];
		t22 = -t19*t2 - t21;
		t23 = M[1];
		t25 = t10*t21;
		t27 = -t25*t2 + t18;
		t28 = M[2];
		t30 = -t15*t16 - t22*t23 - t27*t28;
		t35 = -t9*t11 - t2*t14 - t4*t21 + t6*t18;
		t37 = -t35;
		t43 = t9*t18 + t4*t14 + t6*t11 - t2*t21;
		t49 = t9*t21 + t6*t14 + t2*t18 - t11*t4;
		t51 = -t37*t16 - t43*t23 - t49*t28;
		t52 = -t15;
		t54 = t10*t14;
		t56 = -t54*t2 - t11;
		t61 = t9*t14 - t2*t11 - t4*t18 - t6*t21;
		t65 = t61*t16 + t43*t28 - t23*t49;
		t70 = t56*t16 + t22*t28 - t23*t27;
		t75 = t56*t23 + t27*t16 - t28*t15;
		t76 = -t49;
		t81 = t61*t23 + t49*t16 - t37*t28;
		t82 = -t27;
		t87 = t56*t28 + t23*t15 - t22*t16;
		t88 = -t43;
		t93 = t61*t28 + t37*t23 - t43*t16;
		t94 = -t22;
		t98 = a[4];
		t107 = t30*t88 + t94*t51 + t56*t81 + t61*t75 + t87*t35 + t93*t52 - t70*t76 - t82*t65;
		t109 = a[1];
		t118 = t30*t76 + t82*t51 + t56*t93 + t61*t87 + t70*t88 + t65*t94 - t35*t75 - t81*t52;
		t126 = t76*t51 + t61*t93 + t65*t88 - t81*t35 + t[2];
		t127 = 1 / t126;
		t141 = t51*t88 + t61*t81 + t93*t35 - t65*t76 + t[1];
		t145 = t126*t126;
		t146 = 1 / t145;
		t147 = (t1*(t35*t51 + t61*t65 + t81*t76 - t93*t88 + t[0]) + t98*t141 + t126*t109)*t146;
		jacmRT[0][0] = (t1*(t30*t35 + t52*t51 + t56*t65 + t61*t70 + t76*t75 + t81*t82 - t88*t87
			- t93*t94) + t98*t107 + t109*t118)*t127 - t118*t147;
		t150 = t1*a[3];
		t152 = a[2];
		t159 = (t150*t141 + t126*t152)*t146;
		jacmRT[1][0] = (t107*t150 + t152*t118)*t127 - t159*t118;
		t162 = -t12*t4 + t21;
		t165 = -t19*t4 + t14;
		t168 = -t25*t4 - t11;
		t170 = -t162*t16 - t165*t23 - t168*t28;
		t172 = -t162;
		t175 = -t54*t4 - t18;
		t180 = t175*t16 + t165*t28 - t168*t23;
		t185 = t175*t23 + t168*t16 - t162*t28;
		t187 = -t168;
		t192 = t175*t28 + t162*t23 - t165*t16;
		t194 = -t165;
		t206 = t170*t88 + t51*t194 + t175*t81 + t61*t185 + t192*t35 + t93*t172 - t76*t180 - t65*
			t187;
		t216 = t170*t76 + t51*t187 + t93*t175 + t61*t192 + t180*t88 + t65*t194 - t185*t35 - t81*
			t172;
		jacmRT[0][1] = (t1*(t170*t35 + t172*t51 + t175*t65 + t180*t61 + t185*t76 + t81*t187 -
			t192*t88 - t93*t194) + t98*t206 + t109*t216)*t127 - t147*t216;
		jacmRT[1][1] = (t150*t206 + t152*t216)*t127 - t159*t216;
		t227 = -t12*t6 - t18;
		t230 = -t19*t6 + t11;
		t233 = -t25*t6 + t14;
		t235 = -t227*t16 - t23*t230 - t233*t28;
		t237 = -t227;
		t240 = -t54*t6 - t21;
		t245 = t240*t16 + t230*t28 - t233*t23;
		t250 = t23*t240 + t233*t16 - t227*t28;
		t252 = -t233;
		t257 = t240*t28 + t227*t23 - t230*t16;
		t259 = -t230;
		t271 = t235*t88 + t51*t259 + t81*t240 + t61*t250 + t257*t35 + t93*t237 - t245*t76 - t65*
			t252;
		t281 = t235*t76 + t51*t252 + t240*t93 + t61*t257 + t245*t88 + t259*t65 - t250*t35 - t81*
			t237;
		jacmRT[0][2] = (t1*(t235*t35 + t237*t51 + t240*t65 + t61*t245 + t250*t76 + t81*t252 -
			t257*t88 - t93*t259) + t271*t98 + t281*t109)*t127 - t147*t281;
		jacmRT[1][2] = (t150*t271 + t281*t152)*t127 - t159*t281;
		jacmRT[0][3] = t127*t1;
		jacmRT[1][3] = 0.0;
		jacmRT[0][4] = t98*t127;
		jacmRT[1][4] = t150*t127;
		jacmRT[0][5] = t109*t127 - t147;
		jacmRT[1][5] = t152*t127 - t159;
		t293 = t35*t35;
		t294 = t61*t61;
		t296 = t88*t88;
		t299 = t35*t88;
		t302 = t61*t76;
		t303 = 2.0*t299 + t61*t49 - t302;
		t305 = t35*t76;
		t306 = t61*t88;
		t309 = t305 + 2.0*t306 - t49*t35;
		jacmS[0][0] = (t1*(t293 + t294 + t49*t76 - t296) + t98*t303 + t109*t309)*t127 - t147*
			t309;
		jacmS[1][0] = (t150*t303 + t152*t309)*t127 - t159*t309;
		t324 = t76*t76;
		t325 = t296 + t294 + t35*t37 - t324;
		t327 = t76*t88;
		t330 = t61*t35;
		t331 = 2.0*t327 + t61*t37 - t330;
		jacmS[0][1] = (t1*(t299 + 2.0*t302 - t37*t88) + t98*t325 + t109*t331)*t127 - t147*
			t331;
		jacmS[1][1] = (t150*t325 + t152*t331)*t127 - t159*t331;
		t347 = t327 + 2.0*t330 - t43*t76;
		t350 = t324 + t294 + t43*t88 - t293;
		jacmS[0][2] = (t1*(2.0*t305 + t61*t43 - t306) + t98*t347 + t350*t109)*t127 - t147*
			t350;
		jacmS[1][2] = (t150*t347 + t152*t350)*t127 - t159*t350;
		return;
	}
}
/* Given a parameter vector p made up of the 3D coordinates of n points, compute in
* hx the prediction of the measurements, i.e. the projections of 3D points in the m images. The measurements
* are returned in the order (hx_11^T, .. hx_1m^T, ..., hx_n1^T, .. hx_nm^T)^T, where hx_ij is the predicted
* projection of the i-th point on the j-th camera.
* Notice that depending on idxij, some of the hx_ij might be missing
*
*/
static void img_projsS_x(double *p, struct sba_crsm *idxij, int *rcidxs, int *rcsubs, double *hx, void *adata)
{
	register int i, j;
	int cnp, pnp, mnp;
	double *pqr, *pt, *ppt, *pmeas, *Kparms, *camparams, trot[FULLQUATSZ];
	//int n;
	int m, nnz;
	struct globs_ *gl;

	gl = (struct globs_ *)adata;
	cnp = gl->cnp; pnp = gl->pnp; mnp = gl->mnp;
	Kparms = gl->intrcalib;
	camparams = gl->camparams;

	//n=idxij->nr;
	m = idxij->nc;

	for (j = 0; j<m; ++j){
		/* j-th camera parameters */
		pqr = camparams + j*cnp;
		pt = pqr + 3; // quaternion vector part has 3 elements
		_MK_QUAT_FRM_VEC(trot, pqr);

		nnz = sba_crsm_col_elmidxs(idxij, j, rcidxs, rcsubs); /* find nonzero hx_ij, i=0...n-1 */

		for (i = 0; i<nnz; ++i){
			ppt = p + rcsubs[i] * pnp;
			pmeas = hx + idxij->val[rcidxs[i]] * mnp; // set pmeas to point to hx_ij

			calcImgProjFullR(Kparms, trot, pt, ppt, pmeas); // evaluate Q in pmeas
			//calcImgProj(Kparms, (double *)zerorotquat, pqr, pt, ppt, pmeas); // evaluate Q in pmeas
		}
	}
}
/* Given a parameter vector p made up of the 3D coordinates of n points and the parameters of m cameras, compute in
* hx the prediction of the measurements, i.e. the projections of 3D points in the m images. The measurements
* are returned in the order (hx_11^T, .. hx_1m^T, ..., hx_n1^T, .. hx_nm^T)^T, where hx_ij is the predicted
* projection of the i-th point on the j-th camera.
* Notice that depending on idxij, some of the hx_ij might be missing
*
*/
void img_projsKRTS_x(double *p, struct sba_crsm *idxij, int *rcidxs, int *rcsubs, double *hx, void *adata)
{
	register int i, j;
	int cnp, pnp, mnp;
	double *pa, *pb, *pqr, *pt, *ppt, *pmeas, *pcalib, *pr0, lrot[FULLQUATSZ], trot[FULLQUATSZ];
	//int n;
	int m, nnz;
	struct globs_ *gl;

	gl = (struct globs_ *)adata;
	cnp = gl->cnp; pnp = gl->pnp; mnp = gl->mnp;

	//n=idxij->nr;
	m = idxij->nc;
	pa = p; pb = p + m*cnp;

	for (j = 0; j<m; ++j){
		/* j-th camera parameters */
		pcalib = pa + j*cnp;
		pqr = pcalib + 5;
		pt = pqr + 3; // quaternion vector part has 3 elements
		pr0 = gl->rot0params + j*FULLQUATSZ; // full quat for initial rotation estimate
		_MK_QUAT_FRM_VEC(lrot, pqr);
		quatMultFast(lrot, pr0, trot); // trot=lrot*pr0

		nnz = sba_crsm_col_elmidxs(idxij, j, rcidxs, rcsubs); /* find nonzero hx_ij, i=0...n-1 */

		for (i = 0; i<nnz; ++i){
			ppt = pb + rcsubs[i] * pnp;
			pmeas = hx + idxij->val[rcidxs[i]] * mnp; // set pmeas to point to hx_ij

			calcImgProjFullR(pcalib, trot, pt, ppt, pmeas); // evaluate Q in pmeas
			//calcImgProj(pcalib, pr0, pqr, pt, ppt, pmeas); // evaluate Q in pmeas
		}
	}
}
/* Given a parameter vector p made up of the 3D coordinates of n points and the parameters of m cameras, compute in
* jac the jacobian of the predicted measurements, i.e. the jacobian of the projections of 3D points in the m images.
* The jacobian is returned in the order (A_11, ..., A_1m, ..., A_n1, ..., A_nm, B_11, ..., B_1m, ..., B_n1, ..., B_nm),
* where A_ij=dx_ij/db_j and B_ij=dx_ij/db_i (see HZ).
* Notice that depending on idxij, some of the A_ij, B_ij might be missing
*
*/
void img_projsKRTS_jac_x(double *p, struct sba_crsm *idxij, int *rcidxs, int *rcsubs, double *jac, void *adata)
{
	register int i, j, ii, jj;
	int cnp, pnp, mnp, ncK;
	double *pa, *pb, *pqr, *pt, *ppt, *pA, *pB, *pcalib, *pr0;
	//int n;
	int m, nnz, Asz, Bsz, ABsz;
	struct globs_ *gl;

	gl = (struct globs_ *)adata;
	cnp = gl->cnp; pnp = gl->pnp; mnp = gl->mnp;
	ncK = gl->nccalib;

	//n=idxij->nr;
	m = idxij->nc;
	pa = p; pb = p + m*cnp;
	Asz = mnp*cnp; Bsz = mnp*pnp; ABsz = Asz + Bsz;

	for (j = 0; j<m; ++j){
		/* j-th camera parameters */
		pcalib = pa + j*cnp;
		pqr = pcalib + 5;
		pt = pqr + 3; // quaternion vector part has 3 elements
		pr0 = gl->rot0params + j*FULLQUATSZ; // full quat for initial rotation estimate

		nnz = sba_crsm_col_elmidxs(idxij, j, rcidxs, rcsubs); /* find nonzero hx_ij, i=0...n-1 */

		for (i = 0; i<nnz; ++i){
			ppt = pb + rcsubs[i] * pnp;
			pA = jac + idxij->val[rcidxs[i]] * ABsz; // set pA to point to A_ij
			pB = pA + Asz; // set pB to point to B_ij

			calcImgProjJacKRTS(pcalib, pr0, pqr, pt, ppt, (double(*)[5 + 6])pA, (double(*)[3])pB); // evaluate dQ/da, dQ/db in pA, pB

			/* clear the columns of the Jacobian corresponding to fixed calibration parameters */
			if (ncK){
				int jj0 = 5 - ncK;

				for (ii = 0; ii<mnp; ++ii, pA += cnp)
				for (jj = jj0; jj<5; ++jj)
					pA[jj] = 0.0; // pA[ii*cnp+jj]=0.0;
			}
		}
	}
}
/* Given a parameter vector p made up of the 3D coordinates of n points and the parameters of m cameras, compute in
* hx the prediction of the measurements, i.e. the projections of 3D points in the m images. The measurements
* are returned in the order (hx_11^T, .. hx_1m^T, ..., hx_n1^T, .. hx_nm^T)^T, where hx_ij is the predicted
* projection of the i-th point on the j-th camera.
* Notice that depending on idxij, some of the hx_ij might be missing
*
*/
static void img_projsRTS_x(double *p, struct sba_crsm *idxij, int *rcidxs, int *rcsubs, double *hx, void *adata)
{
	register int i, j;
	int cnp, pnp, mnp;
	double *pa, *pb, *pqr, *pt, *ppt, *pmeas, *Kparms, *pr0, lrot[FULLQUATSZ], trot[FULLQUATSZ];
	//int n;
	int m, nnz;
	struct globs_ *gl;

	gl = (struct globs_ *)adata;
	cnp = gl->cnp; pnp = gl->pnp; mnp = gl->mnp;
	Kparms = gl->intrcalib;

	//n=idxij->nr;
	m = idxij->nc;
	pa = p; pb = p + m*cnp;

	for (j = 0; j<m; ++j){
		/* j-th camera parameters */
		pqr = pa + j*cnp;
		pt = pqr + 3; // quaternion vector part has 3 elements
		pr0 = gl->rot0params + j*FULLQUATSZ; // full quat for initial rotation estimate
		_MK_QUAT_FRM_VEC(lrot, pqr);
		quatMultFast(lrot, pr0, trot); // trot=lrot*pr0

		nnz = sba_crsm_col_elmidxs(idxij, j, rcidxs, rcsubs); /* find nonzero hx_ij, i=0...n-1 */

		for (i = 0; i<nnz; ++i){
			ppt = pb + rcsubs[i] * pnp;
			pmeas = hx + idxij->val[rcidxs[i]] * mnp; // set pmeas to point to hx_ij

			calcImgProjFullR(Kparms, trot, pt, ppt, pmeas); // evaluate Q in pmeas
			//calcImgProj(Kparms, pr0, pqr, pt, ppt, pmeas); // evaluate Q in pmeas
		}
	}
}
/* Given a parameter vector p made up of the 3D coordinates of n points, compute in
* jac the jacobian of the predicted measurements, i.e. the jacobian of the projections of 3D points in the m images.
* The jacobian is returned in the order (B_11, ..., B_1m, ..., B_n1, ..., B_nm),
* where B_ij=dx_ij/db_i (see HZ).
* Notice that depending on idxij, some of the B_ij might be missing
*
*/
static const double zerorotquat[FULLQUATSZ] = { 1.0, 0.0, 0.0, 0.0 };
static void img_projsS_jac_x(double *p, struct sba_crsm *idxij, int *rcidxs, int *rcsubs, double *jac, void *adata)
{
	register int i, j;
	int cnp, pnp, mnp;
	double *pqr, *pt, *ppt, *pB, *Kparms, *camparams;
	//int n;
	int m, nnz, Bsz;
	struct globs_ *gl;

	gl = (struct globs_ *)adata;
	cnp = gl->cnp; pnp = gl->pnp; mnp = gl->mnp;
	Kparms = gl->intrcalib;
	camparams = gl->camparams;

	//n=idxij->nr;
	m = idxij->nc;
	Bsz = mnp*pnp;

	for (j = 0; j<m; ++j){
		/* j-th camera parameters */
		pqr = camparams + j*cnp;
		pt = pqr + 3; // quaternion vector part has 3 elements

		nnz = sba_crsm_col_elmidxs(idxij, j, rcidxs, rcsubs); /* find nonzero hx_ij, i=0...n-1 */

		for (i = 0; i<nnz; ++i){
			ppt = p + rcsubs[i] * pnp;
			pB = jac + idxij->val[rcidxs[i]] * Bsz; // set pB to point to B_ij

			calcImgProjJacS(Kparms, (double *)zerorotquat, pqr, pt, ppt, (double(*)[3])pB); // evaluate dQ/da in pB
		}
	}
}
/* Given a parameter vector p made up of the 3D coordinates of n points and the parameters of m cameras, compute in
* jac the jacobian of the predicted measurements, i.e. the jacobian of the projections of 3D points in the m images.
* The jacobian is returned in the order (A_11, ..., A_1m, ..., A_n1, ..., A_nm, B_11, ..., B_1m, ..., B_n1, ..., B_nm),
* where A_ij=dx_ij/db_j and B_ij=dx_ij/db_i (see HZ).
* Notice that depending on idxij, some of the A_ij, B_ij might be missing
*
*/
static void img_projsRTS_jac_x(double *p, struct sba_crsm *idxij, int *rcidxs, int *rcsubs, double *jac, void *adata)
{
	register int i, j;
	int cnp, pnp, mnp;
	double *pa, *pb, *pqr, *pt, *ppt, *pA, *pB, *Kparms, *pr0;
	//int n;
	int m, nnz, Asz, Bsz, ABsz;
	struct globs_ *gl;

	gl = (struct globs_ *)adata;
	cnp = gl->cnp; pnp = gl->pnp; mnp = gl->mnp;
	Kparms = gl->intrcalib;

	//n=idxij->nr;
	m = idxij->nc;
	pa = p; pb = p + m*cnp;
	Asz = mnp*cnp; Bsz = mnp*pnp; ABsz = Asz + Bsz;

	for (j = 0; j<m; ++j){
		/* j-th camera parameters */
		pqr = pa + j*cnp;
		pt = pqr + 3; // quaternion vector part has 3 elements
		pr0 = gl->rot0params + j*FULLQUATSZ; // full quat for initial rotation estimate

		nnz = sba_crsm_col_elmidxs(idxij, j, rcidxs, rcsubs); /* find nonzero hx_ij, i=0...n-1 */

		for (i = 0; i<nnz; ++i){
			ppt = pb + rcsubs[i] * pnp;
			pA = jac + idxij->val[rcidxs[i]] * ABsz; // set pA to point to A_ij
			pB = pA + Asz; // set pB to point to B_ij

			calcImgProjJacRTS(Kparms, pr0, pqr, pt, ppt, (double(*)[6])pA, (double(*)[3])pB); // evaluate dQ/da, dQ/db in pA, pB
		}
	}
}
/* returns the number of cameras defined in a camera parameters file.
* Each line of the file corresponds to the parameters of a single camera
*/
int KeyFrame::findNcameras(vector<vector<int>> cornerIdx)
{
	return cornerIdx.size();
}
/* returns the number of doubles found in the first line of the supplied text file. rewinds file.
*/
int KeyFrame::countNDoubles(vector<vector<int>> cornerIdx)
{
	int np = 0;
	while (!cornerIdx.size() == NULL){
		for (int i = 0; i < cornerIdx.size(); i++){
			for (int j = 0; j < cornerIdx[i].size(); j++){
				if (cornerIdx[i][j] == 0){
					np++;
				}
			}
		}
		return 3 + np*3;
	}

	return 0; // should not reach this point
}
/* reads (from "fp") "nvals" doubles without storing them.
* Returns EOF on end of file, EOF-1 on error
*/
//static int skipNDoubles(FILE *fp, int nvals)
//{
//	register int i;
//	int j;
//
//	for (i = 0; i<nvals; ++i){
//		j = fscanf(fp, "%*f");
//		if (j == EOF) return EOF;
//
//		if (ferror(fp)) return EOF - 1;
//	}
//
//	return nvals;
//}
/* reads (from "fp") "nvals" doubles into "vals".
* Returns number of doubles actually read, EOF on end of file, EOF-1 on error
*/
//static int readNDoubles(FILE *fp, double *vals, int nvals)
//{
//	register int i;
//	int n, j;
//
//	for (i = n = 0; i < nvals; ++i){
//		j = fscanf(fp, "%lf", vals + i);
//		if (j == EOF) return EOF;
//
//		if (j != 1 || ferror(fp)) return EOF - 1;
//
//		n += j;
//	}
//
//	return n;
//}
/* reads (from "fp") ints into "vals".
* Returns number of ints actually read
*/
int KeyFrame::readNInts(vector<vector<int>> cornerIdx, int *vals, int *presentPoint, int *countFrame)
{
	register int i;
	int n, j;
	if (*countFrame > cornerIdx.size()){
		printf("err! countFrame is too big");
		return 0;
	}
	for (; *presentPoint < cornerIdx[*countFrame].size(); *countFrame++){
		*vals = cornerIdx[*countFrame][*presentPoint];
		*presentPoint++;
		return 1;
	}
	return 0;
}
/* determines the number of 3D points contained in a points parameter file as well as the
* total number of their 2D image projections across all images. Also decides if point
* covariances are being supplied. Each 3D point is assumed to be described by "pnp"
* parameters and its parameters & image projections are stored as a single line.
* The file format is
* X_0...X_{pnp-1}  nframes  frame0 x0 y0 [cov0]  frame1 x1 y1 [cov1] ...
* The portion of the line starting at "frame0" is ignored for all but the first line
*/
void KeyFrame::readNprojections(vector<vector<int>> cornerIdx, int *nprojs)
{
	int nframes = 0;
	*nprojs = 0;
	for (int i = 0; i < cornerIdx.size(); i++){
		nframes = cornerIdx[i].size();
		*nprojs += nframes;
	}
}
/* reads into "params" the camera parameters defined in a camera parameters file.
* "params" is assumed to point to sufficiently large memory.
* Each line contains the parameters of a single camera, "cnp" parameters per camera
*
* infilter points to a function that converts a motion parameters vector stored
* in a file to the format expected by eucsbademo. For instance, it can convert
* a four element quaternion to the vector part of its unit norm equivalent.
*/
void KeyFrame::readCameraParams(vector<cv::Mat> R, vector<cv::Mat> T, cv::Mat cameraMat, int cnp,
	double *params, double *initrot)
{
	int k = 0;
	//double cam1 = (double)cameraMat.at<float>(0, 0);
	//double cam2 = (double)cameraMat.at<float>(0, 2);
	//double cam3 = (double)cameraMat.at<float>(1, 2);

	while (k < R.size()){
		//params[0] = cam1;
		//params[1] = cam2;
		//params[2] = cam3;

		//params[3] = 1;
		//params[4] = 0;
			
		params[0] = (double)R[k].at<float>(0, 0);
		params[1] = (double)R[k].at<float>(1, 0);
		params[2] = (double)R[k].at<float>(2, 0);
			
		params[3] = (double)T[k].at<float>(0, 0);
		params[4] = (double)T[k].at<float>(1, 0);
		params[5] = (double)T[k].at<float>(2, 0);

		/* save rotation assuming the last 3 parameters correspond to translation */
		initrot[1] = params[cnp - 6];
		initrot[2] = params[cnp - 5];
		initrot[3] = params[cnp - 4];
		initrot[0] = sqrt(1.0 - initrot[1] * initrot[1] - initrot[2] * initrot[2] - initrot[3] * initrot[3]);

		params += cnp;
		initrot += FULLQUATSZ;

		k++;
	}
}
/* reads a points parameter file.
* "params", "projs" & "vmask" are assumed preallocated, pointing to
* memory blocks large enough to hold the parameters of 3D points,
* their projections in all images and the point visibility mask, respectively.
* Also, if "covprojs" is non-NULL, it is assumed preallocated and pointing to
* a memory block suitable to hold the covariances of image projections.
* Each 3D point is assumed to be defined by pnp parameters and each of its projections
* by mnp parameters. Optionally, the mnp*mnp covariance matrix in row-major order
* follows each projection. All parameters are stored in a single line.
*
* File format is X_{0}...X_{pnp-1}  nframes  frame0 x0 y0 [covx0^2 covx0y0 covx0y0 covy0^2] frame1 x1 y1 [covx1^2 covx1y1 covx1y1 covy1^2] ...
* with the parameters in angle brackets being optional. To save space, only the upper
* triangular part of the covariance can be specified, i.e. [covx0^2 covx0y0 covy0^2], etc
*/
void KeyFrame::readPointParamsAndProjections(cv::Mat corner3D, vector<cv::Mat> corner2D, vector<vector<int>> cornerIdx,
	double *params, int pnp, double *projs, double *covprojs, int havecov, int mnp, char *vmask, int ncams)
{
	int k = 0;
	int frameno = 0;
	int ptno = 0;
	
	while (k < corner3D.cols){
		*(params++) = (double)corner3D.at<float>(0, k);
		*(params++) = (double)corner3D.at<float>(1, k);
		*(params++) = (double)corner3D.at<float>(2, k);
		k++;
	}
	k = 0;
	/*while (k < corner2D.size()){
		for (int i = 0; i < corner2D[k].cols; i++){
			*(projs++) = (double)corner2D[k].at<float>(0, i);
			*(projs++) = (double)corner2D[k].at<float>(1, i);
		}
		k++;
	}*/

	for (k = 0; k < corner3D.cols; k++){
		for (int m = 0; m < cornerIdx.size(); m++){
			for (int j = 0; j < cornerIdx[m].size(); j++){
				if (k == cornerIdx[m][j]){
					*(projs++) = (double)corner2D[m].at<float>(0, j);
					*(projs++) = (double)corner2D[m].at<float>(1, j);
					//file2 << m << " " << corner2D[m].at<float>(0, j) << " " << corner2D[m].at<float>(1, j) << " ";
				}
			}
		}
	}
	k = 0;
	while (frameno < cornerIdx.size()){
		for (ptno = 0; ptno < cornerIdx[frameno].size(); ptno++){
			vmask[cornerIdx[frameno][ptno]*ncams + frameno] = 1;
		}
		frameno++;
	}
	covprojs = 0;
}
/* combines the above routines to read the initial estimates of the motion + structure parameters from text files.
* Also, it loads the projections of 3D points across images and optionally their covariances.
* The routine dynamically allocates the required amount of memory (last 4 arguments).
* If no covariances are supplied, *covimgpts is set to NULL
*/
void KeyFrame::readInitialSBAEstimate(vector<cv::Mat> R, vector<cv::Mat> T, cv::Mat corner3D, vector<cv::Mat> corner2D, 
	vector<vector<int>> cornerIdx, 	cv::Mat cameraMat, int cnp, int pnp, int mnp, int filecnp,
	int *ncams, int *n3Dpts, int *n2Dprojs,
	double **motstruct, double **initrot, double **imgpts, double **covimgpts, char **vmask)
{
	int havecov;

	if (corner2D.size() == 0){
		fprintf(stderr, "error! num of corner is zero");
	}

	*ncams = findNcameras(cornerIdx);
	*n3Dpts = corner3D.cols;
	havecov = NOCOV;
	readNprojections(cornerIdx, n2Dprojs);

	*motstruct = (double *)malloc((*ncams*cnp + *n3Dpts*pnp)*sizeof(double));
	if (*motstruct == NULL){
		fprintf(stderr, "memory allocation for 'motstruct' failed in readInitialSBAEstimate()\n");
		//exit(1);
	}
	*initrot = (double *)malloc((*ncams*FULLQUATSZ)*sizeof(double)); // Note: this assumes quaternions for rotations!
	if (*initrot == NULL){
		fprintf(stderr, "memory allocation for 'initrot' failed in readInitialSBAEstimate()\n");
		//exit(1);
	}
	cout << "2Dprojs" << *n2Dprojs << " mnp "<< mnp << endl;		//2Dprojs 고쳐라
	*imgpts = (double *)malloc(*n2Dprojs*mnp*sizeof(double));
	if (*imgpts == NULL){
		fprintf(stderr, "memory allocation for 'imgpts' failed in readInitialSBAEstimate()\n");
		//exit(1);
	}
	if (havecov){
		for (int k = 0; k < 100; k++){
			cout << covimgpts << endl;
		}
		*covimgpts = (double *)malloc(*n2Dprojs*mnp*mnp*sizeof(double));
		if (*covimgpts == NULL){
			fprintf(stderr, "memory allocation for 'covimgpts' failed in readInitialSBAEstimate()\n");
			//exit(1);
		}
	}
	else{
		*covimgpts = NULL;
	}
	*vmask = (char *)malloc(*n3Dpts * *ncams * sizeof(char));
	if (*vmask == NULL){
		fprintf(stderr, "memory allocation for 'vmask' failed in readInitialSBAEstimate()\n");
		//exit(1);
	}
	memset(*vmask, 0, *n3Dpts * *ncams * sizeof(char)); /* clear vmask */

	readCameraParams(R, T, cameraMat, cnp, *motstruct, *initrot);
	readPointParamsAndProjections(corner3D, corner2D, cornerIdx, *motstruct + *ncams*cnp, pnp, *imgpts, *covimgpts, havecov, mnp, *vmask, *ncams);
}
void KeyFrame::setSBAStructureData(double* mostruct, int ncams, int n3Dpts, int cnp){
	float* sbaCorner3DPointer = (float*)sbaCorner3D.data;
	float mostruct0 = (float)mostruct[ncams*cnp - 3];
	float mostruct1 = (float)mostruct[ncams*cnp - 2];
	float mostruct2 = (float)mostruct[ncams*cnp - 1];
	
	mostruct += ncams * cnp;

	cout << "mostruct0 : " << mostruct0 << ", " << mostruct1 << ", " << mostruct2 << endl;

	for (int i = 0; i < sbaCorner3D.cols; i++){
		//*(mostruct) = (float)*mostruct - mostruct0;
		sbaCorner3DPointer[0 * sbaCorner3D.cols + i] = (float)*(mostruct++);
		//*(mostruct) = (float)*mostruct - mostruct1;
		sbaCorner3DPointer[1 * sbaCorner3D.cols + i] = (float)*(mostruct++);
		//*(mostruct) = (float)*mostruct - mostruct2;
		sbaCorner3DPointer[2 * sbaCorner3D.cols + i] = (float)*(mostruct++);
	}
}
void KeyFrame::setSBAMotionData(double* mostruct, int ncams, int cnp){//마지막 모션 data만 챙기면됨.
	sbaR = cv::Mat(3, ncams, CV_32F);
	sbaT = cv::Mat(3, ncams, CV_32F);
	//mostruct += cnp*ncams - 6;
	cout << cnp << endl;
	float mostruct0 = (float)mostruct[ncams*cnp - 3];
	float mostruct1 = (float)mostruct[ncams*cnp - 2];
	float mostruct2 = (float)mostruct[ncams*cnp - 1];

	cout << "mostruct0 : " << mostruct0 <<", " <<mostruct1 << ", " << mostruct2 << endl;

	sbaRPointer = (float*)sbaR.data;
	float* sbaTPointer = (float*)sbaT.data;
	for (int i = 0; i < ncams; i++){
		sbaRPointer[0 * ncams + i] = (float)*(mostruct++);
		sbaRPointer[1 * ncams + i] = (float)*(mostruct++);
		sbaRPointer[2 * ncams + i] = (float)*(mostruct++);
		//*(mostruct) = (float)*mostruct - mostruct0;
		sbaTPointer[0 * ncams + i] = (float)*(mostruct++);
		//*(mostruct) = (float)*mostruct - mostruct1;
		sbaTPointer[1 * ncams + i] = (float)*(mostruct++);
		//*(mostruct) = (float)*mostruct - mostruct2;
		sbaTPointer[2 * ncams + i] = (float)*(mostruct++);
	}
}
/* Driver for sba_xxx_levmar */
int KeyFrame::sba_driver(vector<cv::Mat> R, vector<cv::Mat> T, cv::Mat corner3D, vector<cv::Mat> corner2D, vector<vector<int>> cornerIdx, cv::Mat cameraMat,
	int cnp, int pnp, int mnp,
	int filecnp, char *refcamsfname, char *refptsfname)
{
	double *motstruct, *motstruct_copy, *imgpts, *covimgpts, *initrot;
	double K[9], ical[5]; // intrinsic calibration matrix & temp. storage for its params
	char *vmask, tbuf[32];
	double opts[SBA_OPTSSZ], info[SBA_INFOSZ], phi;
	int expert, analyticjac, fixedcal, havedist, n, prnt, verbose = 0;
	int nframes, numpts3D, numprojs, nvars;
	const int nconstframes = 0;
	register int i;
	
#if PRINTDATA==1
	ofstream file, file2, file3, file4;
	file.open("output.txt");
	file2.open("outputPT.txt");
	file3.open("checkData.txt");
	file4.open("resultData.txt");
	for (int k = 0; k < R.size(); k++){
		file << cameraMat.at<float>(0, 0) << " " << cameraMat.at<float>(0, 2) << " " << cameraMat.at<float>(1, 2) << " " << "1.00000 0.0 ";
		float* v, q[FULLQUATSZ];
		v = (float*)R[k].data;
		_MK_QUAT_FRM_VEC(q, v);
		file << q[0] << " " << q[1] << " " << q[2] << " " << q[3] << " "
			<< T[k].at<float>(0, 0) << " " << T[k].at<float>(1, 0) << " " << T[k].at<float>(2, 0) << endl;
	}
	for (int k = 0; k < corner3D.cols; k++){
		file2 << corner3D.at<float>(0, k) << " " << corner3D.at<float>(1, k) << " " << corner3D.at<float>(2, k) << " ";
		int numOfFrame = 0;
		for (int m = 0; m < cornerIdx.size(); m++){
			for (int j = 0; j < cornerIdx[m].size(); j++){
				if (k == cornerIdx[m][j]){
					numOfFrame++;
				}
			}
		}
		if (numOfFrame == 0){
			cout << k << endl;
		}
		cout << numOfFrame << endl;
		file2 << numOfFrame << " ";
		for (int m = 0; m < cornerIdx.size(); m++){
			for (int j = 0; j < cornerIdx[m].size(); j++){
				if (k == cornerIdx[m][j]){
					file2 << m << " " << corner2D[m].at<float>(0, j) << " " << corner2D[m].at<float>(1, j) << " ";
				}
			}

		}
		file2 << "\n";
	}

	file2 << "camera Matrix : " <<
		"\n" << cameraMat.at<float>(0, 0) << "\t" << cameraMat.at<float>(0, 1) << "\t" << cameraMat.at<float>(0, 2) <<
		"\n" << cameraMat.at<float>(1, 0) << "\t" << cameraMat.at<float>(1, 1) << "\t" << cameraMat.at<float>(1, 2) <<
		"\n" << cameraMat.at<float>(2, 0) << "\t" << cameraMat.at<float>(2, 1) << "\t" << cameraMat.at<float>(2, 2) << endl;
	file2 << "R size : " << R.size() << "\nT size : " << T.size() << "\ncorner2D Size : " << corner2D.size() <<
		"\ncornerIdx size : " << cornerIdx.size() << "\ncorner3D size : " << corner3D.cols << endl;

	for (int k = 0; k < R.size(); k++){
		//file2 << "corner2D pts : " << corner2D[k].size() << "\ncornerIdx pts : " << cornerIdx[k].size() << endl;
		file2 << R[k].at<float>(0, 0) << " " << R[k].at<float>(1, 0) << " " << R[k].at<float>(2, 0) << endl;
		//file2 << T[k].at<float>(0, 0) << " " << T[k].at<float>(1, 0) << " " << T[k].at<float>(2, 0) << endl;
	}
	for (int k = 0; k < R.size(); k++){
		file << "frame Num : " << k << endl;
		for (int j = 0; j < cornerIdx[k].size(); j++){
			file << "cornerIdx : " << cornerIdx[k][j] << "\t" << corner2D[k].at<float>(0, j) << "\t" << corner2D[k].at<float>(1, j) << endl;
		}
	}
#endif

	clock_t start_time, end_time;
	/* Notice the various BA options demonstrated below */
	/* minimize motion & structure, motion only, or
	* motion and possibly motion & structure in a 2nd pass?
	*/
	expert = 1;
	/* analytic or approximate jacobian? */
	//analyticjac=0;
	analyticjac = 1;
	/* print motion & structure estimates,
	* motion only or structure only upon completion?
	*/
	prnt = BA_NONE;
	//prnt=BA_MOTSTRUCT;
	//prnt=BA_MOT;
	//prnt=BA_STRUCT;
	/*cout << corner3D << endl;
	cv::Mat corner3Dcopy = cv::Mat(corner3D.rows, corner3D.cols, CV_32F);
	for (int k = 0; k < corner3D.cols; k++){
	corner3Dcopy.at<float>(0, k) = -corner3D.at<float>(0, k);
	corner3Dcopy.at<float>(1, k) = corner3D.at<float>(2, k);
	corner3Dcopy.at<float>(2, k) = corner3D.at<float>(1, k);
	}
	cout << corner3Dcopy << endl;*/

	/* NOTE: readInitialSBAEstimate() sets covimgpts to NULL if no covariances are supplied */
	readInitialSBAEstimate(R, T, corner3D, corner2D, cornerIdx, cameraMat, cnp, pnp, mnp, filecnp, //NULL, 0, 
		&nframes, &numpts3D, &numprojs, &motstruct, &initrot, &imgpts, &covimgpts, &vmask);

	//printSBAData(stdout, motstruct, cnp, pnp, mnp, camoutfilter, filecnp, nframes, numpts3D, imgpts, numprojs, vmask);

#if MOTSTRUCT==1
	/* initialize the local rotation estimates to 0, corresponding to local quats (1, 0, 0, 0) */
	for (i = 0; i < nframes; ++i){		//motion& structure
		register int j;

		j = (i + 1)*cnp; // note the +1, below we move from right to left, assuming 3 parameters for the translation!
		//motstruct[j - 4] = motstruct[j - 5] = motstruct[j - 6] = 0.0; // clear rotation
	}
#endif

	/* note that if howto==BA_STRUCT the rotation parts of motstruct actually equal the initial rotations! */

	/* set up globs structure */
	globs.cnp = cnp; globs.pnp = pnp; globs.mnp = mnp;
	globs.rot0params = initrot;

	ical[0] = (double)cameraMat.at<float>(0, 0); // fu
	ical[1] = (double)cameraMat.at<float>(0, 2); // u0
	ical[2] = (double)cameraMat.at<float>(1, 2); // v0
	ical[3] = (double)cameraMat.at<float>(1, 1) / cameraMat.at<float>(0, 0); // ar
	ical[4] = (double)cameraMat.at<float>(0, 1); // s
	globs.intrcalib = ical;
	fixedcal = 1; /* use fixed intrinsics */
	havedist = 0;
	cout << ical[0] << "\t" << ical[1] << "\t" << ical[2] << "\t" << ical[3] << "\t" << ical[4] << "\t" << endl;
	globs.ptparams = NULL;
	globs.camparams = NULL;

	/* call sparse LM routine */
	opts[0] = SBA_INIT_MU; opts[1] = SBA_STOP_THRESH; opts[2] = SBA_STOP_THRESH;
	opts[3] = SBA_STOP_THRESH;
	//opts[3]=0.05*numprojs; // uncomment to force termination if the average reprojection error drops below 0.05
	opts[4] = 0.0;
	//opts[4]=1E-05; // uncomment to force termination if the relative reduction in the RMS reprojection error drops below 1E-05

	start_time = clock();
#if MOTSTRUCT==1
	nvars = nframes*cnp + numpts3D*pnp;
#endif
#if MOTSTRUCT==0
	globs.camparams = motstruct;
	nvars = numpts3D*pnp;
#endif

#if PRINTDATA==1
	file3 << "numpts3D : " << numpts3D << "nframes : " << nframes << "nconstframes : " << nconstframes << endl;
	file3 << "cnp : " << cnp << "pnp : " << pnp << "mnp : " << mnp << endl;
	file3 << "vmask" << endl;
	for (int k = 0; k < numpts3D * nframes; k++){
		file3 << vmask[k] << ", ";
		if (k % nframes == (nframes - 1)){
			file3 << endl;
		}
	}
	file3 << "motstruct" << endl;
	for (int k = 0; k < nframes*cnp + numpts3D*pnp; k++){
		file3 << motstruct[k] << ", ";
		if (k < nframes*cnp){
			if (k % cnp == (cnp - 1)){
				file3 << endl;
			}
		}
		if (k > nframes * cnp){
			if ((k - nframes*cnp) % pnp == (pnp - 1)){
				file3 << endl;
			}
		}
	}
	file3 << "initrot" << endl;
	for (int k = 0; k < nframes * 4; k++){
		file3 << initrot[k] << ", ";
		if (k % 4 == 3){
			file3 << endl;
		}
	}
	file3 << "rotation" << endl;
	for (int k = 0; k < R.size(); k++){
		file3 << R[k].at<float>(0, 0) << ", " << R[k].at<float>(1, 0) << ", " << R[k].at<float>(2, 0) << endl;
	}
	file3 << "imgpts" << endl;
	for (int k = 0; k < numprojs * mnp; k++){
		file3 << imgpts[k] << ", ";
		if (k % 2 == 1){
			file3 << endl;
		}
	}
#endif

#if MOTSTRUCT==1
	n = sba_motstr_levmar_x(numpts3D, 0, nframes, nconstframes, vmask, motstruct, cnp, pnp, imgpts, covimgpts, mnp,
		img_projsRTS_x, img_projsRTS_jac_x, (void *)(&globs), 2*MAXITER2, verbose, opts, info);
#endif
#if MOTSTRUCT==0;
	n = sba_str_levmar_x(numpts3D, 0, nframes, vmask, motstruct + nframes*cnp, pnp, imgpts, covimgpts, mnp,
		img_projsS_x, img_projsS_jac_x, (void *)(&globs), 50, verbose, opts, info);
#endif
	end_time = clock();
	cout << "n : " << n << endl;
	if (n == SBA_ERROR) goto cleanup;

	/* combine the local rotation estimates with the initial ones */
	for (i = 0; i < nframes; ++i){
		double *v, qs[FULLQUATSZ], *q0, prd[FULLQUATSZ];

		/* retrieve the vector part */
		v = motstruct + (i + 1)*cnp - 6; // note the +1, we access the motion parameters from the right, assuming 3 for translation!
		_MK_QUAT_FRM_VEC(qs, v);

		q0 = initrot + i*FULLQUATSZ;
		quatMultFast(qs, q0, prd); // prd=qs*q0

		/* copy back vector part making sure that the scalar part is non-negative */
		if (prd[0] >= 0.0){
			v[0] = prd[1];
			v[1] = prd[2];
			v[2] = prd[3];
		}
		else{ // negate since two quaternions q and -q represent the same rotation
			v[0] = -prd[1];
			v[1] = -prd[2];
			v[2] = -prd[3];
		}
	}
	fflush(stdout);
	fprintf(stdout, "SBA using %d 3D pts, %d frames and %d image projections, %d variables\n", numpts3D, nframes, numprojs, nvars);
	if (!fixedcal) fprintf(stdout, " (%d fixed)", globs.nccalib);
	fputs("\n\n", stdout);
	fprintf(stdout, "SBA returned %d in %g iter, reason %g, error %g [initial %g], %d/%d func/fjac evals, %d lin. systems\n", n,
		info[5], info[6], info[1] / numprojs, info[0] / numprojs, (int)info[7], (int)info[8], (int)info[9]);
	fprintf(stdout, "Elapsed time: %.2lf seconds, %.2lf msecs\n", ((double)(end_time - start_time)) / CLOCKS_PER_SEC,
		((double)(end_time - start_time)) / CLOCKS_PER_MSEC);
	fflush(stdout);

	if (n != -1){
		sbaCorner3D = cv::Mat(corner3D.rows, corner3D.cols, CV_32F);
		//float* sbaCorner3DPointer = (float*)sbaCorner3D.data;
		setSBAMotionData(motstruct, nframes, cnp);
		setSBAStructureData(motstruct, nframes, numpts3D, cnp);
	}

#if PRINTDATA==1
	file4 << "numpts3D : " << numpts3D << "nframes : " << nframes << "nconstframes : " << nconstframes << endl;
	file4 << "cnp : " << cnp << "pnp : " << pnp << "mnp : " << mnp << endl;
	file4 << "vmask" << endl;
	for (int k = 0; k < numpts3D * nframes; k++){
		file4 << vmask[k] << ", ";
		if (k % nframes == (nframes - 1)){
			file4 << endl;
		}
	}
	file4 << "motstruct" << endl;
	file4 << motstruct[nframes*cnp] << endl;
	for (int k = 0; k < nframes*cnp + numpts3D*pnp; k++){
		file4 << motstruct[k] << ", ";
		if (k < nframes*cnp){
			if (k % cnp == (cnp - 1)){
				file4 << endl;
			}
		}
		if (k > nframes * cnp){
			if ((k - nframes*cnp) % pnp == (pnp - 1)){
				file4 << endl;
			}
		}
	}
	file4 << "initrot" << endl;
	for (int k = 0; k < nframes * 4; k++){
		file4 << initrot[k] << ", ";
		if (k % 4 == 3){
			file4 << endl;
		}
	}
	file4 << "rotation" << endl;
	for (int k = 0; k < R.size(); k++){
		file4 << R[k].at<float>(0, 0) << ", " << R[k].at<float>(1, 0) << ", " << R[k].at<float>(2, 0) << endl;
	}
	file4 << "imgpts" << endl;
	for (int k = 0; k < numprojs * mnp; k++){
		file4 << imgpts[k] << ", ";
		if (k % 2 == 1){
			file4 << endl;
		}
	}

	file.close();
	file2.close();
	file3.close();
	file4.close();
#endif

	return n;

	//file << "error : " << info[1] / numprojs << "initial error : " << info[0] / numprojs << endl;
	/* refined motion and structure are now in motstruct */
	/*switch (prnt){
	case BA_NONE:
		break;

	case BA_MOTSTRUCT:
		if (!refcamsfname || refcamsfname[0] == '-') fp = stdout;
		else if ((fp = fopen(refcamsfname, "w")) == NULL){
			fprintf(stderr, "error opening file %s for writing in sba_driver()!\n", refcamsfname);
			exit(1);
		}
		printSBAMotionData(fp, motstruct, nframes, cnp, camoutfilter, filecnp);
		if (fp != stdout) fclose(fp);

		if (!refptsfname || refptsfname[0] == '-') fp = stdout;
		else if ((fp = fopen(refptsfname, "w")) == NULL){
			fprintf(stderr, "error opening file %s for writing in sba_driver()!\n", refptsfname);
			exit(1);
		}
		printSBAStructureData(fp, motstruct, nframes, numpts3D, cnp, pnp);
		if (fp != stdout) fclose(fp);
		break;

	case BA_MOT:
		if (!refcamsfname || refcamsfname[0] == '-') fp = stdout;
		else if ((fp = fopen(refcamsfname, "w")) == NULL){
			fprintf(stderr, "error opening file %s for writing in sba_driver()!\n", refcamsfname);
			exit(1);
		}
		printSBAMotionData(fp, motstruct, nframes, cnp, camoutfilter, filecnp);
		if (fp != stdout) fclose(fp);
		break;

	case BA_STRUCT:
		if (!refptsfname || refptsfname[0] == '-') fp = stdout;
		else if ((fp = fopen(refptsfname, "w")) == NULL){
			fprintf(stderr, "error opening file %s for writing in sba_driver()!\n", refptsfname);
			exit(1);
		}
		printSBAStructureData(fp, motstruct, nframes, numpts3D, cnp, pnp);
		if (fp != stdout) fclose(fp);
		break;

	default:
		fprintf(stderr, "unknown print option \"%d\" in sba_driver()!\n", prnt);
		exit(1);
	}*/

	//printSBAData(stdout, motstruct, cnp, pnp, mnp, camoutfilter, filecnp, nframes, numpts3D, imgpts, numprojs, vmask);

cleanup:
	/* just in case... */
	globs.intrcalib = NULL;
	globs.nccalib = 0;
	globs.ncdist = 0;

	free(motstruct);
	free(imgpts);
	free(initrot); 
	globs.rot0params = NULL;
	if (covimgpts) free(covimgpts);
	free(vmask);
	return n;
}


KeyFrame::KeyFrame(){
	//cvSba.setParams(cvsba::Sba::Params(cvsba::Sba::MOTIONSTRUCTURE, 200, 0.005, 5, 5, false));
}
KeyFrame::~KeyFrame(){
}
void KeyFrame::setImageData(cv::Mat inputR, cv::Mat inputT, cv::Mat inputCorner3D, cv::Mat inputCorner2D, vector<int> inputCornerIdx){
	R.push_back(rvecTo3Quat(inputR.clone()));
	T.push_back(inputT.clone());
	corner3D = inputCorner3D.clone();
	corner2D.push_back(inputCorner2D.clone());
	cornerIdx.push_back(inputCornerIdx);
	cout << "KeyFrame inputCorner2D size  : " << inputCorner2D.cols << endl;
	cout << "KeyFrame inputCorneridx size : " << inputCornerIdx.size() << endl;
}
int KeyFrame::runSBA(){
	int cnp = 6, pnp = 3, mnp = 2;
	cout << "runSBAAAAAAAAA!!!!!!!!!!!!!!!!!!!!!!" << endl;
	int k = sba_driver(R, T, corner3D, corner2D, cornerIdx, cameraMat, cnp, pnp, mnp, cnp+1, "-", "-");
	return k;
	//int number_of_points = corner3D.cols;
	//int ncon = 0;
	//int number_of_images = R.size();
	//int mcon = 0;
	//char* visibility_mask;
	//memset(visibility_mask, 0, sizeof(char) * number_of_images * number_of_points);
	//int* temp_mask_check;
	//memset(temp_mask_check, 0, sizeof(int)* number_of_images);

	//for (int i = 0; i < number_of_images * number_of_points; i++){
	//	int l = i / number_of_images;//point idx
	//	int m = i % number_of_images;//image idx
	//	
	//	if (temp_mask_check[m] != -1){
	//		if (cornerIdx[m][temp_mask_check[m]] = l){
	//			visibility_mask[i] = 1;
	//			temp_mask_check[m]++;
	//		}
	//		else if (cornerIdx[m][temp_mask_check[m]] > l){
	//			visibility_mask[i] = 0;
	//		}
	//		if (temp_mask_check[m] >= cornerIdx[m].size()){
	//			temp_mask_check[m] = -1;
	//		}
	//	}
	//	else{
	//		visibility_mask[i] = 0;
	//	}
	//}
	//int cnp = 6;
	//int pnp = 3;
	//double* p;
	//memset(p, 0, sizeof(double)* (number_of_images * 6 + number_of_points * 3));
	//{
	//	int i = 0;
	//	for (int j = 0; j < number_of_images; j++){
	//		p[i++] = R[j].at<float>(0, 0);
	//		p[i++] = R[j].at<float>(1, 0);
	//		p[i++] = R[j].at<float>(2, 0);
	//		p[i++] = T[j].at<float>(0, 0);
	//		p[i++] = T[j].at<float>(1, 0);
	//		p[i++] = T[j].at<float>(2, 0);
	//	}
	//	for (int j = 0; j < number_of_points; j++){
	//		p[i++] = corner3D.at<float>(0, j);
	//		p[i++] = corner3D.at<float>(1, j);
	//		p[i++] = corner3D.at<float>(2, j);
	//	}
	//}
	//int mnp = 2;

	////sba_driver();

	//free(temp_mask_check);
	//free(visibility_mask);
	//free(p);
}
void KeyFrame::setCameraMat(cv::Mat inputCameraMat){
	cameraMat = inputCameraMat.clone();
}
cv::Mat KeyFrame::getSbaR(int colum){
	float q0, q1, q2, q3 = 0.0f;
	float theta = 0.0f;
	float v1, v2, v3 = 0.0f;
	q1 = sbaRPointer[0 * sbaR.cols + colum];
	q2 = sbaRPointer[1 * sbaR.cols + colum];
	q3 = sbaRPointer[2 * sbaR.cols + colum];
	q0 = sqrtf(1.0 - q1 * q1 - q2 * q2 - q3 * q3);

	cv::Mat_<float> quatToRotMat(3, 3); 
	quatToRotMat <<
		1-2*q2*q2-2*q3*q3, 2*q1*q2-2*q3*q0, 2*q1*q3+2*q2*q0,
		2*q1*q2+2*q3*q0, 1-2*q1*q1-2*q3*q3, 2*q2*q3-2*q1*q0,
		2*q1*q3-2*q2*q0, 2*q1*q3+2*q1*q0, 1-2*q1*q1-2*q2*q2;
	cv::Mat rvec;
	cv::Rodrigues(quatToRotMat, rvec);

	return rvec;
}
/*cv::Mat KeyFrame::getSbaR(int colum){
	float q0, q1, q2, q3 = 0.0f;
	float theta = 0.0f;
	float v1, v2, v3 = 0.0f;
	q1 = sbaRPointer[0 * sbaR.cols + colum];
	q2 = sbaRPointer[1 * sbaR.cols + colum];
	q3 = sbaRPointer[2 * sbaR.cols + colum];
	q0 = sqrtf(1.0 - q1 * q1 - q2 * q2 - q3 * q3);
	
	theta = 2 * asinf(sqrtf(q1*q1 + q2*q2 + q3*q3));
	q1 = q1 / sinf(theta / 2);
	q2 = q2 / sinf(theta / 2);
	q3 = q3 / sinf(theta / 2);

	cv::Mat quatToRotMat = (cv::Mat_<float>(3, 3) <<
		cosf(theta) + q1*q1*(1 - cosf(theta)), q1*q2*(1 - cosf(theta)) - q3*sinf(theta), q1*q3*(1 - cosf(theta)) + q2*sinf(theta),
		q2*q1*(1 - cosf(theta)) + q3*sinf(theta), cosf(theta) + q2*q2*(1 - cosf(theta)), q2*q3*(1 - cosf(theta)) - q1*sinf(theta),
		q3*q1*(1 - cosf(theta)) - q2*sinf(theta), q3*q2*(1 - cosf(theta)) + q1*sinf(theta), cosf(theta) + q3*q3*(1 - cosf(theta)));
	cv::Mat rvec;
	cv::Rodrigues(quatToRotMat, rvec);

	return rvec;
}*/
cv::Mat KeyFrame::rvecTo3Quat(cv::Mat rvec){//0 1 2
	float theta, q0, q1, q2, q3 = 0.0f;		//3 4 5
	cv::Mat Quat = cv::Mat(3, 1, CV_32F);	//6 7 8
	cv::Mat_<float> RotationMat;
	cv::Rodrigues(rvec, RotationMat);

	float* RotationMatPointer = (float*)RotationMat.data;
	q0 = sqrtf(1 + RotationMatPointer[0] + RotationMatPointer[4] + RotationMatPointer[8]) / 2;
	q1 = (RotationMatPointer[7] - RotationMatPointer[5]) / (4 * q0);
	q2 = (RotationMatPointer[2] - RotationMatPointer[6]) / (4 * q0);
	q3 = (RotationMatPointer[3] - RotationMatPointer[1]) / (4 * q0);
	
	/*float mag, sg = 0.0f;
	mag = sqrtf(q0*q0 + q1*q1 + q2*q2 + q3*q3);
	sg = (q0 >= 0.0) ? 1.0 : -1.0;
	mag = sg / mag;*/
	Quat.at<float>(0, 0) = q1;
	Quat.at<float>(1, 0) = q2;
	Quat.at<float>(2, 0) = q3;

	return Quat;
}
/**cv::Mat KeyFrame::rvecTo3Quat(cv::Mat rvec){
	float theta, q0, q1, q2, q3 = 0.0f;
	cv::Mat Quat = cv::Mat(3, 1, CV_32F);
	float* rvecPointer = (float*)rvec.data;
	theta = sqrtf(rvecPointer[0] * rvecPointer[0] + rvecPointer[1] * rvecPointer[1] + rvecPointer[2] * rvecPointer[2]);
	q0 = cosf(theta / 2);
	q1 = sinf(theta / 2) * rvecPointer[0] / theta;
	q2 = sinf(theta / 2) * rvecPointer[1] / theta;
	q3 = sinf(theta / 2) * rvecPointer[2] / theta;

	float mag, sg = 0.0f;
	mag = sqrtf(q0*q0 + q1*q1 + q2*q2 + q3*q3);
	sg = (q0 >= 0.0) ? 1.0 : -1.0;
	mag = sg / mag;
	Quat.at<float>(0, 0) = q1 * mag;
	Quat.at<float>(1, 0) = q2 * mag;
	Quat.at<float>(2, 0) = q3 * mag;

	return Quat;
}*/

