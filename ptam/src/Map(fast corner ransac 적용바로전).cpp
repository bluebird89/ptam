#include "Map.h"

Map::Map(){
	initFrame = false;
	track = false;
	matchNum = 0;
	resultRansacCount = 0;
	resultRansacError = 1000000;
	/*cameraMat = (cv::Mat_<float>(4, 4) <<
		518.4f, 0.0f, 312.9f, 0.0f,
		0.0f, 518.4f, 263.1f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);
	cv::invert(cameraMat, inverseCameraMat);
	calibR = (cv::Mat_<float>(3, 3) <<
		1.0f, -0.02f, 0.014f,
		0.02f, 1.0f, -0.01f,
		-0.011f, 0.011f, 1.0f);
	calibT = (cv::Mat_<float>(3, 1) <<
		0.0253f,
		0.00037f,
		-0.00252f);*/
}
Map::~Map(){
	initPyramid[0].~GPyramid();
	initPyramid[1].~GPyramid();
	matcher.~BFMatcher();
}
void Map::setProperty(cv::Mat calibMat){
	cameraMat = cv::Mat(4, 4, CV_32F);
	for (int i = 0; i < 3; i++){
		cameraMat.at<float>(i, 0) = calibMat.at<float>(i, 0);
		cameraMat.at<float>(i, 1) = calibMat.at<float>(i, 1);
		cameraMat.at<float>(i, 2) = calibMat.at<float>(i, 2);
		cameraMat.at<float>(i, 3) = 0;
	}
	cameraMat.at<float>(3, 0) = 0.0f;
	cameraMat.at<float>(3, 1) = 0.0f;
	cameraMat.at<float>(3, 2) = 0.0f;
	cameraMat.at<float>(3, 3) = 1.0f;
	cv::invert(cameraMat, inverseCameraMat);
}
void Map::initMap(cv::Mat grayImage, cv::Mat depthImage){
	clearSuccessFeature();
	if (initFrame == false){
		matchNum = 0;//초기화
		matches[0].clear();
		matches[1].clear();
		matches[2].clear();
		matches[3].clear();
		clearSuccessFeature();

		initPyramid[0].run(grayImage, depthImage);
		//cv::imshow("check", initPyramid[0].getGrayMat(0));

		initFrame = true;
		track = true;
	}
	else{
		initPyramid[1].run(grayImage, depthImage);
		//double t = (double)cv::getTickCount();		//0.003~0.01s
		{
			int query = 0;
			int train = 0;
			matchNum = 0;
			for (int i = 0; i < 1; i++){		//일단 640*480사이즈 이미지에서만 실행
				if (initPyramid[0].getDescriptor(i).size>0 && initPyramid[1].getDescriptor(i).size > 0
					&& initPyramid[0].getfastCorner(i).size() > 0 && initPyramid[1].getfastCorner(i).size() > 0){
					matcher.match(initPyramid[0].getDescriptor(i), initPyramid[1].getDescriptor(i), matches[i]);

					vector <cv::KeyPoint> temp0 = initPyramid[0].getfastCorner(i);
					vector <cv::KeyPoint> temp1 = initPyramid[1].getfastCorner(i);
					vector <cv::KeyPoint> queryFastRest;

					int matchIdx[100] = { NULL };

					
					/*----------------------------------Ransac 해서 평균 구해서 길이 10퍼이상 차이나는거 제거 -------------------------*/
					double t = (double)cv::getTickCount();
					vector <int> usingMatch1;
					vector <int> ran6Point;

					float meanRansac = 0.0f;
					float tempMean = 0.0f;
					float threshold = 0.0f;
					int count = 0;
					int tempCount = 0;
					for (int m = 0; m < 10; m++){
						ran6Point = random6PointSelect(matches[i].size());
						for (int j = 0; j < 6; j++){
							tempMean += matches[i][ran6Point[j]].distance;
						}
						tempMean /= 6;
						threshold = tempMean / 10.0f;
						for (int j = 0; j < matches[i].size(); j++){
							if (fabsf(matches[i][j].distance - tempMean) < threshold){
								tempCount++;
							}
						}
						if (count < tempCount){
							meanRansac = tempMean;
							count = tempCount;
						}
						tempCount = 0;
						tempMean = 0.0f;
					}
					for (int k = 0; k < matches[i].size(); k++){
						if (fabsf(matches[i][k].distance - meanRansac) < threshold){
							usingMatch1.push_back(k);
						}
					}
					cout << "ransac count 1 :" << usingMatch1.size() << endl;
					/*---------------------------------Ransac 해서 기울기 10퍼이상 차이나는거 제거-------------------------------------*/
					vector <int> usingMatch2;
					vector <float> delta;

					float tempX, tempY = 0.0f;
					float tempDelta = 0.0f;
					for (int k = 0; k < usingMatch1.size(); k++){
						tempX = temp0[matches[i][usingMatch1[k]].queryIdx].pt.x - temp1[matches[i][usingMatch1[k]].trainIdx].pt.x;
						tempY = temp0[matches[i][usingMatch1[k]].queryIdx].pt.y - temp1[matches[i][usingMatch1[k]].trainIdx].pt.y;
						tempDelta = atan2f(tempY, tempX) * 57.296;
						delta.push_back(tempDelta);
					}
					tempDelta = 0.0f;
					count = 0;
					tempCount = 0;
					meanRansac = 0.0f;
					for (int k = 0; k < 20; k++){
						ran6Point = random6PointSelect(usingMatch1.size());
						for (int j = 0; j < 6; j++){
							tempDelta += delta[ran6Point[j]];
						}
						tempDelta /= 6;
						threshold = 40.0f;
						for (int j = 0; j < usingMatch1.size(); j++){
							if (fabsf(delta[j] - tempDelta) < threshold){
								tempCount++;
							}
						}
						if (count < tempCount){
							meanRansac = tempDelta;
							count = tempCount;
						}
						tempCount = 0;
						tempDelta = 0.0f;
					}
					for (int k = 0; k < usingMatch1.size(); k++){
						if (fabsf(delta[k] - meanRansac) < threshold){
							usingMatch2.push_back(usingMatch1[k]);
						}
					}
					cout << "ransac count 2 :" << usingMatch2.size() << endl;




					t = ((double)cv::getTickCount() - t) / cv::getTickFrequency();
					cout << t << "s" << endl;
					/*-----------------------------여기까지 제거함---------------------------------------------------------------------*/

					for (int k = 0; k < matches[i].size(); k++){
						query = matches[i][k].queryIdx;
						train = matches[i][k].trainIdx;

						if (abs(temp0[query].pt.y - temp1[train].pt.y) < 30.0f){
							trackSuccessFeature[0][i].push_back(cv::Point2f(temp0[query].pt.x, temp0[query].pt.y));
							trackSuccessFeature[1][i].push_back(cv::Point2f(temp1[train].pt.x, temp1[train].pt.y));

							queryFastRest.push_back(temp0[query]);
							matchNum++;
						}
					}
					//initPyramid[0].setFastCorner(queryFastRest, i);
				}
			}
			initPyramid[0].Brisk();
			cout << matchNum << endl;
			if (matchNum < 10){
				initFrame = false;
				track = false;
			}
		}
		
		//cv::imshow("check", initPyramid[0].getGrayMat(0));
	}
}
void Map::genMap(){
	for (int i = 0; i < 1; i++){				//일단 하나만하자.
		cv::Mat initPoint3fTemp = cv::Mat(4, trackSuccessFeature[0][i].size(), CV_32F);					//여기까지 init point3f Mat 형태로 받아옴
		cv::Mat initDepthMat = initPyramid[0].getDepthMat(i);

		cv::Mat trackPoint3fTemp = cv::Mat(4, trackSuccessFeature[1][i].size(), CV_32F);					//여기까지 track point3f Mat 형태로 받아옴
		cv::Mat trackDepthMat = initPyramid[1].getDepthMat(i);

		float* initPoint3fTempData = (float*)initPoint3fTemp.data;
		float* trackPoint3fTempData = (float*)trackPoint3fTemp.data;

		float depth1 = 0;
		float depth2 = 0;
		int count = 0;
		float focalX = cameraMat.at<float>(0, 0);
		float focalY = cameraMat.at<float>(1, 1);
		vector <int> getSuccessDepth[2];
		
		int trackSuccessCount[4];
		trackSuccessCount[0] = trackSuccessFeature[0][i].size();
		
		//double t = (double)cv::getTickCount();
		//t = ((double)cv::getTickCount() - t) / cv::getTickFrequency();
		//cout << t << "s" << endl;

		for (int j = 0; j < trackSuccessFeature[0][i].size(); j++){						////여기서부터 체크 다시하고 opengl로 넘어가자.
			depth1 = getDepth(initDepthMat, trackSuccessFeature[0][i][j].y, trackSuccessFeature[0][i][j].x);			//depthMat를 그림으로 주지말고 vertex로 줘라 그러면 해결
			depth2 = getDepth(trackDepthMat, trackSuccessFeature[1][i][j].y, trackSuccessFeature[1][i][j].x);
			if (depth1 != 32000.0f && depth2 != 32000.0f){
				initPoint3fTempData[0 * trackSuccessCount[i] + count] = trackSuccessFeature[0][i][j].x * depth1;
				initPoint3fTempData[1 * trackSuccessCount[i] + count] = trackSuccessFeature[0][i][j].y * depth1;
				initPoint3fTempData[2 * trackSuccessCount[i] + count] = depth1;
				initPoint3fTempData[3 * trackSuccessCount[i] + count] = 1;

				trackPoint3fTempData[0 * trackSuccessCount[i] + count] = trackSuccessFeature[1][i][j].x * depth1;
				trackPoint3fTempData[1 * trackSuccessCount[i] + count] = trackSuccessFeature[1][i][j].y * depth1;
				trackPoint3fTempData[2 * trackSuccessCount[i] + count] = depth1;
				trackPoint3fTempData[3 * trackSuccessCount[i] + count] = 1;

				/*initPoint3fTemp.at<float>(0, count) = trackSuccessFeature[0][i][j].x * depth1;
				initPoint3fTemp.at<float>(1, count) = trackSuccessFeature[0][i][j].y * depth1;
				initPoint3fTemp.at<float>(2, count) = depth1;
				initPoint3fTemp.at<float>(3, count) = 1;

				trackPoint3fTemp.at<float>(0, count) = trackSuccessFeature[1][i][j].x * depth2;
				trackPoint3fTemp.at<float>(1, count) = trackSuccessFeature[1][i][j].y * depth2;
				trackPoint3fTemp.at<float>(2, count) = depth2;
				trackPoint3fTemp.at<float>(3, count) = 1;*/

				count++;
			}
		}
		//cout << "count" << count << endl;

		initPoint3fDepth = cv::Mat(4, count, CV_32F);
		trackPoint3fDepth = cv::Mat(4, count, CV_32F);

		for (int j = 0; j < count; j++){
			initPoint3fDepth.at<float>(0, j) = initPoint3fTempData[0 * trackSuccessCount[0] + j];
			initPoint3fDepth.at<float>(1, j) = initPoint3fTempData[1 * trackSuccessCount[0] + j];
			initPoint3fDepth.at<float>(2, j) = initPoint3fTempData[2 * trackSuccessCount[0] + j];
			initPoint3fDepth.at<float>(3, j) = initPoint3fTempData[3 * trackSuccessCount[0] + j];

			trackPoint3fDepth.at<float>(0, j) = trackPoint3fTempData[0 * trackSuccessCount[0] + j];
			trackPoint3fDepth.at<float>(1, j) = trackPoint3fTempData[1 * trackSuccessCount[0] + j];
			trackPoint3fDepth.at<float>(2, j) = trackPoint3fTempData[2 * trackSuccessCount[0] + j];
			trackPoint3fDepth.at<float>(3, j) = trackPoint3fTempData[3 * trackSuccessCount[0] + j];
		}
		//cout << "initPoint3f" << initPoint3fDepth << endl;

		cv::Mat worldPoint3f = inverseCameraMat * initPoint3fDepth;
		cv::Mat addPoint3f = inverseCameraMat * trackPoint3fDepth;
		cv::Mat inverseAddPoint3f;

		for (int k = 0; k < 30; k++){
			calculateCoarseCamPose(worldPoint3f, addPoint3f);
		}
	}
	//cout << "result count : " << resultRansacCount << endl;
	//cout << "result Cam Pose : " << resultCamPose << endl;
	//cout << "result Error : " << resultRansacError << endl;
}
void Map::clearSuccessFeature(){
	for (int i = 0; i < 4; i++){
		trackSuccessFeature[0][i].clear();
		trackSuccessFeature[1][i].clear();
	}
}
int Map::getMatchNum(){
	return matchNum;
}
vector<cv::DMatch> Map::getMatches(int i){
	return matches[i];
}
vector<cv::Point2f> Map::getSuccessFeature(int i, int j){
	return trackSuccessFeature[i][j];
}
cv::Mat Map::getResult(){
	return result;
}
cv::Mat Map::getInitPoint3fDepth(){
	return initPoint3fDepth;
}
cv::Mat Map::getTrackPoint3fDepth(){
	return trackPoint3fDepth;
}
bool Map::getTrack(){
	return track;
}
bool Map::getInitFrame(){
	return initFrame;
}



//private
cv::Point3f Map::calUnitVector(cv::Point3f input){
	cv::Point3f unitResult;
	float length = 0.0f;
	length = sqrtf(input.x * input.x + input.y * input.y + input.z * input.z);
	unitResult.x = input.x / length;
	unitResult.y = input.y / length;
	unitResult.z = input.z / length;

	return unitResult;
}
cv::Point3f Map::calNormal3Point(cv::Point3f inputCenter, cv::Point3f input2, cv::Point3f input3){
	cv::Point3f cross1(input2 - inputCenter);
	cv::Point3f cross2(input3 - inputCenter);
	cv::Point3f crossResult = cross1.cross(cross2);
	cout << "crossResult" << calUnitVector(crossResult) << endl;
	return calUnitVector(crossResult);
}
vector<int> Map::random6PointSelect(int cols){
	int max = cols;
	vector<int> select;
	int ran = 0;
	for (int i = 0; i < 6; i++){
		ran = rand() % max - 1;
		if (ran < 0)
			ran = 0;
		select.push_back(ran);
	}
	return select;
}
cv::Mat Map::random6PointSelectMat(vector<int> select, cv::Mat inputInit){
	cv::Mat random6PointResult = (cv::Mat_<float>(3, 6) <<
		inputInit.at<float>(0, select[0]), inputInit.at<float>(0, select[1]), inputInit.at<float>(0, select[2]), inputInit.at<float>(0, select[3]), inputInit.at<float>(0, select[4]), inputInit.at<float>(0, select[5]),
		inputInit.at<float>(1, select[0]), inputInit.at<float>(1, select[1]), inputInit.at<float>(1, select[2]), inputInit.at<float>(1, select[3]), inputInit.at<float>(1, select[4]), inputInit.at<float>(1, select[5]),
		inputInit.at<float>(2, select[0]), inputInit.at<float>(2, select[1]), inputInit.at<float>(2, select[2]), inputInit.at<float>(2, select[3]), inputInit.at<float>(2, select[4]), inputInit.at<float>(2, select[5]));
	return random6PointResult;
}
void Map::calculateCoarseCamPose(cv::Mat inputInit, cv::Mat inputLater){
	vector<int> select = random6PointSelect(inputInit.cols);
	cv::Mat init = random6PointSelectMat(select, inputInit);
	cv::Mat later = random6PointSelectMat(select, inputLater);
	/*cout << "init" << endl;
	cout << init << endl;
	cout << "later" << endl;
	cout << later << endl;*/
	cv::Mat A = (cv::Mat_<float>(18, 6) <<
		0, init.at<float>(2, 0), -init.at<float>(1, 0), 1, 0, 0,
		-init.at<float>(2, 0), 0, init.at<float>(0, 0), 0, 1, 0,
		init.at<float>(1, 0), -init.at<float>(0, 0), 0, 0, 0, 1,
		0, init.at<float>(2, 1), -init.at<float>(1, 1), 1, 0, 0,
		-init.at<float>(2, 1), 0, init.at<float>(0, 1), 0, 1, 0,
		init.at<float>(1, 1), -init.at<float>(0, 1), 0, 0, 0, 1,
		0, init.at<float>(2, 2), -init.at<float>(1, 2), 1, 0, 0,
		-init.at<float>(2, 2), 0, init.at<float>(0, 2), 0, 1, 0,
		init.at<float>(1, 2), -init.at<float>(0, 2), 0, 0, 0, 1,
		0, init.at<float>(2, 3), -init.at<float>(1, 3), 1, 0, 0,
		-init.at<float>(2, 3), 0, init.at<float>(0, 3), 0, 1, 0,
		init.at<float>(1, 3), -init.at<float>(0, 3), 0, 0, 0, 1,
		0, init.at<float>(2, 4), -init.at<float>(1, 4), 1, 0, 0,
		-init.at<float>(2, 4), 0, init.at<float>(0, 4), 0, 1, 0,
		init.at<float>(1, 4), -init.at<float>(0, 4), 0, 0, 0, 1,
		0, init.at<float>(2, 5), -init.at<float>(1, 5), 1, 0, 0,
		-init.at<float>(2, 5), 0, init.at<float>(0, 5), 0, 1, 0,
		init.at<float>(1, 5), -init.at<float>(0, 5), 0, 0, 0, 1);
	cv::Mat B = (cv::Mat_<float>(18, 1) <<
		later.at<float>(0, 0) - init.at<float>(0, 0),
		later.at<float>(1, 0) - init.at<float>(1, 0),
		later.at<float>(2, 0) - init.at<float>(2, 0),
		later.at<float>(0, 1) - init.at<float>(0, 1),
		later.at<float>(1, 1) - init.at<float>(1, 1),
		later.at<float>(2, 1) - init.at<float>(2, 1),
		later.at<float>(0, 2) - init.at<float>(0, 2),
		later.at<float>(1, 2) - init.at<float>(1, 2),
		later.at<float>(2, 2) - init.at<float>(2, 2),
		later.at<float>(0, 3) - init.at<float>(0, 3),
		later.at<float>(1, 3) - init.at<float>(1, 3),
		later.at<float>(2, 3) - init.at<float>(2, 3),
		later.at<float>(0, 4) - init.at<float>(0, 4),
		later.at<float>(1, 4) - init.at<float>(1, 4),
		later.at<float>(2, 4) - init.at<float>(2, 4),
		later.at<float>(0, 5) - init.at<float>(0, 5),
		later.at<float>(1, 5) - init.at<float>(1, 5),
		later.at<float>(2, 5) - init.at<float>(2, 5));

	cv::Mat inverseA;
	cv::invert(A, inverseA, cv::DECOMP_SVD);
	cv::Mat camPoseResult;
	camPoseResult = inverseA*B;
	//cout << "result" << endl;
	//cout << camPoseResult << endl;

	cv::Mat error;
	cv::Mat RT = (cv::Mat_<float>(4, 4) <<
		1, -camPoseResult.at<float>(2, 0), camPoseResult.at<float>(1, 0), camPoseResult.at<float>(3, 0),
		camPoseResult.at<float>(2, 0), 1, -camPoseResult.at<float>(0, 0), camPoseResult.at<float>(4, 0),
		-camPoseResult.at<float>(1, 0), camPoseResult.at<float>(0, 0), 1, camPoseResult.at<float>(5, 0),
		0, 0, 0, 1);
	error = RT*inputInit - inputLater;
	float sumError = 0.0f;
	for (int k = 0; k < error.cols; k++){
		sumError += fabsf(error.at<float>(0, k));
		sumError += fabsf(error.at<float>(1, k));
		sumError += fabsf(error.at<float>(2, k));
	}
	//cout << "error : \n" << error << endl;
	//cout << "Sum Error : " << sumError << endl;

	int ransacCount = 0;
	float threshold = 40.0f;
	for (int j = 0; j < error.cols; j++){
		if (fabsf(error.at<float>(0, j)) < threshold && fabsf(error.at<float>(1, j)) < threshold && fabsf(error.at<float>(2, j)) < threshold){
			ransacCount++;
		}
	}
	//cout << "ransac count : " << ransacCount << endl;

	if (ransacCount > resultRansacCount){
		resultRansacCount = ransacCount;
		resultCamPose = camPoseResult;
		resultRansacError = sumError;
	}
	else if (ransacCount == resultRansacCount){
		if (sumError < resultRansacError){
			resultCamPose = camPoseResult;
			resultRansacError = sumError;
		}
	}
}
//void Map::calculateCoarseCamPose(cv::Mat inputInit, cv::Mat inputLater){
//	vector<int> select = random6PointSelect(inputInit.cols);
//	cv::Mat init = random6PointSelectMat(select, inputInit);
//	cv::Mat later = random6PointSelectMat(select, inputLater);
//	/*cout << "init" << endl;
//	cout << init << endl;
//	cout << "later" << endl;
//	cout << later << endl;*/
//	cv::Mat A = (cv::Mat_<float>(18, 6) <<
//		0, init.at<float>(2, 0), -init.at<float>(1, 0), 1, 0, 0,
//		-init.at<float>(2, 0), 0, init.at<float>(0, 0), 0, 1, 0,
//		init.at<float>(1, 0), -init.at<float>(0, 0), 0, 0, 0, 1,
//		0, init.at<float>(2, 1), -init.at<float>(1, 1), 1, 0, 0,
//		-init.at<float>(2, 1), 0, init.at<float>(0, 1), 0, 1, 0,
//		init.at<float>(1, 1), -init.at<float>(0, 1), 0, 0, 0, 1,
//		0, init.at<float>(2, 2), -init.at<float>(1, 2), 1, 0, 0,
//		-init.at<float>(2, 2), 0, init.at<float>(0, 2), 0, 1, 0,
//		init.at<float>(1, 2), -init.at<float>(0, 2), 0, 0, 0, 1,
//		0, init.at<float>(2, 3), -init.at<float>(1, 3), 1, 0, 0,
//		-init.at<float>(2, 3), 0, init.at<float>(0, 3), 0, 1, 0,
//		init.at<float>(1, 3), -init.at<float>(0, 3), 0, 0, 0, 1,
//		0, init.at<float>(2, 4), -init.at<float>(1, 4), 1, 0, 0,
//		-init.at<float>(2, 4), 0, init.at<float>(0, 4), 0, 1, 0,
//		init.at<float>(1, 4), -init.at<float>(0, 4), 0, 0, 0, 1,
//		0, init.at<float>(2, 5), -init.at<float>(1, 5), 1, 0, 0,
//		-init.at<float>(2, 5), 0, init.at<float>(0, 5), 0, 1, 0,
//		init.at<float>(1, 5), -init.at<float>(0, 5), 0, 0, 0, 1 );
//	cv::Mat B = (cv::Mat_<float>(18, 1) <<
//		later.at<float>(0, 0) - init.at<float>(0, 0),
//		later.at<float>(1, 0) - init.at<float>(1, 0),
//		later.at<float>(2, 0) - init.at<float>(2, 0),
//		later.at<float>(0, 1) - init.at<float>(0, 1),
//		later.at<float>(1, 1) - init.at<float>(1, 1),
//		later.at<float>(2, 1) - init.at<float>(2, 1),
//		later.at<float>(0, 2) - init.at<float>(0, 2),
//		later.at<float>(1, 2) - init.at<float>(1, 2),
//		later.at<float>(2, 2) - init.at<float>(2, 2),
//		later.at<float>(0, 3) - init.at<float>(0, 3),
//		later.at<float>(1, 3) - init.at<float>(1, 3),
//		later.at<float>(2, 3) - init.at<float>(2, 3), 
//		later.at<float>(0, 4) - init.at<float>(0, 4),
//		later.at<float>(1, 4) - init.at<float>(1, 4),
//		later.at<float>(2, 4) - init.at<float>(2, 4),
//		later.at<float>(0, 5) - init.at<float>(0, 5),
//		later.at<float>(1, 5) - init.at<float>(1, 5),
//		later.at<float>(2, 5) - init.at<float>(2, 5));
//
//	cv::Mat inverseA;
//	cv::invert(A, inverseA, cv::DECOMP_SVD);
//	cv::Mat result;
//	result = inverseA*B;
//	cout << "result" << endl;
//	cout << result << endl;
//}
void Map::calculateCoarseCamPoseICP(cv::Mat init, cv::Mat later){	
	//ICP 계산			오류있는걸로 추정됨.
	vector <cv::Point3f> d, n, s;
	for (int i = 0; i < 6; i++){
		s.push_back(cv::Point3f(init.at<float>(0, i), init.at<float>(1, i), init.at<float>(2, i)));
	}
	for (int i = 1; i < 7; i++){
		
		d.push_back(cv::Point3f(later.at<float>(0, i), later.at<float>(1, i), later.at<float>(2, i)));
	}
	cv::Point3f n1 = calNormal3Point(d[0], d[1], d[2]);
	cv::Point3f n2 = n1;
	cv::Point3f n3 = n1;
	cv::Point3f n4 = calNormal3Point(d[3], d[4], d[5]);
	cv::Point3f n5 = n4;
	cv::Point3f n6 = n4;
	cout << "s"<<s << endl;
	cout << "d"<<d << endl;
	n.push_back(n1);
	n.push_back(n2);
	n.push_back(n3);
	n.push_back(n4);
	n.push_back(n5);
	n.push_back(n6);

	cv::Mat A = (cv::Mat_<float>(6, 6) <<
		n[0].z*s[0].y - n[0].y*s[0].z, n[0].x*s[0].z - n[0].z*s[0].x, n[0].y*s[0].x - n[0].x*s[0].y, n[0].x, n[0].y, n[0].z,
		n[1].z*s[1].y - n[1].y*s[1].z, n[1].x*s[1].z - n[1].z*s[1].x, n[1].y*s[1].x - n[1].x*s[1].y, n[1].x, n[1].y, n[1].z,
		n[2].z*s[2].y - n[2].y*s[2].z, n[2].x*s[2].z - n[2].z*s[2].x, n[2].y*s[2].x - n[2].x*s[2].y, n[2].x, n[2].y, n[2].z,
		n[3].z*s[3].y - n[3].y*s[3].z, n[3].x*s[3].z - n[3].z*s[3].x, n[3].y*s[3].x - n[3].x*s[3].y, n[3].x, n[3].y, n[3].z,
		n[4].z*s[4].y - n[4].y*s[4].z, n[4].x*s[4].z - n[4].z*s[4].x, n[4].y*s[4].x - n[4].x*s[4].y, n[4].x, n[4].y, n[4].z,
		n[5].z*s[5].y - n[5].y*s[5].z, n[5].x*s[5].z - n[5].z*s[5].x, n[5].y*s[5].x - n[5].x*s[5].y, n[5].x, n[5].y, n[5].z);

	cv::Mat B = (cv::Mat_<float>(6, 1) << 
		n[0].x*d[0].x + n[0].y*d[0].y + n[0].z*d[0].z - n[0].x*s[0].x - n[0].y*s[0].y - n[0].z*s[0].z,
		n[1].x*d[1].x + n[1].y*d[1].y + n[1].z*d[1].z - n[1].x*s[1].x - n[1].y*s[1].y - n[1].z*s[1].z,
		n[2].x*d[2].x + n[2].y*d[2].y + n[2].z*d[2].z - n[2].x*s[2].x - n[2].y*s[2].y - n[2].z*s[2].z,
		n[3].x*d[3].x + n[3].y*d[3].y + n[3].z*d[3].z - n[3].x*s[3].x - n[3].y*s[3].y - n[3].z*s[3].z,
		n[4].x*d[4].x + n[4].y*d[4].y + n[4].z*d[4].z - n[4].x*s[4].x - n[4].y*s[4].y - n[4].z*s[4].z,
		n[5].x*d[5].x + n[5].y*d[5].y + n[5].z*d[5].z - n[5].x*s[5].x - n[5].y*s[5].y - n[5].z*s[5].z);
	cv::Mat inverseA;
	cv::invert(A, inverseA, cv::DECOMP_SVD);

	cv::Mat ICPresult = inverseA*B;

	cout << "result" << endl;
	for (int i = 0; i < 6; i++){
		for (int j = 0; j < 1; j++){
			cout << ICPresult.at<float>(i, j) << "  ";
		}
		cout << endl;
	}
}
float Map::getDepth(cv::Mat depth, float row, float col){
	float temp[5] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
	float* depthData = (float*)depth.data;
	int depthPosition[5];
	depthPosition[0] = depth.rows*row + col;
	temp[0] = 3000.0f * (1.0f - depthData[depthPosition[0]]);
	//	1
	//2 0 3
	//  4

	int rad = 1;

	if (temp[0] == 3000.0f){
		temp[0] = 32000.0f;
		float row1 = row - rad;
		float row2 = row;
		float row3 = row;
		float row4 = row + rad;
		if (row1 < 0){
			row1 = row;
		}
		if (row4 > 479){
			row4 = row;
		}
		float col1 = col;
		float col2 = col - rad;
		float col3 = col + rad;
		float col4 = col;
		if (col2 < 0){
			col2 = col;
		}
		if (col4 > 639){
			col4 = col;
		}
		depthPosition[1] = depth.rows*row1 + col1;
		temp[1] = 3000.0f * (1.0f - depthData[depthPosition[1]]);
		depthPosition[2] = depth.rows*row2 + col2;
		temp[2] = 3000.0f * (1.0f - depthData[depthPosition[2]]);
		depthPosition[3] = depth.rows*row3 + col3;
		temp[3] = 3000.0f * (1.0f - depthData[depthPosition[3]]);
		depthPosition[4] = depth.rows*row4 + col4;
		temp[4] = 3000.0f * (1.0f - depthData[depthPosition[4]]);

		for (int i = 0; i < 5; i++){
			if (temp[i] == 3000.0f){
				temp[i] = 32000.0f;
			}
		}
		
		float min = 0.0f;
		min = (temp[1] < temp[2]) ? temp[1] : temp[2];
		min = (min < temp[3]) ? min : temp[3];
		min = (min < temp[4]) ? min : temp[4];
		temp[0] = (temp[0] < min) ? temp[0] : min;
		temp[0] = min;
		if (temp[0] == 32000.0f){
			rad = 2;
			float row1 = row - rad;
			float row2 = row;
			float row3 = row;
			float row4 = row + rad;
			if (row1 < 0){
				row1 = row;
			}
			if (row4 > 479){
				row4 = row;
			}
			float col1 = col;
			float col2 = col - rad;
			float col3 = col + rad;
			float col4 = col;
			if (col2 < 0){
				col2 = col;
			}
			if (col4 > 639){
				col4 = col;
			}
			depthPosition[1] = depth.rows*row1 + col1;
			temp[1] = 3000.0f * (1.0f - depthData[depthPosition[1]]);
			depthPosition[2] = depth.rows*row2 + col2;
			temp[2] = 3000.0f * (1.0f - depthData[depthPosition[2]]);
			depthPosition[3] = depth.rows*row3 + col3;
			temp[3] = 3000.0f * (1.0f - depthData[depthPosition[3]]);
			depthPosition[4] = depth.rows*row4 + col4;
			temp[4] = 3000.0f * (1.0f - depthData[depthPosition[4]]);

			for (int i = 0; i < 5; i++){
				if (temp[i] == 3000.0f){
					temp[i] = 32000.0f;
				}
			}

			float min = 0.0f;
			min = (temp[1] < temp[2]) ? temp[1] : temp[2];
			min = (min < temp[3]) ? min : temp[3];
			min = (min < temp[4]) ? min : temp[4];
			temp[0] = (temp[0] < min) ? temp[0] : min;
		}
	}
	return temp[0];
}