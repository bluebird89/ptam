#include "Map.h"

Map::Map(){
	tracking3Frame = false;
	initFrame = false;
	track = false;
	matchNum = 0;
	resultRansacCount = 0;
	resultRansacError = 1000000;
	resultCamPose = cv::Mat::zeros(6, 1, CV_32F);
#if BRISK_SIFT == 1
	matcher = new cv::BFMatcher(cv::NORM_HAMMING, true);
#endif
#if BRISK_SIFT == 0
	matcher = new cv::BFMatcher(cv::NORM_L2, true);
#endif
	prevR = (cv::Mat_<float>(3, 1) << 0.0f, 0.0f, 0.0f);
	prevT = (cv::Mat_<float>(3, 1) << 0.0f, 0.0f, 0.0f);
}
Map::~Map(){
	initImage.~ImageData();
	trackImage.~ImageData();
	matcher->~BFMatcher();
}
void Map::setProperty(cv::Mat calibMat, cv::Mat coff){
	cameraMat = cv::Mat(3, 3, CV_32F);
	cameraCoff = cv::Mat(1, 4, CV_32F);
	for (int i = 0; i < 3; i++){
		cameraMat.at<float>(i, 0) = calibMat.at<float>(i, 0);
		cameraMat.at<float>(i, 1) = calibMat.at<float>(i, 1);
		cameraMat.at<float>(i, 2) = calibMat.at<float>(i, 2);
	}
	cameraCoff = coff;
	cv::invert(cameraMat, inverseCameraMat);
	resultMap.setProperty(cameraMat, inverseCameraMat);
	keyFrame.setCameraMat(cameraMat); 
}
void Map::initMap(cv::Mat colorImage, cv::Mat grayImage, cv::Mat depthImage){
	cornerColor.clear();
	resultMap.clear();
	if (initFrame == false){
		matchNum = 0;//초기화
		matches.clear();
		successCalculatePointColor.clear();

		initImage.run(colorImage, grayImage, depthImage, false);

		initFrame = true;
		track = true;
	}
	else{
		trackImage.run(colorImage, grayImage, depthImage, true);
		if (initImage.getDescriptor().size > 0 && trackImage.getDescriptor().size > 0
			&& initImage.getfastCorner().size() > 0 && trackImage.getfastCorner().size() > 0){
			
			matcher->match(initImage.getDescriptor(), trackImage.getDescriptor(), matches);
			vector<int> usingMatch = ransacMatchPoint(initImage.getfastCorner(), trackImage.getfastCorner(), matches);
			
			if (usingMatch.size() > 6){
				initImage.setUsingPoint(usingMatch, matches, false);
				trackImage.setUsingPoint(usingMatch, matches, true);
				trackImage.setUsingPoint(initImage.getDescriptorIdx(), true);

#if PYR == 1
				if (initImage.getPyrDescriptor().size > 0 && trackImage.getPyrDescriptor().size > 0
					&& initImage.getPyrfastCorner().size() > 0 && trackImage.getPyrfastCorner().size() > 0){
					matcher->match(initImage.getPyrDescriptor(), trackImage.getPyrDescriptor(), matches);
					usingMatch = ransacMatchPoint(initImage.getPyrfastCorner(), trackImage.getPyrfastCorner(), matches);

					if (usingMatch.size() > 4){
						initImage.setPyrUsingPoint(usingMatch, matches, false);
						trackImage.setPyrUsingPoint(usingMatch, matches, true);
						trackImage.setPyrUsingPoint(initImage.getPyrDescriptorIdx(), true);
					}
					else{
						cout << "tracking 실패 222222" << endl;
						initFrame = false;
						track = false;
					}
				}
#endif
			}
			else{
				cout << "tracking 실패" << endl;
				initFrame = false;
				track = false;
			}
		}
	}
}
void Map::genMap(){
	vector<cv::Point3f> initTrackCorner = initImage.getTrackFastCornerDepth();
	if (initTrackCorner.size() > 6){
		cv::Mat initCorner(3, initTrackCorner.size(), CV_32F);
		float* initCornerPointer = (float*)initCorner.data;
		for (int i = 0; i < initTrackCorner.size(); i++){
			initCornerPointer[initTrackCorner.size() * 0 + i] = initTrackCorner[i].x;
			initCornerPointer[initTrackCorner.size() * 1 + i] = initTrackCorner[i].y;
			initCornerPointer[initTrackCorner.size() * 2 + i] = initTrackCorner[i].z;
		}
		
		cv::Mat worldPoint3f = inverseCameraMat * initCorner;
		float* worldPoint3fPointer = (float*)worldPoint3f.data;
		
		vector<cv::Point3f> worldPoint3fVector;
		for (int i = 0; i < worldPoint3f.cols; i++){
			worldPoint3fVector.push_back(cv::Point3f(
				worldPoint3fPointer[worldPoint3f.cols * 0 + i],
				worldPoint3fPointer[worldPoint3f.cols * 1 + i],
				worldPoint3fPointer[worldPoint3f.cols * 2 + i]));
		}
		calculateCamPoseSolvePNP(worldPoint3fVector, trackImage.getTrackFastCorner());
		resultMap.setRT(resultR, resultT);
		resultMap.setPoint(worldPoint3f, initImage.getTrackDescriptor(), initImage.getTrackRGB());
		vector<int> temp;
		vector<cv::Point2f> temp2 = trackImage.getTrackFastCorner();
		cv::Mat trackImageMat = cv::Mat(2, worldPoint3f.cols, CV_32F);
		float* trackImageMatPointer = (float*)trackImageMat.data;

		for (int i = 0; i < worldPoint3f.cols; i++){
			temp.push_back(i);
			trackImageMatPointer[0 * worldPoint3f.cols + i] = temp2[i].x;
			trackImageMatPointer[1 * worldPoint3f.cols + i] = temp2[i].y;
		}
		keyFrame.setImageData(resultR, resultT, resultMap.getResultPoint(), trackImageMat, temp);
	}
	else{
		cout << "유효 depth를 가진 point 수가 부족합니다." << endl;
		initFrame = false;
		track = false;
	}
}
void Map::updateMap(cv::Mat colorImage, cv::Mat grayImage, cv::Mat depthImage){
	trackImage.run(colorImage, grayImage, depthImage, true);
	if (resultMap.setPredictPoint()){
		int numOfAddPoint = predictPoint();
		cout << "updateMap numOfAddPoint : " << numOfAddPoint << endl;
		if (numOfAddPoint > 0){
			//cv::Mat addPointMat = trackImage.getAddPointDepthMat();
			//addPointMat = inverseCameraMat * addPointMat;
			resultMap.setAddPoint(trackImage.getAddPointDepthMat(), trackImage.getAddPoint(), trackImage.getAddRGB(), trackImage.getAddDescriptor(), tracking3Frame);
			tracking3Frame = true;
			if (resultMap.getAddPointIdx().size() > 0){
				trackImage.setResultAddPoint(resultMap.getPointIdx());
				//ImageData addFrame = trackImage;
				keyFrame.setImageData(resultR, resultT, resultMap.getResultPoint(), trackImage.getKeyPoint(), trackImage.getKeyPointIdx());
				resultMap.refineMap(trackImage.getKeyPointDepth(), trackImage.getKeyPointIdx());
				//	keyFrame.push_back(trackImage);
			}
		}
		else{
			tracking3Frame = false;
		}
	}
	else{
		tracking3Frame = false;
		cout << "tracking이 이상합니다 descriptor로 카메라 포즈를 예측합니다 updateMap" << endl;
		loseCamPose();
	}
	
	cv::Mat checkProjectData = colorImage.clone();
	cv::Mat resultPointMat = resultMap.getResultPoint().clone();
	cv::Mat resultRMat;
	cv::Rodrigues(resultR, resultRMat);
	resultPointMat = resultRMat * resultPointMat;
	resultPointMat.row(0) = resultPointMat.row(0) + resultT.at<float>(0, 0);
	resultPointMat.row(1) = resultPointMat.row(1) + resultT.at<float>(1, 0);
	resultPointMat.row(2) = resultPointMat.row(2) + resultT.at<float>(2, 0);

	resultPointMat = cameraMat * resultPointMat;

	resultPointMat.row(0) = resultPointMat.row(0) / resultPointMat.row(2);
	resultPointMat.row(1) = resultPointMat.row(1) / resultPointMat.row(2);
	resultPointMat.row(2) = resultPointMat.row(2) / resultPointMat.row(2);

	float* resultPointMatPointer = (float*)resultPointMat.data;


	for (int i = 0; i < resultPointMat.cols; i++){
		if (resultPointMatPointer[resultPointMat.cols * 0 + i] < (640)	//가로세로 50pixel만큼 더 체크하기로.
			&& resultPointMatPointer[resultPointMat.cols * 0 + i] > (0)
			&& resultPointMatPointer[resultPointMat.cols * 1 + i] < (480)
			&& resultPointMatPointer[resultPointMat.cols * 1 + i] > (0)){
			cv::circle(checkProjectData, cv::Point2f(resultPointMatPointer[resultPointMat.cols * 0 + i],
				resultPointMatPointer[resultPointMat.cols * 1 + i]), 1.0, cv::Scalar(0, 255, 0));
		}
	}
	cv::imshow("checkProjectData", checkProjectData);

}
void Map::runSBA(){
	resultMap.printMap(true);
	if (keyFrame.runSBA() != -1){
		resultR.at<float>(0, 0) = keyFrame.getSbaR(keyFrame.sbaR.cols-1).at<float>(0, 0);
		resultR.at<float>(1, 0) = keyFrame.getSbaR(keyFrame.sbaR.cols-1).at<float>(1, 0);
		resultR.at<float>(2, 0) = keyFrame.getSbaR(keyFrame.sbaR.cols-1).at<float>(2, 0);
		resultT.at<float>(0, 0) = keyFrame.sbaT.at<float>(0, keyFrame.sbaT.cols-1);
		resultT.at<float>(1, 0) = keyFrame.sbaT.at<float>(1, keyFrame.sbaT.cols-1);
		resultT.at<float>(2, 0) = keyFrame.sbaT.at<float>(2, keyFrame.sbaT.cols-1);
		resultMap.setSBAData(keyFrame.sbaCorner3D);
		resultMap.setRT(resultR, resultT);
	}
}
void Map::clearSuccessFeature(){
	trackSuccessFeature[0].clear();
	trackSuccessFeature[1].clear();
}
int Map::getMatchNum(){
	return matchNum;
}
vector<cv::DMatch> Map::getMatches(){
	return matches;
}
vector<cv::Point2f> Map::getSuccessFeature(int i){
	return trackSuccessFeature[i];
}
cv::Mat Map::getResult(){
	return resultCamPose;
}
cv::Mat Map::getResultR(){
	return resultR;
}
cv::Mat Map::getResultT(){
	return resultT;
}
cv::Mat Map::getResultRGB(){
	return resultMap.getResultRGB();
}
cv::Mat Map::getInitPoint3fDepth(){
	return initPoint3fDepth;
}
vector<cv::Point2f> Map::getTrackPoint3f(){
	return trackImage.getTrackFastCorner();
}
cv::Mat Map::getSuccessCalculateMapPoint(){
	return successCalculateMatPoint;
}
cv::Mat Map::getCorner3D(){
	return keyFrame.corner3D;
}
cv::Mat Map::getSba3D(){
	return keyFrame.sbaCorner3D;
}
cv::Mat Map::getSbaR(int colum){
	return keyFrame.getSbaR(colum);
}
cv::Mat Map::getSbaT(){
	return keyFrame.sbaT;
}
bool Map::getTrack(){
	return track;
}
bool Map::getInitFrame(){
	return initFrame;
}
//private
cv::Point3f Map::calUnitVector(cv::Point3f input){
	cv::Point3f unitResult;
	float length = 0.0f;
	length = sqrtf(input.x * input.x + input.y * input.y + input.z * input.z);
	unitResult.x = input.x / length;
	unitResult.y = input.y / length;
	unitResult.z = input.z / length;

	return unitResult;
}
cv::Point3f Map::calNormal3Point(cv::Point3f inputCenter, cv::Point3f input2, cv::Point3f input3){
	cv::Point3f cross1(input2 - inputCenter);
	cv::Point3f cross2(input3 - inputCenter);
	cv::Point3f crossResult = cross1.cross(cross2);
	std::cout << "crossResult" << calUnitVector(crossResult) << endl;
	return calUnitVector(crossResult);
}
vector<int> Map::random6PointSelect(int cols){
	int max = cols;
	vector<int> select;
	if (cols >= 6){
		int ran = 0;
		for (int i = 0; i < 6; i++){
			ran = rand() % max - 1;
			if (ran < 0)
				ran = 0;
			select.push_back(ran);
		}
	}
	else{
		select.push_back(0);
		std::cout << "인자가 6보다 작습니다. 오류(random6PointSelect)" << endl;
	}
	return select;
}
cv::Mat Map::random6PointSelectMat(vector<int> select, cv::Mat input){
	float* inputInit = (float*)input.data;
	int cols = input.cols;
	cv::Mat random6PointResult = (cv::Mat_<float>(3, 6) <<
		inputInit[0 * cols + select[0]], inputInit[0 * cols + select[1]], inputInit[0 * cols + select[2]], inputInit[0 * cols + select[3]], inputInit[0 * cols + select[4]], inputInit[0 * cols + select[5]],
		inputInit[1 * cols + select[0]], inputInit[1 * cols + select[1]], inputInit[1 * cols + select[2]], inputInit[1 * cols + select[3]], inputInit[1 * cols + select[4]], inputInit[1 * cols + select[5]],
		inputInit[2 * cols + select[0]], inputInit[2 * cols + select[1]], inputInit[2 * cols + select[2]], inputInit[2 * cols + select[3]], inputInit[2 * cols + select[4]], inputInit[2 * cols + select[5]]);
	return random6PointResult;
}
vector<int> Map::ransacMatchPoint(vector<cv::KeyPoint> queryPoint, vector<cv::KeyPoint> trainPoint, vector<cv::DMatch> match, int num_of_point){
	vector <int> usingMatch1;
	vector <int> ran6Point;
	vector <float> distance;
	vector <int> usingMatch2;

	float meanRansac = 0.0f;
	float tempMean = 0.0f;
	float threshold2 = 0.0f;
	int count = 0;
	int tempCount = 0;
	float tempX, tempY = 0.0f;
	vector <float> distance2;
	for (int k = 0; k < match.size(); k++){
		tempX = queryPoint[match[k].queryIdx].pt.x - trainPoint[match[k].trainIdx].pt.x;
		tempY = queryPoint[match[k].queryIdx].pt.y - trainPoint[match[k].trainIdx].pt.y;
		tempMean = sqrtf(tempX*tempX + tempY*tempY);
		//cout << "tempMean : " << tempMean << endl;
		//cout << "distance : " << match[k].distance << endl;
		tempMean += match[k].distance;
		distance.push_back(tempMean);
		distance2.push_back(tempMean);
	}
	cout << "distance size : " << distance2.size() << endl;
	
	sort(distance.begin(), distance.end());
	if (distance2.size() > num_of_point){
		if (num_of_point < 50){
			tempMean = distance[distance.size()*2 / 5];
		}	
		else{
			tempMean = distance[num_of_point];
		}
		//cout << distance[distance.size()-1] <<endl;
#if BRISK_SIFT == 0
		if (tempMean > 200.0f){
			tempMean = 130.0f;
		}
#endif 
#if BRISK_SIFT == 1
		if (tempMean > 70.f){
			tempMean = 70.0f;
		}
#endif
		tempMean = 90.0f;
		cout << "threshold : " << tempMean << endl;
		for (int k = 0; k < distance2.size(); k++){
			if (distance2[k] <= tempMean){
				usingMatch1.push_back(k);
			}
		}
	}
	cout << "match size : " << usingMatch1.size() << endl;
	return usingMatch1;
}
bool Map::checkPoint(cv::Mat pointArray, cv::Mat checkPoint, int checkPointNum, float threshold){
	float addPointX = 0.0f, addPointY = 0.0f, addPointZ = 0.0f;
	float* pointArrayPointer = (float*)pointArray.data;
	addPointX = checkPoint.at<float>(0, checkPointNum);
	addPointY = checkPoint.at<float>(1, checkPointNum);
	addPointZ = checkPoint.at<float>(2, checkPointNum);
	for (int j = 0; j < pointArray.cols; j++){
		if (fabsf(addPointX - pointArrayPointer[0 * pointArray.cols + j]) < threshold
			&& fabsf(addPointY - pointArrayPointer[1 * pointArray.cols + j]) < threshold
			&& fabsf(addPointZ - pointArrayPointer[2 * pointArray.cols + j]) < threshold){	//xyz 포인트가 존재하는 포인트의 200mm안으로 들어오면 추가안함.
			return false;
		}
	}
	return true;
}
void Map::calculateCamPoseSolvePNP(vector<cv::Point3f> inputInit, vector<cv::Point2f> inputLater){
	cv::Mat vecR, vecT;
	prevR = resultR.clone();
	prevT = resultT.clone();
	resultR = cv::Mat(3, 1, CV_32F);
	resultT = cv::Mat(3, 1, CV_32F);
	/*cout << inputInit << endl;
	cout << inputLater << endl;*/
	int inlier = 4 * inputInit.size() / 5;
	vector<int> inlierArray;
	cv::Mat distCoff = (cv::Mat_<float>(1, 4) << 0.0, 0.0, 0.0, 0.0);
	if (inputInit.size() != inputLater.size()){
		cout << "size오류" << endl;
	}
	else{
		//cv::solvePnPRansac(inputInit, inputLater, cameraMat, distCoff, vecR, vecT, false, 100, 50.0f, inlier, inlierArray);
		cv::solvePnP(inputInit, inputLater, cameraMat, distCoff, vecR, vecT);
		//cout << "inlier 크기 : " << inlierArray.size() << " / " << inputLater.size() << endl;
		resultR.at<float>(0, 0) = (float)vecR.at<double>(0, 0);
		resultR.at<float>(1, 0) = (float)vecR.at<double>(1, 0);
		resultR.at<float>(2, 0) = (float)vecR.at<double>(2, 0);
		resultT.at<float>(0, 0) = (float)vecT.at<double>(0, 0);
		resultT.at<float>(1, 0) = (float)vecT.at<double>(1, 0);
		resultT.at<float>(2, 0) = (float)vecT.at<double>(2, 0);
	}
	//cout << "R\n" << resultR << endl;
	//cout << "T\n" << resultT << endl;
	/*cv::Rodrigues(rvec, resultR);
	cout << R << endl;
	cv::Mat RInv = R.inv();
	cv::Mat P = -RInv * tvec;
	double* p = (double*)P.data;
	printf("x=%lf, y=%lf, z=%lf\n", p[0], p[1], p[2]);*/
}
void Map::calculateCoarseCamPose(cv::Mat inputInit, cv::Mat inputLater){
	vector<int> select = random6PointSelect(inputInit.cols);
	cv::Mat initMat = random6PointSelectMat(select, inputInit);
	cv::Mat laterMat = random6PointSelectMat(select, inputLater);
	float* init = (float*)initMat.data;
	float* later = (float*)laterMat.data;
	/*cout << "init" << endl;
	cout << init << endl;
	cout << "later" << endl;
	cout << later << endl;*/
	cv::Mat A = (cv::Mat_<float>(18, 6) <<
		0, init[2 * 6 + 0], -init[1 * 6 + 0], 1, 0, 0,
		-init[2 * 6 + 0], 0, init[0 * 6 + 0], 0, 1, 0,
		init[1 * 6 + 0], -init[0 * 6 + 0], 0, 0, 0, 1,
		0, init[2 * 6 + 1], -init[1 * 6 + 1], 1, 0, 0,
		-init[2 * 6 + 1], 0, init[0 * 6 + 1], 0, 1, 0,
		init[1 * 6 + 1], -init[0 * 6 + 1], 0, 0, 0, 1,
		0, init[2 * 6 + 2], -init[1 * 6 + 2], 1, 0, 0,
		-init[2 * 6 + 2], 0, init[0 * 6 + 2], 0, 1, 0,
		init[1 * 6 + 2], -init[0 * 6 + 2], 0, 0, 0, 1,
		0, init[2 * 6 + 3], -init[1 * 6 + 3], 1, 0, 0,
		-init[2 * 6 + 3], 0, init[0 * 6 + 3], 0, 1, 0,
		init[1 * 6 + 3], -init[0 * 6 + 3], 0, 0, 0, 1,
		0, init[2 * 6 + 4], -init[1 * 6 + 4], 1, 0, 0,
		-init[2 * 6 + 4], 0, init[0 * 6 + 4], 0, 1, 0,
		init[1 * 6 + 4], -init[0 * 6 + 4], 0, 0, 0, 1,
		0, init[2 * 6 + 5], -init[1 * 6 + 5], 1, 0, 0,
		-init[2 * 6 + 5], 0, init[0 * 6 + 5], 0, 1, 0,
		init[1 * 6 + 5], -init[0 * 6 + 5], 0, 0, 0, 1);
	cv::Mat B = (cv::Mat_<float>(18, 1) <<
		later[0 * 6 + 0] - init[0 * 6 + 0],
		later[1 * 6 + 0] - init[1 * 6 + 0],
		later[2 * 6 + 0] - init[2 * 6 + 0],
		later[0 * 6 + 1] - init[0 * 6 + 1],
		later[1 * 6 + 1] - init[1 * 6 + 1],
		later[2 * 6 + 1] - init[2 * 6 + 1],
		later[0 * 6 + 2] - init[0 * 6 + 2],
		later[1 * 6 + 2] - init[1 * 6 + 2],
		later[2 * 6 + 2] - init[2 * 6 + 2],
		later[0 * 6 + 3] - init[0 * 6 + 3],
		later[1 * 6 + 3] - init[1 * 6 + 3],
		later[2 * 6 + 3] - init[2 * 6 + 3],
		later[0 * 6 + 4] - init[0 * 6 + 4],
		later[1 * 6 + 4] - init[1 * 6 + 4],
		later[2 * 6 + 4] - init[2 * 6 + 4],
		later[0 * 6 + 5] - init[0 * 6 + 5],
		later[1 * 6 + 5] - init[1 * 6 + 5],
		later[2 * 6 + 5] - init[2 * 6 + 5]);

	cv::Mat inverseA;
	cv::invert(A, inverseA, cv::DECOMP_SVD);
	cv::Mat camPoseResult;
	camPoseResult = inverseA*B;
	//cout << "result" << endl;
	//cout << camPoseResult << endl;

	cv::Mat error;
	cv::Mat RT = (cv::Mat_<float>(4, 4) <<
		1, -camPoseResult.at<float>(2, 0), camPoseResult.at<float>(1, 0), camPoseResult.at<float>(3, 0),
		camPoseResult.at<float>(2, 0), 1, -camPoseResult.at<float>(0, 0), camPoseResult.at<float>(4, 0),
		-camPoseResult.at<float>(1, 0), camPoseResult.at<float>(0, 0), 1, camPoseResult.at<float>(5, 0),
		0, 0, 0, 1);
	error = RT*inputInit - inputLater;
	float sumError = 0.0f;
	for (int k = 0; k < error.cols; k++){
		sumError += fabsf(error.at<float>(0, k));
		sumError += fabsf(error.at<float>(1, k));
		sumError += fabsf(error.at<float>(2, k));
	}
	//cout << "error : \n" << error << endl;
	//cout << "Sum Error : " << sumError << endl;

	int ransacCount = 0;
	float threshold = 40.0f;
	vector<int> successPoint;
	for (int j = 0; j < error.cols; j++){
		if (fabsf(error.at<float>(0, j)) < threshold && fabsf(error.at<float>(1, j)) < threshold && fabsf(error.at<float>(2, j)) < threshold){
			successPoint.push_back(j);
			ransacCount++;
		}
	}
	//cout << "ransac count : " << ransacCount << endl;

	if (ransacCount > resultRansacCount){
		resultRansacCount = ransacCount;
		resultCamPose = camPoseResult;
		resultRansacError = sumError;
		successCalculateMatPointArray = successPoint;
	}
	else if (ransacCount == resultRansacCount){
		if (sumError < resultRansacError){
			resultCamPose = camPoseResult;
			resultRansacError = sumError;
			successCalculateMatPointArray = successPoint;
		}
	}
}
void Map::calculateCoarseCamPoseICP(cv::Mat init, cv::Mat later){
	//ICP 계산			오류있는걸로 추정됨.
	vector <cv::Point3f> d, n, s;
	for (int i = 0; i < 6; i++){
		s.push_back(cv::Point3f(init.at<float>(0, i), init.at<float>(1, i), init.at<float>(2, i)));
	}
	for (int i = 1; i < 7; i++){

		d.push_back(cv::Point3f(later.at<float>(0, i), later.at<float>(1, i), later.at<float>(2, i)));
	}
	cv::Point3f n1 = calNormal3Point(d[0], d[1], d[2]);
	cv::Point3f n2 = n1;
	cv::Point3f n3 = n1;
	cv::Point3f n4 = calNormal3Point(d[3], d[4], d[5]);
	cv::Point3f n5 = n4;
	cv::Point3f n6 = n4;
	std::cout << "s" << s << endl;
	std::cout << "d" << d << endl;
	n.push_back(n1);
	n.push_back(n2);
	n.push_back(n3);
	n.push_back(n4);
	n.push_back(n5);
	n.push_back(n6);

	cv::Mat A = (cv::Mat_<float>(6, 6) <<
		n[0].z*s[0].y - n[0].y*s[0].z, n[0].x*s[0].z - n[0].z*s[0].x, n[0].y*s[0].x - n[0].x*s[0].y, n[0].x, n[0].y, n[0].z,
		n[1].z*s[1].y - n[1].y*s[1].z, n[1].x*s[1].z - n[1].z*s[1].x, n[1].y*s[1].x - n[1].x*s[1].y, n[1].x, n[1].y, n[1].z,
		n[2].z*s[2].y - n[2].y*s[2].z, n[2].x*s[2].z - n[2].z*s[2].x, n[2].y*s[2].x - n[2].x*s[2].y, n[2].x, n[2].y, n[2].z,
		n[3].z*s[3].y - n[3].y*s[3].z, n[3].x*s[3].z - n[3].z*s[3].x, n[3].y*s[3].x - n[3].x*s[3].y, n[3].x, n[3].y, n[3].z,
		n[4].z*s[4].y - n[4].y*s[4].z, n[4].x*s[4].z - n[4].z*s[4].x, n[4].y*s[4].x - n[4].x*s[4].y, n[4].x, n[4].y, n[4].z,
		n[5].z*s[5].y - n[5].y*s[5].z, n[5].x*s[5].z - n[5].z*s[5].x, n[5].y*s[5].x - n[5].x*s[5].y, n[5].x, n[5].y, n[5].z);

	cv::Mat B = (cv::Mat_<float>(6, 1) <<
		n[0].x*d[0].x + n[0].y*d[0].y + n[0].z*d[0].z - n[0].x*s[0].x - n[0].y*s[0].y - n[0].z*s[0].z,
		n[1].x*d[1].x + n[1].y*d[1].y + n[1].z*d[1].z - n[1].x*s[1].x - n[1].y*s[1].y - n[1].z*s[1].z,
		n[2].x*d[2].x + n[2].y*d[2].y + n[2].z*d[2].z - n[2].x*s[2].x - n[2].y*s[2].y - n[2].z*s[2].z,
		n[3].x*d[3].x + n[3].y*d[3].y + n[3].z*d[3].z - n[3].x*s[3].x - n[3].y*s[3].y - n[3].z*s[3].z,
		n[4].x*d[4].x + n[4].y*d[4].y + n[4].z*d[4].z - n[4].x*s[4].x - n[4].y*s[4].y - n[4].z*s[4].z,
		n[5].x*d[5].x + n[5].y*d[5].y + n[5].z*d[5].z - n[5].x*s[5].x - n[5].y*s[5].y - n[5].z*s[5].z);
	cv::Mat inverseA;
	cv::invert(A, inverseA, cv::DECOMP_SVD);

	cv::Mat ICPresult = inverseA*B;

	std::cout << "result" << endl;
	for (int i = 0; i < 6; i++){
		for (int j = 0; j < 1; j++){
			std::cout << ICPresult.at<float>(i, j) << "  ";
		}
		std::cout << endl;
	}
}
int Map::predictPoint(){
	cv::Mat predictDescriptor = resultMap.getPredictDescriptor();
	vector<cv::KeyPoint> predictPoint = resultMap.getTempPredictPoint();
	//cout << "predictPoint size " << predictPoint.size();
	if (predictDescriptor.rows > 6){
		cv::Mat trackPointDescriptor = trackImage.getDescriptor();

		if (trackPointDescriptor.rows > 6){
			matcher->match(predictDescriptor, trackPointDescriptor, matches);
			vector<int> query_train_idx[2];
			/**********************************************************/
			vector<int> usingMatch = ransacMatchPoint(predictPoint, trackImage.getfastCorner(), matches, 10);
			cout << "오류찾기    " <<usingMatch.size() << endl;
			for (int i = 0; i < usingMatch.size(); i++){
				query_train_idx[0].push_back(matches[usingMatch[i]].queryIdx);
				query_train_idx[1].push_back(matches[usingMatch[i]].trainIdx);
			}
			/**************************************************************/
			
			//for (int i = 0; i < matches.size(); i++){
			//	if (matches[i].distance < 40.0f){
			//		//cout << matches[i].distance << endl;
			//		query_train_idx[0].push_back(matches[i].queryIdx);
			//		query_train_idx[1].push_back(matches[i].trainIdx);
			//	}
			//}
			if (usingMatch.size() > 6){
				vector<cv::Point3f> worldPoint = resultMap.getPredictPoint();
				vector<cv::Point3f> trackWorldPoint;
				vector<cv::Point2f> trackPoint;
				vector<cv::KeyPoint> trackKeyPoint = trackImage.getfastCorner();
				vector<int> worldPointIdx = resultMap.getPredictPointIdx();

				for (int i = 0; i < query_train_idx[1].size(); i++){
					trackWorldPoint.push_back(worldPoint[query_train_idx[0][i]]);
					trackPoint.push_back(trackKeyPoint[query_train_idx[1][i]].pt);
				}
				
				/***************essentialMat*************/
				cv::Mat predictPointMat = resultMap.getPredictPointMat().clone();
				cv::Mat rotateMat;
				cv::Rodrigues(resultR, rotateMat);
				predictPointMat = rotateMat * predictPointMat;
				predictPointMat.row(0) = predictPointMat.row(0) - resultT.at<float>(0, 0);
				predictPointMat.row(1) = predictPointMat.row(1) - resultT.at<float>(1, 0);
				predictPointMat.row(2) = predictPointMat.row(2) - resultT.at<float>(2, 0);

				predictPointMat = cameraMat * predictPointMat;

				predictPointMat.row(0) = predictPointMat.row(0) / predictPointMat.row(2);
				predictPointMat.row(1) = predictPointMat.row(1) / predictPointMat.row(2);
				predictPointMat.row(2) = predictPointMat.row(2) / predictPointMat.row(2);

				float* predictPointMatPointer = (float*)predictPointMat.data;

				vector<cv::Point2f> calculateEssential[2];
				for (int i = 0; i < query_train_idx[1].size(); i++){
					calculateEssential[0].push_back(cv::Point2f(predictPointMatPointer[0 * predictPointMat.cols + query_train_idx[0][i]],
						predictPointMatPointer[1 * predictPointMat.cols + query_train_idx[0][i]]));
					calculateEssential[1].push_back(trackKeyPoint[query_train_idx[1][i]].pt);
				}
				cv::Mat mask;
				cv::Mat essential = cv::findEssentialMat(calculateEssential[0], calculateEssential[1], 589.291, cv::Point2d(302.741, 182.349), cv::RANSAC, 0.999, 1.0, mask);

				//cout << essential << endl;
				//cout << mask << endl;
				//cout << (int)mask.at<char>(0, 0) << endl;
				//cout << mask.cols << "  " << mask.rows << endl;

				vector<cv::Point3f> trackWorldPointCopy;
				vector<cv::Point2f> trackPointCopy;
				trackWorldPointCopy.assign(trackWorldPoint.begin(), trackWorldPoint.end());
				trackPointCopy.assign(trackPoint.begin(), trackPoint.end());
				trackWorldPoint.clear();
				trackPoint.clear();
				for (int i = 0; i < mask.rows; i++){
					if ((int)mask.at<char>(i, 0)==1){
						trackWorldPoint.push_back(trackWorldPointCopy[i]);
						trackPoint.push_back(trackPointCopy[i]);
					}
				}
				/************************************************************/
				calculateCamPoseSolvePNP(trackWorldPoint, trackPoint);

				cv::Mat tolerantR = prevR - resultR;				//tracking 시 solvePnP의 오류를 대략적으로 걸러낸다.
				cv::Mat tolerantT = prevT - resultT;
				float tolerantRScore = 0.0f;
				float tolerantTScore = 0.0f;
				tolerantRScore = fabsf(tolerantR.at<float>(0, 0)) + fabsf(tolerantR.at<float>(1, 0)) + fabsf(tolerantR.at<float>(2, 0));
				tolerantTScore = fabsf(tolerantT.at<float>(0, 0) + tolerantT.at<float>(1, 0) + tolerantT.at<float>(2, 0));
				if (tolerantRScore > 0.3 || tolerantTScore > 100){
					resultR = prevR.clone();
					resultT = prevT.clone();
					if (tolerantRScore > 0.3){
						tracking3Frame = false;
						cout << "tracking이 잘 되지 않고 있음 R값이 너무 차이남" << endl;
					}
					else{
						tracking3Frame = false;
						cout << "tracking이 잘 되지 않고 있음 T값이 너무 차이남" << endl;
					}
					return 0;
				}
				//trackImage.setUsingPoint(query_train_idx[1], true);
				vector<int> trackPointIdx[2];		//0이 해당 idx 1이 이에 해당하는 world 인덱스
				for (int i = 0; i < query_train_idx[0].size(); i++){
					trackPointIdx[0].push_back(query_train_idx[1][i]);
					trackPointIdx[1].push_back(worldPointIdx[query_train_idx[0][i]]);
				}
				trackImage.setTrackCornerIdx(trackPointIdx[1], trackPointIdx[0], resultMap.getPointSize());
				trackImage.setAddPoint(query_train_idx[1]);
				//추가하려는 Point의 Descriptor을 체크하여 3D Point와 유사한 Point를 제거하자.

				//trackImage.setExtrincRT(resultR, resultT);
				resultMap.setRT(resultR, resultT);

				/***************************3d morphing*****************************/
				/*cv::Mat trackMat = trackImage.getTrackFastCornerDepthMat().clone();
				predictPointMat.release();
				predictPointMat = resultMap.getPredictPointMat().clone();
				
				cv::Mat trackMatCopy = cv::Mat(3, query_train_idx[1].size(), CV_32F);
				cv::Mat predictPointMatCopy = cv::Mat(3, query_train_idx[1].size(), CV_32F);

				float* trackMatPointer = (float*)trackMat.data;
				predictPointMatPointer = (float*)predictPointMat.data;

				float* trackMatCopyPointer = (float*)trackMatCopy.data;
				float* predictPointMatCopyPointer = (float*)predictPointMatCopy.data;

				for (int i = 0; i < query_train_idx[1].size(); i++){
					if (trackMatPointer[2 * trackMat.cols + query_train_idx[1][i]] != 32000.0f && trackMatPointer[2 * trackMat.cols + query_train_idx[1][i]] > 500){
						predictPointMatCopyPointer[0 * predictPointMatCopy.cols + i] = predictPointMatPointer[0 * predictPointMat.cols + query_train_idx[0][i]];
						predictPointMatCopyPointer[1 * predictPointMatCopy.cols + i] = predictPointMatPointer[1 * predictPointMat.cols + query_train_idx[0][i]];
						predictPointMatCopyPointer[2 * predictPointMatCopy.cols + i] = predictPointMatPointer[2 * predictPointMat.cols + query_train_idx[0][i]];

						trackMatCopyPointer[0 * trackMatCopy.cols + i] = trackMatPointer[0 * trackMat.cols + query_train_idx[1][i]];
						trackMatCopyPointer[1 * trackMatCopy.cols + i] = trackMatPointer[1 * trackMat.cols + query_train_idx[1][i]];
						trackMatCopyPointer[2 * trackMatCopy.cols + i] = trackMatPointer[2 * trackMat.cols + query_train_idx[1][i]];
					}
				}

				trackMatCopy = inverseCameraMat * trackMatCopy;
				cv::Mat invR, R;
				cv::Rodrigues(resultR, R);
				cv::invert(R, invR);
				trackMatCopy = invR * trackMatCopy;
				trackMatCopy.row(0) = trackMatCopy.row(0) + resultT.at<float>(0, 0);
				trackMatCopy.row(1) = trackMatCopy.row(1) + resultT.at<float>(1, 0);
				trackMatCopy.row(2) = trackMatCopy.row(2) + resultT.at<float>(2, 0);
				cv::Mat invTrackMat, refineR;
				cv::invert(trackMatCopy, invTrackMat, cv::DECOMP_SVD);
				refineR = predictPointMatCopy * invTrackMat;
				cout << refineR << endl; */
				/*****************************************/

				return trackImage.getAddPointDepthMat().cols;
			}
		}
		else{
			cout << "tracking Image 오류" << endl;
		}
	}
	cout << "point의 수가 부족합니다.predictPoint" << endl;
	cout << "descriptor로 새로 pose를 예측합니다." << endl;
	//--------------------------------여기서 순수히 descriptor을 이용하여 카메라 pose를 예측하자.
	loseCamPose();

	return 0;			//잘 매치된 수가 일정 이상이면 맵의 업데이트를 시행하자.
}
bool compareDistance(cv::DMatch &i, cv::DMatch &j){
	return i.distance < j.distance;
}
void Map::loseCamPose(){
	//match 의 Hamming distance 가 일정 이하인 포인트가 일정 갯수 이상이면 cam pose를 예측
	//Hamming distance가 일정 이하인 포인트가 일정 갯수 이하이면 cam pose를 잃은 것으로 출력.
	matcher->match(trackImage.getDescriptor(), resultMap.getResultDescriptor(), matches);
	sort(matches.begin(), matches.end(), compareDistance);
	/*for (int i = 0; i < matches.size(); i++){
		cout << "distance : " << matches[i].distance << "  query : " << matches[i].queryIdx << "  train : " << matches[i].trainIdx << endl;
	}*/

	int NumOfGoodMatch = 0;
	for (NumOfGoodMatch = 0; NumOfGoodMatch < matches.size() && matches[NumOfGoodMatch].distance < 50; NumOfGoodMatch++){
	}
	if (NumOfGoodMatch > 15){
		cout << "카메라 포즈 descriptor로 예측" << endl;
		vector<cv::Point3f> obj;
		vector<cv::Point2f> point;

		cv::Mat objMat = resultMap.getResultPoint();
		float* objMatPointer = (float*)objMat.data;
		vector<cv::KeyPoint> pointKeyPoint = trackImage.getfastCorner();
		vector<int> worldToConerIdx[2];

		for (int i = 0; i < NumOfGoodMatch; i++){
			obj.push_back(cv::Point3f(objMatPointer[0 * objMat.cols + matches[i].trainIdx],
								      objMatPointer[1 * objMat.cols + matches[i].trainIdx],
									  objMatPointer[2 * objMat.cols + matches[i].trainIdx]));
			point.push_back(pointKeyPoint[matches[i].queryIdx].pt);
			worldToConerIdx[0].push_back(matches[i].trainIdx);
			worldToConerIdx[1].push_back(matches[i].queryIdx);
		}
		trackImage.setTrackCornerIdx(worldToConerIdx[0], worldToConerIdx[1], NULL);
		calculateCamPoseSolvePNP(obj, point);
		cout << "R : " << resultR << " \nT : " << resultT << endl;
		resultMap.setRT(resultR, resultT);

	}
	else{
		cout << "losing cam pose" << endl;
	}
}
/*float Map::trackCamYaw(vector<cv::Point3f> inputInit, vector<cv::Point2f> inputLater, quat& q, cv::Point3f& t) {
	cv::Mat cp = cameraMat;
	float cx = cameraMat.at<float>(0, 2);
	float cy = cameraMat.at<float>(1, 2);
	float f = cameraMat.at<float>(0, 0);
	vector<cv::Point2f> p2d;
	p2d.resize(inputLater.size());
	for (int i = 0; i < inputLater.size(); i++) {
		p2d[i].x = (inputLater[i].x - cx) / f;
		p2d[i].y = (inputLater[i].y - cx) / f;
	}
	const uchar mask[3] = { 1, 1, 1 };
	cv::Point3f prm = cv::Point3f(0, t.y, t.z);
	cv::Point3f orgPose = prm;

	int count = p3d.size(), nerr = count * 2;
	TREAL *del = new TREAL[nerr], *J = new TREAL[nerr * 3];
	LevenMarquardt solver(3, nerr, 20);
	int ret = 1;
	double error;
	while (1) {
		error = computeErrorJacobeanY(p3d, q, prm, cp, p2d, orgPose, del, J);
		ret = solver.step(prm.p, J, del, error, mask);
		if (ret == 0) break;
	}
	q = exp(prm.x*YAXIS / 2)*q;
	t.y = prm.y; t.z = prm.z;
	delete del; delete J;
	return error;
}
*/