#include "ResultMap.h"
#include <fstream>
#define EXACT_TRACK_3FRAME 0
#define USE_3FRAME 0
ResultMap::ResultMap(){
	resultR = (cv::Mat_<float>(3, 1) << 0.0f, 0.0f, 0.0f);
	resultT = (cv::Mat_<float>(3, 1) << 0.0f, 0.0f, 0.0f);
#if BRISK_SIFT == 1
	matcher = new cv::BFMatcher(cv::NORM_HAMMING, true);
#endif
#if BRISK_SIFT == 0
	matcher = new cv::BFMatcher(cv::NORM_L2, true);
#endif
	for (int i = 0; i < 3; i++){
#if BRISK_SIFT == 0
		tracking3FrameDescriptor[i] = cv::Mat(1, 1, CV_32F);
#endif
#if BRISK_SIFT == 1
		tracking3FrameDescriptor[i] = cv::Mat(1, 1, CV_8U);
#endif
		
		tracking3FramePointDepth[i] = cv::Mat(1, 1, CV_32F);
		tracking3FrameRGB[i] = cv::Mat(1, 1, CV_32F);
	}
}
ResultMap::~ResultMap(){

}
void ResultMap::setProperty(cv::Mat camera, cv::Mat incamera){
	cameraMat = camera.clone();
	inverseCameraMat = incamera.clone();
}
void ResultMap::setRT(cv::Mat R, cv::Mat T){
	prevR = resultR.clone();
	prevT = resultT.clone();
	resultR = R.clone();
	resultT = T.clone();
}
void ResultMap::setPoint(cv::Mat Point, cv::Mat descriptor, vector<cv::Point3i> RGB){
	resultPoint = Point.clone();
	resultDescriptor = descriptor.clone();
	resultMultDescriptor.resize(descriptor.rows);
	for (int i = 0; i < descriptor.rows; i++){
		resultMultDescriptor[i].push_back(descriptor.row(i));
	}
	if (!resultRGB.empty())  {
		resultRGB.release();
		resultRGB = NULL;
	}
	resultRGB = cv::Mat(3, RGB.size(), CV_32F);
	float* resultRGBPointer = (float*)resultRGB.data;
	for (int i = 0; i < RGB.size(); i++){
		resultRGBPointer[RGB.size() * 0 + i] = (float)RGB[i].x;
		resultRGBPointer[RGB.size() * 1 + i] = (float)RGB[i].y;
		resultRGBPointer[RGB.size() * 2 + i] = (float)RGB[i].z;
	}
}
bool ResultMap::setPredictPoint(){
	predictPointIdx.clear();
	tempPredictPointVector.clear();
	cv::Mat predictRMat;
	cv::Mat tempPredictPoint;
	
	cv::Rodrigues(resultR, predictRMat);

	tempPredictPoint = resultPoint.clone();
	tempPredictPoint = predictRMat * tempPredictPoint;
	tempPredictPoint.row(0) = tempPredictPoint.row(0) - resultT.at<float>(0, 0);
	tempPredictPoint.row(1) = tempPredictPoint.row(1) - resultT.at<float>(1, 0);
	tempPredictPoint.row(2) = tempPredictPoint.row(2) - resultT.at<float>(2, 0);

	tempPredictPoint = cameraMat * tempPredictPoint;

	tempPredictPoint.row(0) = tempPredictPoint.row(0) / tempPredictPoint.row(2);
	tempPredictPoint.row(1) = tempPredictPoint.row(1) / tempPredictPoint.row(2);
	tempPredictPoint.row(2) = tempPredictPoint.row(2) / tempPredictPoint.row(2);
	
	//cv::Mat blackImage = cv::imread("images/black.jpg");
	
	float* tempPredictPointPointer = (float*)tempPredictPoint.data;
	float* resultRGBPointer = (float*)resultRGB.data;
	for (int i = 0; i < tempPredictPoint.cols; i++){
		if (tempPredictPointPointer[tempPredictPoint.cols * 0 + i] < (640+50)	//가로세로 50pixel만큼 더 체크하기로.
			&& tempPredictPointPointer[tempPredictPoint.cols * 0 + i] > (0-50)
			&& tempPredictPointPointer[tempPredictPoint.cols * 1 + i] < (480+50)
			&& tempPredictPointPointer[tempPredictPoint.cols * 1 + i] > (0-50)){
			predictPointIdx.push_back(i);
			/*cv::circle(blackImage, cv::Point2f(
				tempPredictPointPointer[tempPredictPoint.cols * 0 + i]+50,
				tempPredictPointPointer[tempPredictPoint.cols * 1 + i]+50),
				3.0,
				cv::Scalar(resultRGBPointer[resultRGB.cols * 0 + i],
				resultRGBPointer[resultRGB.cols * 1 + i],
				resultRGBPointer[resultRGB.cols * 2 + i]), 3);*/
			tempPredictPointVector.push_back(cv::KeyPoint(cv::Point2f(tempPredictPointPointer[tempPredictPoint.cols * 0 + i],
				tempPredictPointPointer[tempPredictPoint.cols * 1 + i]), 0.0));
		}
	}
	if (predictPointIdx.size() > 6){
		predictPoint = cv::Mat(3, predictPointIdx.size(), CV_32F);
#if BRISK_SIFT == 0
		predictDescriptor = cv::Mat(predictPointIdx.size(), DES_SIZE, CV_32F);
		float* predictDescriptorPointer = (float*)predictDescriptor.data;
		float* resultDescriptorPointer = (float*)resultDescriptor.data;
#endif
#if BRISK_SIFT == 1
		predictDescriptor = cv::Mat(predictPointIdx.size(), DES_SIZE, CV_8U);
		uchar* predictDescriptorPointer = (uchar*)predictDescriptor.data;
		uchar* resultDescriptorPointer = (uchar*)resultDescriptor.data;
#endif
		
		predictRGB = cv::Mat(3, predictPointIdx.size(), CV_32F);
		float* predictPointPointer = (float*)predictPoint.data;
		float* predictRGBPointer = (float*)predictRGB.data;
		float* resultPointPointer = (float*)resultPoint.data;
		predictMultDescriptor.clear();
		predictMultDescriptor.resize(predictPointIdx.size());
		for (int i = 0; i < predictPointIdx.size(); i++){
			predictPointPointer[predictPointIdx.size() * 0 + i] = resultPointPointer[tempPredictPoint.cols * 0 + predictPointIdx[i]];
			predictPointPointer[predictPointIdx.size() * 1 + i] = resultPointPointer[tempPredictPoint.cols * 1 + predictPointIdx[i]];
			predictPointPointer[predictPointIdx.size() * 2 + i] = resultPointPointer[tempPredictPoint.cols * 2 + predictPointIdx[i]];
			for (int j = 0; j < DES_SIZE; j++){
				predictDescriptorPointer[DES_SIZE * i + j] = resultDescriptorPointer[DES_SIZE * predictPointIdx[i] + j];
			}
			predictMultDescriptor[i].push_back(resultPointPointer[predictPointIdx[i]]);
		}
	}
	else{
		predictDescriptor.empty();
		cout << "point부족" << endl;

		return false;
	}
	//cv::imshow("test", blackImage);
	return true;
}
void ResultMap::setAddPoint(cv::Mat addPointDepth, vector<cv::Point2f> addPoint, vector<cv::Point3i> addRGB, cv::Mat addDescriptor, bool tracking3FrameBool){
	cv::Mat R, invR, addPointDepthCopy;
	cv::Rodrigues(resultR, R);
	cv::invert(R, invR);
	addPointDepthCopy = addPointDepth.clone();
	addPointDepthCopy = inverseCameraMat * addPointDepthCopy;
	addPointDepthCopy = invR*addPointDepthCopy;
	addPointDepthCopy.row(0) = addPointDepthCopy.row(0) + resultT.at<float>(0, 0);
	addPointDepthCopy.row(1) = addPointDepthCopy.row(1) + resultT.at<float>(1, 0);
	addPointDepthCopy.row(2) = addPointDepthCopy.row(2) + resultT.at<float>(2, 0);
	pointIdx.clear();
	addPointIdx.clear();

	vector<cv::DMatch> dMatch;
	matcher->match(addDescriptor, resultDescriptor, dMatch);
#if USE_3FRAME == 1	
	for (int i = 0; i < addPointDepthCopy.cols; i++){
		if (checkPoint(resultPoint, addPointDepthCopy, i)){
			if (checkDescriptor(dMatch, i, 100.0f)){
				pointIdx.push_back(i);
			}
		}
	}
	if (pointIdx.size()>0){
#if BRISK_SIFT == 0
		tracking3FrameDescriptor[0] = cv::Mat(pointIdx.size(), DES_SIZE, CV_32F);
		float* trackingDescriptorPointer = (float*)tracking3FrameDescriptor[0].data;
		float* addDescriptorPointer = (float*)addDescriptor.data;
#endif
#if BRISK_SIFT == 1
		tracking3FrameDescriptor[0] = cv::Mat(pointIdx.size(), DES_SIZE, CV_8U);
		uchar* trackingDescriptorPointer = (uchar*)tracking3FrameDescriptor[0].data;
		uchar* addDescriptorPointer = (uchar*)addDescriptor.data;
#endif
		
		tracking3FramePointDepth[0] = cv::Mat(3, pointIdx.size(), CV_32F);
		tracking3FrameRGB[0] = cv::Mat(3, pointIdx.size(), CV_32F);

		float* trackingRGBPointer = (float*)tracking3FrameRGB[0].data;
		float* trackingPointPointer = (float*)tracking3FramePointDepth[0].data;
		

		float* addPointPointer = (float*)addPointDepthCopy.data;
		

		for (int i = 0; i < pointIdx.size(); i++){
			tracking3FramePoint[0].push_back(addPoint[pointIdx[i]]);

			trackingRGBPointer[0 * pointIdx.size() + i] = addRGB[pointIdx[i]].x;
			trackingRGBPointer[1 * pointIdx.size() + i] = addRGB[pointIdx[i]].y;
			trackingRGBPointer[2 * pointIdx.size() + i] = addRGB[pointIdx[i]].z;

			trackingPointPointer[0 * pointIdx.size() + i] = addPointPointer[0 * addPointDepthCopy.cols + pointIdx[i]];
			trackingPointPointer[1 * pointIdx.size() + i] = addPointPointer[1 * addPointDepthCopy.cols + pointIdx[i]];
			trackingPointPointer[2 * pointIdx.size() + i] = addPointPointer[2 * addPointDepthCopy.cols + pointIdx[i]];

			for (int j = 0; j < DES_SIZE; j++){
				trackingDescriptorPointer[DES_SIZE * i + j] = addDescriptorPointer[DES_SIZE * pointIdx[i] + j];
			}
		}
	}

	tracking3Frame(tracking3FrameBool);
#endif

#if USE_3FRAME == 0
	cv::Mat RGB = cv::Mat(3, 1, CV_32F);
	float* RGBPointer = (float*)RGB.data;

	for (int i = 0; i < addPointDepthCopy.cols; i++){
		if (checkPoint(resultPoint, addPointDepthCopy, i)){
			if (checkDescriptor(dMatch, i)){
				addPointIdx.push_back(i);
				RGBPointer[0] = addRGB[i].x;
				RGBPointer[1] = addRGB[i].y;
				RGBPointer[2] = addRGB[i].z;
				cv::hconcat(resultRGB, RGB, resultRGB);
				cv::hconcat(resultPoint, addPointDepthCopy.col(i), resultPoint);
				resultDescriptor.push_back(addDescriptor.row(i));
			}
			/*else{
				cout << "fail to add point descriptor" << endl;
			}*/
		}
	}
	//cout << "addPointSize      " << addPointIdx.size() <<  " , " << addPointIdx.size()<< endl;
	//cout << resultRGB.cols << "    " << resultPoint.cols << endl;
#endif
}
void ResultMap::setSBAData(cv::Mat sbaData){
	sbaData.copyTo(resultPoint);
}
void ResultMap::tracking3Frame(bool tracking3FrameBool){
	if (tracking3FrameBool&&tracking3FramePoint[0].size() > 0){
		if (tracking3FramePoint[1].size()>0){
			if (tracking3FramePoint[2].size()>0){
				//3Frame Tracking Process.............
				vector<cv::DMatch> zeroToOne;
				vector<int> zeroToOneIdx;
				vector<cv::DMatch> oneToTwo;
				vector<int> oneToTwoIdx;
				addPointIdx.clear();
				//float tempX, tempY = 0.0f;
				//float* trackingPointOnePointer = tracking3FramePoint[1].data;
				//float* trackintPointTwoPointer = tracking
				//cout << tracking3FramePoint[0].size() << endl;
				matcher->match(tracking3FrameDescriptor[0], tracking3FrameDescriptor[1], zeroToOne);
				matcher->match(tracking3FrameDescriptor[1], tracking3FrameDescriptor[2], oneToTwo);
#if EXACT_TRACK_3FRAME == 1
				zeroToOneIdx = matchPoint(tracking3FramePoint[0], tracking3FramePoint[1], zeroToOne);
				oneToTwoIdx = matchPoint(tracking3FramePoint[1], tracking3FramePoint[2], oneToTwo);
#endif
#if EXACT_TRACK_3FRAME == 0
				for (int i = 0; i < zeroToOne.size(); i++){
					zeroToOneIdx.push_back(i);
				}
				for (int i = 0; i < oneToTwo.size(); i++){
					oneToTwoIdx.push_back(i);
				}
#endif
				
				for (int i = 0; i < zeroToOneIdx.size(); i++){
					for (int j = 0; j < oneToTwoIdx.size(); j++){
						if (zeroToOne[zeroToOneIdx[i]].trainIdx == oneToTwo[oneToTwoIdx[j]].queryIdx){
							addPointIdx.push_back(zeroToOne[zeroToOneIdx[i]].queryIdx);
						}
					}
				}

				for (int i = 0; i < addPointIdx.size(); i++){
					cv::hconcat(resultRGB, tracking3FrameRGB[0].col(addPointIdx[i]), resultRGB);
					cv::hconcat(resultPoint, tracking3FramePointDepth[0].col(addPointIdx[i]), resultPoint);
					resultDescriptor.push_back(tracking3FrameDescriptor[0].row(addPointIdx[i]));
				}

				tracking3FrameDescriptor[2] = tracking3FrameDescriptor[1].clone();
				tracking3FramePointDepth[2] = tracking3FramePointDepth[1].clone();
				tracking3FrameRGB[2] = tracking3FrameRGB[1].clone();
				tracking3FramePoint[2].clear();
				for (int i = 0; i < tracking3FramePoint[1].size(); i++){
					tracking3FramePoint[2].push_back(tracking3FramePoint[1][i]);
				}
				
				tracking3FrameDescriptor[1] = tracking3FrameDescriptor[0].clone();
				tracking3FramePointDepth[1] = tracking3FramePointDepth[0].clone();
				tracking3FrameRGB[1] = tracking3FrameRGB[0].clone();
				tracking3FramePoint[1].clear();
				for (int i = 0; i < tracking3FramePoint[0].size(); i++){
					tracking3FramePoint[1].push_back(tracking3FramePoint[0][i]);
				}
				tracking3FramePoint[0].clear();
			}
			else{
				tracking3FrameDescriptor[2] = tracking3FrameDescriptor[1].clone();
				tracking3FramePointDepth[2] = tracking3FramePointDepth[1].clone();
				tracking3FrameRGB[2] = tracking3FrameRGB[1].clone();
				tracking3FramePoint[2].clear();
				for (int i = 0; i < tracking3FramePoint[1].size(); i++){
					tracking3FramePoint[2].push_back(tracking3FramePoint[1][i]);
				}

				tracking3FrameDescriptor[1] = tracking3FrameDescriptor[0].clone();
				tracking3FramePointDepth[1] = tracking3FramePointDepth[0].clone();
				tracking3FrameRGB[1] = tracking3FrameRGB[0].clone();
				tracking3FramePoint[1].clear();
				for (int i = 0; i < tracking3FramePoint[0].size(); i++){
					tracking3FramePoint[1].push_back(tracking3FramePoint[0][i]);
				}
				tracking3FramePoint[0].clear();
			}
		}
		else{
			tracking3FrameDescriptor[1] = tracking3FrameDescriptor[0].clone();
			tracking3FramePointDepth[1] = tracking3FramePointDepth[0].clone();
			tracking3FrameRGB[1] = tracking3FrameRGB[0].clone();
			tracking3FramePoint[1].clear();
			for (int i = 0; i < tracking3FramePoint[0].size(); i++){
				tracking3FramePoint[1].push_back(tracking3FramePoint[0][i]);
			}
			tracking3FramePoint[0].clear();
			tracking3FramePoint[2].clear();
		}
	}
	else{
		tracking3FrameDescriptor[1] = tracking3FrameDescriptor[0].clone();
		tracking3FramePointDepth[1] = tracking3FramePointDepth[0].clone();
		tracking3FrameRGB[1] = tracking3FrameRGB[0].clone();
		tracking3FramePoint[1].clear();
		for (int i = 0; i < tracking3FramePoint[0].size(); i++){
			tracking3FramePoint[1].push_back(tracking3FramePoint[0][i]);
		}
		tracking3FramePoint[0].clear();
		tracking3FramePoint[2].clear();
	}
}
void ResultMap::refineMap(cv::Mat trackPoint, vector<int> trackPointIdx){
	int num = 0;
	vector<int> usingIdx;
	cv::Mat trackPointCopy = trackPoint.clone();
	float* trackPointPointer = (float*)trackPointCopy.data;

	int resize = (trackPointIdx[trackPointIdx.size()-1] < resultPoint.cols) ? resultPoint.cols : trackPointIdx[trackPointIdx.size()-1];
	if (resize > resultPointTrackScore.size()){
		resultPointTrackScore.resize(4*resize, 1);
	}
	for (int i = 0; i < trackPointIdx.size(); i++){
		resultPointTrackScore[trackPointIdx[i]]++;
		if (trackPointIdx[num] < resultPoint.cols){
			num++;
		}
	}
	for (int i = 0; i < num; i++){
		if (trackPointPointer[3 * trackPoint.cols + i] != 32000.f && trackPointPointer[3 * trackPoint.cols + i] > 500){
			usingIdx.push_back(i);
			
		}
	}
	if (usingIdx.size() > 0){
		cout << "refineMap" << endl;
		cv::Mat rMat, invR;
		float* resultPointer = (float*)resultPoint.data;
		cv::Rodrigues(resultR, rMat);
		cv::invert(rMat, invR);
		trackPointCopy = inverseCameraMat * trackPointCopy;
		trackPointCopy = invR * trackPointCopy;
		trackPointCopy.row(0) = trackPointCopy.row(0) + resultT.at<float>(0, 0);
		trackPointCopy.row(1) = trackPointCopy.row(1) + resultT.at<float>(1, 0);
		trackPointCopy.row(2) = trackPointCopy.row(2) + resultT.at<float>(2, 0);

		float x, y, z = 0.0f;

		for (int i = 0; i < usingIdx.size(); i++){
			x = trackPointPointer[0 * trackPointCopy.cols + usingIdx[i]] - resultPointer[0 * resultPoint.cols + trackPointIdx[usingIdx[i]]];
			y = trackPointPointer[1 * trackPointCopy.cols + usingIdx[i]] - resultPointer[1 * resultPoint.cols + trackPointIdx[usingIdx[i]]];
			z = trackPointPointer[2 * trackPointCopy.cols + usingIdx[i]] - resultPointer[2 * resultPoint.cols + trackPointIdx[usingIdx[i]]];
			//cout << "x : " << x << ", y : " << y << ", z : " << z << endl;
			resultPointer[0 * resultPoint.cols + trackPointIdx[usingIdx[i]]] += x / resultPointTrackScore[trackPointIdx[usingIdx[i]]];
			resultPointer[1 * resultPoint.cols + trackPointIdx[usingIdx[i]]] += y / resultPointTrackScore[trackPointIdx[usingIdx[i]]];
			resultPointer[2 * resultPoint.cols + trackPointIdx[usingIdx[i]]] += z / resultPointTrackScore[trackPointIdx[usingIdx[i]]];
		}
	}
}
void ResultMap::printMap(bool print){
	if (print == true){
		ofstream output;
		output.open("outputMap.txt");
		output << "#3Dpoint" << endl;
		float* resultPointPointer = (float*)resultPoint.data;
		for (int i = 0; i < resultPoint.cols; i ++){
			output << resultPointPointer[resultPoint.cols * 0 + i] << " " << resultPointPointer[resultPoint.cols * 1 + i] << " " << resultPointPointer[resultPoint.cols * 2 + i] << endl;
		}
#if BRISK_SIFT == 0
		float* resultDescriptorPointer = (float*)resultDescriptor.data;
#endif
#if BRISK_SIFT == 1
		uchar* resultDescriptorPointer = (uchar*)resultDescriptor.data;
#endif	
		output << "#Descriptor" << endl;
		for (int i = 0; i < resultDescriptor.rows; i++){
			for (int j = 0; j < DES_SIZE; j++){
				output << (int)resultDescriptorPointer[DES_SIZE*i + j] << " ";
			}
			output << endl;
		}
		output.close();
	}
};
cv::Mat ResultMap::getPredictDescriptor(){
	return predictDescriptor;
}
cv::Mat ResultMap::getResultPoint(){
	return resultPoint;
}
cv::Mat ResultMap::getResultRGB(){
	return resultRGB;
}
cv::Mat ResultMap::getResultDescriptor(){
	return resultDescriptor;
}
cv::Mat ResultMap::getPredictPointMat(){
	return predictPoint;
}
vector<cv::Point3f> ResultMap::getPredictPoint(){
	float* predictPointPointer = (float*)predictPoint.data;
	vector<cv::Point3f> returnPoint;
	for (int i = 0; i < predictPoint.cols; i++){
		returnPoint.push_back(cv::Point3f(
			predictPointPointer[predictPoint.cols * 0 + i],
			predictPointPointer[predictPoint.cols * 1 + i],
			predictPointPointer[predictPoint.cols * 2 + i]));
	}
	return returnPoint;
}
vector<cv::KeyPoint> ResultMap::getTempPredictPoint(){
	return tempPredictPointVector;
}
vector<int> ResultMap::getPredictPointIdx(){
	return predictPointIdx;
}
vector<int> ResultMap::getAddPointIdx(){
	return addPointIdx;
}
vector<int> ResultMap::getPointIdx(){
#if USE_3FRAME == 1
	vector<int> temp;
	for (int i = 0; i < addPointIdx.size(); i++){
		temp.push_back(pointIdx[addPointIdx[i]]);
	}
	return temp;
#endif
#if USE_3FRAME == 0
	return addPointIdx;
#endif
}
vector<int> ResultMap::getTrackingScore(){
	return resultPointTrackScore;
}
int ResultMap::getPointSize(){
	return resultPoint.cols;
}
void ResultMap::clear(){
	resultPointTrackScore.clear();
	maxScore = 0;
}
bool ResultMap::checkPoint(cv::Mat pointArray, cv::Mat checkPoint, int checkPointNum, float threshold){
	float addPointX = 0.0f, addPointY = 0.0f, addPointZ = 0.0f;
	float* pointArrayPointer = (float*)pointArray.data;
	addPointX = checkPoint.at<float>(0, checkPointNum);
	addPointY = checkPoint.at<float>(1, checkPointNum);
	addPointZ = checkPoint.at<float>(2, checkPointNum);
	for (int j = 0; j < pointArray.cols; j++){
		if (fabsf(addPointX - pointArrayPointer[0 * pointArray.cols + j]) < threshold
			&& fabsf(addPointY - pointArrayPointer[1 * pointArray.cols + j]) < threshold
			&& fabsf(addPointZ - pointArrayPointer[2 * pointArray.cols + j]) < threshold){	//xyz 포인트가 존재하는 포인트의 200mm안으로 들어오면 추가안함.
			return false;
		}
	}
	return true;
}
bool ResultMap::checkDescriptor(vector<cv::DMatch> match, int i, float threshold){
	for (int k = 0; k < match.size(); k++){
		if (match[k].queryIdx == i){
			return match[k].distance > threshold;
		}
	}
	return false;
}
vector<int> ResultMap::matchPoint(vector<cv::Point2f> pointA, vector<cv::Point2f> pointB, vector<cv::DMatch> match){
	vector <int> usingMatch1;
	vector <int> ran6Point;
	vector <float> distance;

	//int num_of_point = 6;

	float meanRansac = 0.0f;
	tempMean = 60.0f;
	float threshold2 = 0.0f;
	int count = 0;
	int tempCount = 0;
	float tempX, tempY = 0.0f;
	vector <float> distance2;
	for (int k = 0; k < match.size(); k++){
		tempX = pointA[match[k].queryIdx].x - pointB[match[k].trainIdx].x;
		tempY = pointA[match[k].queryIdx].y - pointB[match[k].trainIdx].y;
		tempMean = sqrtf(tempX*tempX + tempY*tempY);
		//cout << "tempMean : " << tempMean << endl;
		//cout << "distance : " << match[k].distance << endl;
		tempMean += match[k].distance;
		distance.push_back(tempMean);
		distance2.push_back(tempMean);
	}
	sort(distance.begin(), distance.end());
	cout << "tracking 3Point distance size " << distance2.size() << endl;
	if (distance2.size() > 0){
		cout << "matchPoint tempMean : " << tempMean << endl;
		for (int k = 0; k < distance2.size(); k++){
			if (distance2[k] < tempMean){
				usingMatch1.push_back(k);
			}
		}
	}
	return usingMatch1;
}

float vectorNorm(cv::Point3f vector){
	return sqrtf(vector.x*vector.x + vector.y*vector.y + vector.z*vector.z);
}
