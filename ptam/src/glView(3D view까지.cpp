#include "glView.h"

glView::glView(){
	// Initial position : on +Z
	position = glm::vec3(0, 0, 300);
	// Initial horizontal angle : toward -Z
	horizontalAngle = 3.14f;
	// Initial vertical angle : none
	verticalAngle = 0.0f;
	// Initial Field of View
	initialFoV = 45.0f;

	speed = 3.0f; // 3 units / second

	positionSpeed = 30.0f;

	if (!glfwInit()){
		cout << stderr << "Failed to initialize GLFW" << endl;
	}
	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL 

	// Open a window and create its OpenGL context 

	window = glfwCreateWindow(1024, 768, "glView", NULL, NULL);
	if (window == NULL){
		cout << stderr << "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials." << endl;
		glfwTerminate();
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW 
	glewExperimental = true; // Needed in core profile 
	if (glewInit() != GLEW_OK) {
		cout << stderr << "Failed to initialize GLEW\n" << endl;
	}
	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetCursorPos(window, 1024 / 2, 768 / 2);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Cull triangles which normal is not towards the camera
	//glEnable(GL_CULL_FACE);

	VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	programID = LoadShaders("TransformVertexShader.vertexshader", "ColorFragmentShader.fragmentshader");

	// Get a handle for our "MVP" uniform
	MatrixID = glGetUniformLocation(programID, "MVP");

	TextureID = glGetUniformLocation(programID, "myTextureSampler");

	//// Our vertices. Tree consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
	//// A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
	//static const GLfloat g_vertex_buffer_data[] = {
	//	-1.0f, -1.0f, -1.0f,
	//	-1.0f, -1.0f, 1.0f,
	//	-1.0f, 1.0f, 1.0f,
	//	1.0f, 1.0f, -1.0f,
	//	-1.0f, -1.0f, -1.0f,
	//	-1.0f, 1.0f, -1.0f,
	//	1.0f, -1.0f, 1.0f,
	//	-1.0f, -1.0f, -1.0f,
	//	1.0f, -1.0f, -1.0f,
	//	1.0f, 1.0f, -1.0f,
	//	1.0f, -1.0f, -1.0f,
	//	-1.0f, -1.0f, -1.0f,
	//	-1.0f, -1.0f, -1.0f,
	//	-1.0f, 1.0f, 1.0f,
	//	-1.0f, 1.0f, -1.0f,
	//	1.0f, -1.0f, 1.0f,
	//	-1.0f, -1.0f, 1.0f,
	//	-1.0f, -1.0f, -1.0f,
	//	-1.0f, 1.0f, 1.0f,
	//	-1.0f, -1.0f, 1.0f,
	//	1.0f, -1.0f, 1.0f,
	//	1.0f, 1.0f, 1.0f,
	//	1.0f, -1.0f, -1.0f,
	//	1.0f, 1.0f, -1.0f,
	//	1.0f, -1.0f, -1.0f,
	//	1.0f, 1.0f, 1.0f,
	//	1.0f, -1.0f, 1.0f,
	//	1.0f, 1.0f, 1.0f,
	//	1.0f, 1.0f, -1.0f,
	//	-1.0f, 1.0f, -1.0f,
	//	1.0f, 1.0f, 1.0f,
	//	-1.0f, 1.0f, -1.0f,
	//	-1.0f, 1.0f, 1.0f,
	//	1.0f, 1.0f, 1.0f,
	//	-1.0f, 1.0f, 1.0f,
	//	1.0f, -1.0f, 1.0f
	//};

	//// One color for each vertex. They were generated randomly.
	//static const GLfloat g_color_buffer_data[] = {
	//	0.583f, 0.771f, 0.014f,
	//	0.609f, 0.115f, 0.436f,
	//	0.327f, 0.483f, 0.844f,
	//	0.822f, 0.569f, 0.201f,
	//	0.435f, 0.602f, 0.223f,
	//	0.310f, 0.747f, 0.185f,
	//	0.597f, 0.770f, 0.761f,
	//	0.559f, 0.436f, 0.730f,
	//	0.359f, 0.583f, 0.152f,
	//	0.483f, 0.596f, 0.789f,
	//	0.559f, 0.861f, 0.639f,
	//	0.195f, 0.548f, 0.859f,
	//	0.014f, 0.184f, 0.576f,
	//	0.771f, 0.328f, 0.970f,
	//	0.406f, 0.615f, 0.116f,
	//	0.676f, 0.977f, 0.133f,
	//	0.971f, 0.572f, 0.833f,
	//	0.140f, 0.616f, 0.489f,
	//	0.997f, 0.513f, 0.064f,
	//	0.945f, 0.719f, 0.592f,
	//	0.543f, 0.021f, 0.978f,
	//	0.279f, 0.317f, 0.505f,
	//	0.167f, 0.620f, 0.077f,
	//	0.347f, 0.857f, 0.137f,
	//	0.055f, 0.953f, 0.042f,
	//	0.714f, 0.505f, 0.345f,
	//	0.783f, 0.290f, 0.734f,
	//	0.722f, 0.645f, 0.174f,
	//	0.302f, 0.455f, 0.848f,
	//	0.225f, 0.587f, 0.040f,
	//	0.517f, 0.713f, 0.338f,
	//	0.053f, 0.959f, 0.120f,
	//	0.393f, 0.621f, 0.362f,
	//	0.673f, 0.211f, 0.457f,
	//	0.820f, 0.883f, 0.371f,
	//	0.982f, 0.099f, 0.879f
	//};
	//GLuint a, b;
	//vertexbuffer.push_back(a);
	//glGenBuffers(1, &vertexbuffer[0]);
	//glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer[0]);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

	//colorbuffer.push_back(b);
	//glGenBuffers(1, &colorbuffer[0]);
	//glBindBuffer(GL_ARRAY_BUFFER, colorbuffer[0]);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data), g_color_buffer_data, GL_STATIC_DRAW);

	///*GLfloat dd[] = {
	//	0.0f, 0.0f, 0.0f,
	//	2.0f, 2.0f, 2.0f,
	//	2.0f, 0.0f, 0.0f
	//};
	//GLfloat ddColor[] = {
	//	1.0f, 1.0f, 1.0f,
	//	1.0f, 1.0f, 1.0f,
	//	1.0f, 1.0f, 1.0f
	//};*/

	//vector<GLfloat> dd;
	//dd.push_back(0.0f);
	//dd.push_back(0.0f);
	//dd.push_back(0.0f);
	//dd.push_back(2.0f);
	//dd.push_back(2.0f);
	//dd.push_back(2.0f);
	//dd.push_back(2.0f);
	//dd.push_back(0.0f);
	//dd.push_back(0.0f);

	//vector<GLfloat> ddColor;
	//ddColor.push_back(1.0f);
	//ddColor.push_back(1.0f);
	//ddColor.push_back(1.0f);
	//ddColor.push_back(1.0f);
	//ddColor.push_back(1.0f);
	//ddColor.push_back(1.0f);
	//ddColor.push_back(1.0f);
	//ddColor.push_back(1.0f);
	//ddColor.push_back(1.0f);

	//GLuint c, d;
	//vertexbuffer.push_back(c);
	//glGenBuffers(1, &vertexbuffer[1]);
	//glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer[1]);
	//glBufferData(GL_ARRAY_BUFFER, 4 * 3 * 3, dd.data(), GL_STATIC_DRAW);

	//colorbuffer.push_back(d);
	//glGenBuffers(1, &colorbuffer[1]);
	//glBindBuffer(GL_ARRAY_BUFFER, colorbuffer[1]);
	//glBufferData(GL_ARRAY_BUFFER, 4 * 3 * 3, ddColor.data(), GL_STATIC_DRAW);

}
glView::~glView(){
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &colorbuffer);
	glDeleteVertexArrays(1, &VertexArrayID);
	glDeleteProgram(programID);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();
}
void glView::run(){
	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use our shader
	glUseProgram(programID);

	// Compute the MVP matrix from keyboard and mouse input
	computeMatricesFromInputs();
	glm::mat4 ProjectionMatrix = getProjectionMatrix();
	glm::mat4 ViewMatrix = getViewMatrix();
	glm::mat4 ModelMatrix = glm::mat4(1.0);
	glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

	// Send our transformation to the currently bound shader, 
	// in the "MVP" uniform
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

	/*--------------------------snez3DvertexDataDraw------------------------------------------------*/
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
		);

	// 2nd attribute buffer : colors
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glVertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);

	/*glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, depthbuffer);
	glVertexAttribPointer(
		2,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		2,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);
	*/


	// Draw the triangle !
	glDrawArrays(GL_POINTS, 0, 320*240); // 12*3 indices starting at 0 -> 12 triangles

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	//glDisableVertexAttribArray(2);

	glPointSize(5);

	// Swap buffers
	glfwSwapBuffers(window);
	glfwPollEvents();


}
void glView::setVertex(cv::Mat cvVertex, cv::Mat colorImage, cv::Mat extrinsicMatR, cv::Mat extrinsicMatT, cv::Mat calib){
	vector<GLfloat> vertex;
	vector<GLfloat> color;
	texture = *cvVertex.data;
	float z, u, v = 0;

	for (int i = 0; i < 240; i++){
		for (int j = 0; j < 320; j++){
			if ((float)cvVertex.at<cv::Vec3s>(i, j)[2] < 32000.0f){
				vertex.push_back((float)cvVertex.at<cv::Vec3s>(i, j)[0]);
				vertex.push_back((float)cvVertex.at<cv::Vec3s>(i, j)[1]);
				vertex.push_back(-(float)cvVertex.at<cv::Vec3s>(i, j)[2]);

				z = extrinsicMatR.at<float>(2, 0)*(float)cvVertex.at<cv::Vec3s>(i, j)[0] + extrinsicMatR.at<float>(2, 1)*(float)cvVertex.at<cv::Vec3s>(i, j)[1] + extrinsicMatR.at<float>(2, 2)*(float)cvVertex.at<cv::Vec3s>(i, j)[2];
				u = ((calib.at<float>(0, 0)*extrinsicMatR.at<float>(0, 0) + calib.at<float>(0, 2)*extrinsicMatR.at<float>(2, 0))*(float)cvVertex.at<cv::Vec3s>(i, j)[0] + (calib.at<float>(0, 0)*extrinsicMatR.at<float>(0, 1) + calib.at<float>(0, 2)*extrinsicMatR.at<float>(2, 1))*(float)cvVertex.at<cv::Vec3s>(i, j)[1] + (calib.at<float>(0, 0)*extrinsicMatR.at<float>(0, 2) + calib.at<float>(0, 2)*extrinsicMatR.at<float>(2, 2))*(float)cvVertex.at<cv::Vec3s>(i, j)[2] + (calib.at<float>(0, 0)*extrinsicMatT.at<float>(0, 0) + calib.at<float>(0, 2)*extrinsicMatT.at<float>(2, 0))) / z;
				v = ((calib.at<float>(1, 1)*extrinsicMatR.at<float>(1, 0) + calib.at<float>(1, 2)*extrinsicMatR.at<float>(2, 0))*(float)cvVertex.at<cv::Vec3s>(i, j)[0] + (calib.at<float>(1, 1)*extrinsicMatR.at<float>(1, 1) + calib.at<float>(1, 2)*extrinsicMatR.at<float>(2, 1))*(float)cvVertex.at<cv::Vec3s>(i, j)[1] + (calib.at<float>(1, 1)*extrinsicMatR.at<float>(1, 2) + calib.at<float>(1, 2)*extrinsicMatR.at<float>(2, 2))*(float)cvVertex.at<cv::Vec3s>(i, j)[2] + (calib.at<float>(1, 1)*extrinsicMatT.at<float>(0, 0) + calib.at<float>(1, 2)*extrinsicMatT.at<float>(2, 0))) / z;

				if (u > 0 && v > 0 && u < 640 && v < 480){
					color.push_back(colorImage.at<cv::Vec3b>(v, u)[2] / 255.0f);
					color.push_back(colorImage.at<cv::Vec3b>(v, u)[1] / 255.0f);
					color.push_back(colorImage.at<cv::Vec3b>(v, u)[0] / 255.0f);
				}
				else{
					color.push_back(0.0f);
					color.push_back(0.0f);
					color.push_back(0.0f);
				}
			}
		}
	}
	
	GLuint a, b;
	
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertex.size() * 4, vertex.data(), GL_STATIC_DRAW);
	
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, color.size() * 4, color.data(), GL_STATIC_DRAW);

	//glGenBuffers(1, &depthbuffer);
	//glBindBuffer(GL_ARRAY_BUFFER, depthbuffer);

}
void glView::computeMatricesFromInputs(){
	// glfwGetTime is called only once, the first time this function is called
	static double lastTime = glfwGetTime();

	// Compute time difference between current and last frame
	double currentTime = glfwGetTime();
	float deltaTime = float(currentTime - lastTime);

	// Get mouse position
	//double xpos, ypos;
	//glfwGetCursorPos(window, &xpos, &ypos);

	// Reset mouse position for next frame
	//glfwSetCursorPos(window, 1024/2, 768/2);

	// Compute new orientation
	//horizontalAngle += mouseSpeed * float(1024/2 - xpos );
	//verticalAngle   += mouseSpeed * float( 768/2 - ypos );

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS){
		verticalAngle += speed / 2 * deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS){
		verticalAngle -= speed / 2 * deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS){
		horizontalAngle += speed / 2 * deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS){
		horizontalAngle -= speed / 2 * deltaTime;
	}
	// Direction : Spherical coordinates to Cartesian coordinates conversion
	glm::vec3 direction(
		cos(verticalAngle) * sin(horizontalAngle),
		sin(verticalAngle),
		cos(verticalAngle) * cos(horizontalAngle)
		);

	// Right vector
	glm::vec3 right = glm::vec3(
		sin(horizontalAngle - 3.14f / 2.0f),
		0,
		cos(horizontalAngle - 3.14f / 2.0f)
		);

	// Up vector
	glm::vec3 up = glm::cross(right, direction);

	// Move forward
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS){
		position += direction * deltaTime * positionSpeed;
	}
	// Move backward
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS){
		position -= direction * deltaTime * positionSpeed;
	}
	// Strafe right
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS){
		position += right * deltaTime * positionSpeed;
	}
	// Strafe left
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS){
		position -= right * deltaTime * positionSpeed;
	}

	float FoV = initialFoV;// - 5 * glfwGetMouseWheel(); // Now GLFW 3 requires setting up a callback for this. It's a bit too complicated for this beginner's tutorial, so it's disabled instead.

	// Projection matrix : 45?Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	ProjectionMatrix = glm::perspective(FoV, 4.0f / 3.0f, 0.1f, 10000.0f);
	// Camera matrix
	ViewMatrix = glm::lookAt(
		position,           // Camera is here
		position + direction, // and looks here : at the same position, plus "direction"
		up                  // Head is up (set to 0,-1,0 to look upside-down)
		);

	// For the next frame, the "last time" will be "now"
	lastTime = currentTime;
}
glm::mat4 glView::getViewMatrix(){
	return ViewMatrix;
}
glm::mat4 glView::getProjectionMatrix(){
	return ProjectionMatrix;
}