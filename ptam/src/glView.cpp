#include "glView.h"

glView::glView(){
	// Initial position : on +Z
	position = glm::vec3(0, 0, -300);
	// Initial horizontal angle : toward -Z
	horizontalAngle = 0.0f;
	// Initial vertical angle : none
	verticalAngle = 0.0f;
	// Initial Field of View
	initialFoV = 45.0f;

	speed = 3.0f; // 3 units / second

	positionSpeed = 30.0f;

	numOfVertex = 0;

	if (!glfwInit()){
		cout << stderr << "Failed to initialize GLFW" << endl;
	}
	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL 

	// Open a window and create its OpenGL context 

	window = glfwCreateWindow(1024, 768, "glView", NULL, NULL);
	if (window == NULL){
		cout << stderr << "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials." << endl;
		glfwTerminate();
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW 
	glewExperimental = true; // Needed in core profile 
	if (glewInit() != GLEW_OK) {
		cout << stderr << "Failed to initialize GLEW\n" << endl;
	}
	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetCursorPos(window, 1024 / 2, 768 / 2);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Cull triangles which normal is not towards the camera
	//glEnable(GL_CULL_FACE);

	VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	programID = LoadShaders("TransformVertexShader.vertexshader", "ColorFragmentShader.fragmentshader");

	// Get a handle for our "MVP" uniform
	MatrixID = glGetUniformLocation(programID, "MVP");

	//TextureID = glGetUniformLocation(programID, "myTextureSampler");
	setGrid();
	setCamera();
}
glView::~glView(){
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &colorbuffer);
	glDeleteBuffers(1, &cameraVertexbuffer);
	glDeleteBuffers(1, &cameraColorbuffer);
	glDeleteBuffers(1, &gridVertexbuffer);
	glDeleteBuffers(1, &gridColorbuffer);
	glDeleteVertexArrays(1, &VertexArrayID);
	glDeleteProgram(programID);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();
}
void glView::run(){
	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use our shader
	glUseProgram(programID);

	// Compute the MVP matrix from keyboard and mouse input
	computeMatricesFromInputs();
	glm::mat4 ProjectionMatrix = getProjectionMatrix();
	glm::mat4 ViewMatrix = getViewMatrix();
	glm::mat4 ModelMatrix = glm::mat4(1.0);
	ModelMatrix = glm::scale(ModelMatrix, glm::vec3(1.0f, 1.0f, 1.0f));
	glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

	// Send our transformation to the currently bound shader, 
	// in the "MVP" uniform
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

	/*--------------------------snez3DvertexDataDraw------------------------------------------------*/
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_SHORT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
		);

	// 2nd attribute buffer : colors
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glVertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);

	/*glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, depthbuffer);
	glVertexAttribPointer(
		2,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		2,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);
	*/


	// Draw the triangle !
	glDrawArrays(GL_POINTS, 0, numOfVertex); // 12*3 indices starting at 0 -> 12 triangles

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	//glDisableVertexAttribArray(2);

	glPointSize(5);

	// Swap buffers
	glfwSwapBuffers(window);
	glfwPollEvents();
}
void glView::runMapping(){
	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use our shader
	glUseProgram(programID);

	// Compute the MVP matrix from keyboard and mouse input
	computeMatricesFromInputs();
	glm::mat4 ProjectionMatrix = getProjectionMatrix();
	glm::mat4 ViewMatrix = getViewMatrix();
	glm::mat4 ModelMatrix = glm::mat4(1.0);
	ModelMatrix = glm::scale(ModelMatrix, glm::vec3(1.0f, 1.0f, 1.0f));
	glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

	// Send our transformation to the currently bound shader, 
	// in the "MVP" uniform
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

	/*--------------------------snez3DvertexDataDraw------------------------------------------------*/

	//drawCamera
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, cameraVertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
		);

	// 2nd attribute buffer : colors
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, cameraColorbuffer);
	glVertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);
	glDrawArrays(GL_LINES, 0, 6);

	//drawGrid
	glBindBuffer(GL_ARRAY_BUFFER, gridVertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
		);

	glBindBuffer(GL_ARRAY_BUFFER, gridColorbuffer);
	glVertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);

	glDrawArrays(GL_LINES, 0, 400);

	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
		);

	// 2nd attribute buffer : colors

	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glVertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);
	glDrawArrays(GL_POINTS, 0, numOfVertex); // 12*3 indices starting at 0 -> 12 triangles
	glPointSize(5);

	

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);



	// Swap buffers
	glfwSwapBuffers(window);
	glfwPollEvents();
}
void glView::setVertexDepthSense(const DepthSense::Vertex* senzImage){
	numOfVertex = 320 * 240;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, 320 * 240 * 3 * 2, senzImage, GL_STATIC_DRAW);
	for (int i = 0; i < 20; i++){
		cout << senzImage[i].z << endl;
	}
}
void glView::calDepthToColor(const DepthSense::Vertex* senzImage, cv::Mat colorImage, float* extrinsicR, float* extrinsicT, float* calibArray, cv::Mat* outputDepthImage){
	cv::Mat opencvImage = cv::Mat::zeros(480, 640, CV_32F);
	float z, u, v = 0;
	for (int i = 0; i < 240; i++){
		for (int j = 0; j < 320; j++){
			if (senzImage[i * 320 + j].z < 32000.0f){
				z = extrinsicR[2 * 3 + 0] * (float)senzImage[320 * i + j].x + extrinsicR[2 * 3 + 1] * (float)senzImage[320 * i + j].y + extrinsicR[2 * 3 + 2] * (float)senzImage[320 * i + j].z + extrinsicT[2];
				u = ((calibArray[0 * 3 + 0] * extrinsicR[0 * 3 + 0] + calibArray[0 * 3 + 2] * extrinsicR[2 * 3 + 0])*(float)senzImage[320 * i + j].x + (calibArray[0 * 3 + 0] * extrinsicR[0 * 3 + 1] + calibArray[0 * 3 + 2] * extrinsicR[2 * 3 + 1])*(float)senzImage[320 * i + j].y + (calibArray[0 * 3 + 0] * extrinsicR[0 * 3 + 2] + calibArray[0 * 3 + 2] * extrinsicR[2 * 3 + 2])*(float)senzImage[320 * i + j].z + (calibArray[0 * 3 + 0] * extrinsicT[0] + calibArray[0 * 3 + 2] * extrinsicT[2])) / z;
				v = ((calibArray[1 * 3 + 1] * extrinsicR[1 * 3 + 0] + calibArray[1 * 3 + 2] * extrinsicR[2 * 3 + 0])*(float)senzImage[320 * i + j].x + (calibArray[1 * 3 + 1] * extrinsicR[1 * 3 + 1] + calibArray[1 * 3 + 2] * extrinsicR[2 * 3 + 1])*(float)senzImage[320 * i + j].y + (calibArray[1 * 3 + 1] * extrinsicR[1 * 3 + 2] + calibArray[1 * 3 + 2] * extrinsicR[2 * 3 + 2])*(float)senzImage[320 * i + j].z + (calibArray[1 * 3 + 1] * extrinsicT[1] + calibArray[1 * 3 + 2] * extrinsicT[2])) / z;

				if (u > 0 && v > 0 && u < 640 && v < 480){
					color[3 * (i * 320 + j)] = colorImage.at<cv::Vec3b>(v, u)[2] / 255.0f;
					color[3 * (i * 320 + j) + 1] = colorImage.at<cv::Vec3b>(v, u)[1] / 255.0f;
					color[3 * (i * 320 + j) + 2] = colorImage.at<cv::Vec3b>(v, u)[0] / 255.0f;

					opencvImage.at<float>(v, u) = 1.0f - (float)senzImage[320 * i + j].z / 3000.f;		//calibration 된 depth image만들기
				}
				else{
					color[3 * (i * 320 + j)] = 0.0f;
					color[3 * (i * 320 + j) + 1] = 0.0f;
					color[3 * (i * 320 + j) + 2] = 0.0f;
				}
			}
			else{
				color[3 * (i * 320 + j)] = 0.0f;
				color[3 * (i * 320 + j) + 1] = 0.0f;
				color[3 * (i * 320 + j) + 2] = 0.0f;
			}
		}
	}
	opencvImage.copyTo(*outputDepthImage);
}
void glView::setVertexFastFeature(cv::Mat feature, cv::Mat cameraExtrincR, cv::Mat cameraExtrincT, cv::Mat RGB){		//float 로 바꾸고 feature Num 도 넣어줘야됌 그릴때
	/*int featureNum = feature.cols;
	cv::Mat vertex = cv::Mat(featureNum, 3, CV_32F);
	float* featurePointer = (float*)feature.data;
	float* vertexPointer = (float*)vertex.data;

	for (int i = 0; i < featureNum; i++){
		vertexPointer[3 * i + 0] = featurePointer[0 * featureNum + i];
		vertexPointer[3 * i + 1] = featurePointer[1 * featureNum + i];
		vertexPointer[3 * i + 2] = featurePointer[2 * featureNum + i];
	}*/
	
	cv::Mat vertex;
	cv::Mat copyFeature = feature.clone();

	float* copyFeaturePointer = (float*)copyFeature.data;
	float* featurePointer = (float*)feature.data;

	for (int i = 0; i < 2*feature.cols; i++){
		copyFeaturePointer[i] = -featurePointer[i];
	}
	for (int i = 2 * feature.cols; i < 3 * feature.cols; i++){
		copyFeaturePointer[i] = featurePointer[i];
	}

	//copyFeature.row(0) = -copyFeature.row(0);
	//copyFeature.row(1) = -copyFeature.row(1);
	cv::transpose(copyFeature, vertex);

	numOfVertex = feature.cols;
	
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, numOfVertex * 3 * 4, vertex.data, GL_STATIC_DRAW);

	for (int i = 0; i < numOfVertex; i++){
		color[i * 3 + 0] = RGB.at<float>(2, i) / 255;
		color[i * 3 + 1] = RGB.at<float>(1, i) / 255;
		color[i * 3 + 2] = RGB.at<float>(0, i) / 255;
	}
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, numOfVertex * 3 * 4, color, GL_STATIC_DRAW);
	setCamera();

	drawCamera(cameraExtrincR, cameraExtrincT);


}
void glView::setColor(const DepthSense::Vertex* senzImage, cv::Mat colorImage, cv::Mat extrinsicMatR, cv::Mat extrinsicMatT, cv::Mat calib){
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, 320 * 240 * 3 * 4, color, GL_STATIC_DRAW);
}
void glView::setCamera(){
	cameraVertex = cv::Mat(3, 6, CV_32F);		//x는 빨강 y는 파랑 z는 초록
	float* cameraVertexPointer = (float*)cameraVertex.data;
	cameraVertexPointer[6 * 0 + 0] = 0;
	cameraVertexPointer[6 * 1 + 0] = 0;
	cameraVertexPointer[6 * 2 + 0] = 0;
	cameraVertexPointer[6 * 0 + 1] = -10;
	cameraVertexPointer[6 * 1 + 1] = 0;
	cameraVertexPointer[6 * 2 + 1] = 0;
	cameraVertexPointer[6 * 0 + 2] = 0;
	cameraVertexPointer[6 * 1 + 2] = 0;
	cameraVertexPointer[6 * 2 + 2] = 0;
	cameraVertexPointer[6 * 0 + 3] = 0;
	cameraVertexPointer[6 * 1 + 3] = 10;
	cameraVertexPointer[6 * 2 + 3] = 0;
	cameraVertexPointer[6 * 0 + 4] = 0;
	cameraVertexPointer[6 * 1 + 4] = 0;
	cameraVertexPointer[6 * 2 + 4] = 0;
	cameraVertexPointer[6 * 0 + 5] = 0;
	cameraVertexPointer[6 * 1 + 5] = 0;
	cameraVertexPointer[6 * 2 + 5] = 10;
	cameraColor[0] = 1;
	cameraColor[1] = 0;
	cameraColor[2] = 0;
	cameraColor[3] = 1;
	cameraColor[4] = 0;
	cameraColor[5] = 0;
	cameraColor[6] = 0;
	cameraColor[7] = 0;
	cameraColor[8] = 1;
	cameraColor[9] = 0;
	cameraColor[10] = 0;
	cameraColor[11] = 1;
	cameraColor[12] = 0;
	cameraColor[13] = 1;
	cameraColor[14] = 0;
	cameraColor[15] = 0;
	cameraColor[16] = 1;
	cameraColor[17] = 0;
}
void glView::drawCamera(cv::Mat cameraExtrincR, cv::Mat cameraExtrincT){		
	cv::Mat tempExtricR, tempExtircT, R, RInv, temp, temp2, camera;
	tempExtricR = cv::Mat(3, 1, CV_32F);
	tempExtricR.at<float>(0, 0) = cameraExtrincR.at<float>(0, 0);
	tempExtricR.at<float>(1, 0) = cameraExtrincR.at<float>(2, 0);
	tempExtricR.at<float>(2, 0) = cameraExtrincR.at<float>(1, 0);
	cv::Rodrigues(cameraExtrincR, R);
	
	RInv = R.inv();

	tempExtircT = cv::Mat(3, 1, CV_32F);
	tempExtircT = RInv * cameraExtrincT;
	
	temp = cameraVertex;
	//cout << "temp" <<temp << endl;
	temp.row(0) = cameraVertex.row(0) + tempExtircT.at<float>(0, 0);
	temp.row(2) = cameraVertex.row(2) + tempExtircT.at<float>(2, 0);
	temp.row(1) = cameraVertex.row(1) - tempExtircT.at<float>(1, 0);
	//cout << temp << endl;
	//cout << RInv << endl;
	//cout << temp << endl;

	temp2 = R * temp;

	cv::transpose(temp2, camera);

	//cout << camera << endl;

	glGenBuffers(1, &cameraVertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, cameraVertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, 18 * 4, camera.data, GL_STATIC_DRAW);
	glGenBuffers(1, &cameraColorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, cameraColorbuffer);
	glBufferData(GL_ARRAY_BUFFER, 18 * 4, cameraColor, GL_STATIC_DRAW);
}
void glView::setGrid(){			
	int k = 0;
	for (int j = 0; j < 200; j++){
		gridVertex[j * 3 + 0] = k;
		gridVertex[j * 3 + 1] = 0;
		gridVertex[j * 3 + 2] = 0;

		gridColor[j * 3 + 0] = 0.2;
		gridColor[j * 3 + 1] = 0.2;
		gridColor[j * 3 + 2] = 0.2;
		j++;

		gridVertex[j * 3 + 0] = k;
		gridVertex[j * 3 + 1] = 0;
		gridVertex[j * 3 + 2] = 1000;

		gridColor[j * 3 + 0] = 0.2;
		gridColor[j * 3 + 1] = 0.2;
		gridColor[j * 3 + 2] = 0.2;
		k += 10;
	}
	k = 0;
	for (int j = 0; j < 200; j++){
		gridVertex[600 + j * 3 + 0] = 0;
		gridVertex[600 + j * 3 + 1] = 0;
		gridVertex[600 + j * 3 + 2] = k;

		gridColor[600 + j * 3 + 0] = 0.2;
		gridColor[600 + j * 3 + 1] = 0.2;
		gridColor[600 + j * 3 + 2] = 0.2;
		j++;	  


		gridVertex[600 + j * 3 + 0] = 1000;
		gridVertex[600 + j * 3 + 1] = 0;
		gridVertex[600 + j * 3 + 2] = k;

		gridColor[600 + j * 3 + 0] = 0.2;
		gridColor[600 + j * 3 + 1] = 0.2;
		gridColor[600 + j * 3 + 2] = 0.2;
		k += 10;
	}
	glGenBuffers(1, &gridVertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, gridVertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, 1200 * 4, gridVertex, GL_STATIC_DRAW);
	glGenBuffers(1, &gridColorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, gridColorbuffer);
	glBufferData(GL_ARRAY_BUFFER, 1200 * 4, gridColor, GL_STATIC_DRAW);

}
void glView::computeMatricesFromInputs(){
	// glfwGetTime is called only once, the first time this function is called
	static double lastTime = glfwGetTime();

	// Compute time difference between current and last frame
	double currentTime = glfwGetTime();
	float deltaTime = float(currentTime - lastTime);

	// Get mouse position
	//double xpos, ypos;
	//glfwGetCursorPos(window, &xpos, &ypos);

	// Reset mouse position for next frame
	//glfwSetCursorPos(window, 1024/2, 768/2);

	// Compute new orientation
	//horizontalAngle += mouseSpeed * float(1024/2 - xpos );
	//verticalAngle   += mouseSpeed * float( 768/2 - ypos );

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS){
		verticalAngle += speed / 2 * deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS){
		verticalAngle -= speed / 2 * deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS){
		horizontalAngle += speed / 2 * deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS){
		horizontalAngle -= speed / 2 * deltaTime;
	}
	// Direction : Spherical coordinates to Cartesian coordinates conversion
	glm::vec3 direction(
		cos(verticalAngle) * sin(horizontalAngle),
		sin(verticalAngle),
		cos(verticalAngle) * cos(horizontalAngle)
		);

	// Right vector
	glm::vec3 right = glm::vec3(
		sin(horizontalAngle - 3.14f / 2.0f),
		0,
		cos(horizontalAngle - 3.14f / 2.0f)
		);

	// Up vector
	glm::vec3 up = glm::cross(right, direction);

	// Move forward
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS){
		position += direction * deltaTime * positionSpeed;
	}
	// Move backward
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS){
		position -= direction * deltaTime * positionSpeed;
	}
	// Strafe right
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS){
		position += right * deltaTime * positionSpeed;
	}
	// Strafe left
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS){
		position -= right * deltaTime * positionSpeed;
	}

	float FoV = initialFoV;// - 5 * glfwGetMouseWheel(); // Now GLFW 3 requires setting up a callback for this. It's a bit too complicated for this beginner's tutorial, so it's disabled instead.

	// Projection matrix : 45?Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	ProjectionMatrix = glm::perspective(FoV, 4.0f / 3.0f, 0.1f, 10000.0f);
	// Camera matrix
	ViewMatrix = glm::lookAt(
		position,           // Camera is here
		position + direction, // and looks here : at the same position, plus "direction"
		up                  // Head is up (set to 0,-1,0 to look upside-down)
		);
	// For the next frame, the "last time" will be "now"
	lastTime = currentTime;
}
glm::mat4 glView::getViewMatrix(){
	return ViewMatrix;
}
glm::mat4 glView::getProjectionMatrix(){
	return ProjectionMatrix;
}