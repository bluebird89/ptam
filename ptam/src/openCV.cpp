#include "stdafx.h"

#include <iostream>
#include <fstream>
#include <Windows.h>
#include <NuiApi.h>

#include "Map.h"

using namespace std;
#if !WEBCAM
RGBQUAD m_rgbWk[640 * 480];

RGBQUAD Nui_ShortToQuad_Depth(USHORT s)
{
	USHORT realDepth = (s & 0xfff8) >> 3;
	//플레이어 비트 정보가 필요할 경우 : USHORT Player = s & 7;
	BYTE l = 255 - (BYTE)(256 * realDepth / (0x0fff));

	RGBQUAD q;
	q.rgbRed = q.rgbBlue = q.rgbGreen = l;
	return q;
}
#endif

int main(){
	cv::Mat inputImage;
	cv::Mat grayImage;
	cv::VideoCapture cap(0);
	int keyboardInput;
	bool nextFrame = false;
#if !WEBCAM
	cv::Mat DepthMat;
	cv::Mat transDepth;
	cv::Mat RgbMat;
	HANDLE depthStreamHandle;
	HANDLE nextDepthFrameEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	HANDLE rgbStreamHandle;
	NuiInitialize(NUI_INITIALIZE_FLAG_USES_DEPTH | NUI_INITIALIZE_FLAG_USES_COLOR);
	NuiImageStreamOpen(NUI_IMAGE_TYPE_COLOR, NUI_IMAGE_RESOLUTION_640x480, 0, 2, NULL, &rgbStreamHandle);
	IplImage* rgbImage = cvCreateImage(cvSize(640, 480), IPL_DEPTH_8U, 4);
	NuiImageStreamOpen(NUI_IMAGE_TYPE_DEPTH, NUI_IMAGE_RESOLUTION_640x480, 0, 2, NULL, &depthStreamHandle);
	IplImage* DepthImage = cvCreateImage(cvSize(640, 480), IPL_DEPTH_8U, 4);

	cv::Mat kinectRgb = *(cv::Mat_<float>(3, 3) <<
		518.4f, 0.0f, 312.9f,
		0.0f, 518.4f, 263.1f,
		0.0f, 0.0f, 1.0f);
	cv::Mat inkinectRgb;
	cv::invert(kinectRgb, inkinectRgb);
	cv::Mat kinectRgbDst = *(cv::Mat_<float>(1, 5) <<
		0.1745f, -0.5226f, -0.00057f, -0.003668f, 0.3588f);
	cv::Mat kinectDepth = *(cv::Mat_<float>(3, 3) <<
		580.4f, 0.0f, 313.4f,
		0.0f, 579.9f, 245.0f,
		0.0f, 0.0f, 1.0f);
	cv::Mat inkinectDepth;
	cv::invert(kinectDepth, inkinectDepth);
	cv::Mat kinectDepthDst = *(cv::Mat_<float>(1, 5) <<
		-0.1964f, 0.8216f, 0.0000005487f, -0.004413f, -1.1663f);
	cv::Mat calibR = *(cv::Mat_<float>(3, 3) <<
		1.0f, -0.02f, 0.014f,
		0.02f, 1.0f, -0.01f,
		-0.011f, 0.011f, 1.0f);
	cv::Mat calibT = *(cv::Mat_<float>(3, 1) <<
		0.0253f,
		0.00037f,
		-0.00252f);

#endif
	Map map;

#if WEBCAM
	if (!cap.isOpened()){
		cout << "video not opened" << endl;
		return -1;
	}
#endif
	while (1){
		keyboardInput = cv::waitKey(33);	//keyboard input 받아서 init map 시작함.
#if !WEBCAM
		const NUI_IMAGE_FRAME *pImageFrame = NULL;											//kinect로부터 Depth 이미지를 받음
		HRESULT hr = NuiImageStreamGetNextFrame(depthStreamHandle, 1000, &pImageFrame);
		//NuiImageStreamSetImageFrameFlags(depthStreamHandle, NUI_IMAGE_STREAM_FLAG_ENABLE_NEAR_MODE);	//kinect near mode enable
		if (FAILED(hr)){
			printf("Failed to get a Next Frame(Kinect Image)\n");
		}
		INuiFrameTexture *pTexture = pImageFrame->pFrameTexture;
		NUI_LOCKED_RECT LockedRect;
		pTexture->LockRect(0, &LockedRect, NULL, 0);
		if (LockedRect.Pitch != 0)
		{
			BYTE * pBuffer = (BYTE*)LockedRect.pBits;
			RGBQUAD * rgbrun = m_rgbWk;
			USHORT * pBufferRun = (USHORT*)pBuffer;

			for (int y = 0; y < 480; y++)
			{
				for (int x = 0; x < 640; x++)
				{
					RGBQUAD quad = Nui_ShortToQuad_Depth(*pBufferRun);
					pBufferRun++;
					*rgbrun = quad;
					rgbrun++;
				}
			}
			cvSetData(DepthImage, (BYTE*)m_rgbWk, DepthImage->widthStep);
			DepthMat = cv::Mat(DepthImage);
		}
		hr = NuiImageStreamReleaseFrame(depthStreamHandle, pImageFrame); // 이건 한 루프당 반드시 릴리즈 시켜줘야 에러가 안난다.

		const NUI_IMAGE_FRAME *rgbImageFrame = NULL;									//kinect로부터 Rgb 이미지를 받음.
		HRESULT rgbhr = NuiImageStreamGetNextFrame(rgbStreamHandle, 1000, &rgbImageFrame);
		if (FAILED(rgbhr)){
			printf("Failed to get a Next Frame(RGB Kinect Image)\n");
			continue;
		}
		INuiFrameTexture *rgbTexture = rgbImageFrame->pFrameTexture;
		NUI_LOCKED_RECT rgbLockedRect;
		rgbTexture->LockRect(0, &rgbLockedRect, NULL, 0);
		if (rgbLockedRect.Pitch != 0)
		{
			BYTE * pBuffer = (BYTE*)rgbLockedRect.pBits;
			USHORT * pBufferRun = (USHORT*)pBuffer;
			// Draw가 들어갈 부분 : csSetData, cvShowImage 로 구현하기.
			cvSetData(rgbImage, (BYTE*)pBuffer, rgbLockedRect.Pitch);
			inputImage = cv::Mat(rgbImage);
			
			//뎁스 + rgb 테스트					//Depth 위치 맞춘거
			transDepth = DepthMat;
			//transDepth = cv::Mat::zeros(DepthMat.size(), DepthMat.type());
			//DepthMat(cv::Rect(0, 3, DepthMat.cols, DepthMat.rows - 3)).copyTo(transDepth(cv::Rect(0, 0, DepthMat.cols, DepthMat.rows - 3)));
		}
		rgbhr = NuiImageStreamReleaseFrame(rgbStreamHandle, rgbImageFrame); // 이건 한 루프당 반드시 릴리즈 시켜줘야 에러가 안난다.
#endif
#if WEBCAM
		cap >> inputImage;
#endif
		//!WEBCAM 일경우 실제로 사용할 정보는 transDepth와 RgbMat이다 RgbMat은 inputImage로 변환했음

		cv::cvtColor(inputImage, grayImage, CV_RGB2GRAY);

		if ((char)keyboardInput == ' '&&nextFrame ==false){
			cout << "init Map" << endl;
			keyboardInput = NULL;
#if !WEBCAM
			map.initMap(grayImage, transDepth);
#endif
#if WEBCAM
			map.initMap(grayImage);
#endif
			nextFrame = true;


		}
		else if (nextFrame == true){
#if !WEBCAM
			map.initMap(grayImage, transDepth);
#endif
#if WEBCAM
			map.initMap(grayImage);
#endif
			cv::Mat inputImage2;
			cv::Mat newOne;
			cv::Mat newTwo;
			cv::undistort(inputImage, inputImage2, kinectRgb, kinectRgbDst, newOne);
			cv::Mat transDepth2;
			cv::undistort(transDepth, transDepth2, kinectDepth, kinectDepthDst, newTwo);
			cout << "one" << endl;
			for (int i = 0; i < 3; i++){
				for (int j = 0; j < 3; j++){
					cout << newOne.at<uchar>(i, j) << endl;
				
				}
			}
			cout << "two" << endl;
			for (int i = 0; i < 3; i++){
				for (int j = 0; j < 3; j++){
					
					cout << newTwo.at<uchar>(i, j) << endl;
				}
			}

			cv::imshow("dsfsfd", inputImage2 + transDepth2);
			if (map.getTrack() == true){
				vector<cv::Point2f> drawLine0 = map.getSuccessFeature(0, 0);
				vector<cv::Point2f> drawLine1 = map.getSuccessFeature(1, 0);
				for (int i = 0; i < drawLine0.size(); i++){
					cv::line(inputImage, drawLine0.at(i), drawLine1.at(i), cv::Scalar(255, 0, 0), 2);
					/*float x = inkinectRgb.at<float>(0, 0) * drawLine0.at(i).x + inkinectRgb.at<float>(0, 1) * drawLine0.at(i).y + inkinectRgb.at<float>(0, 2) * 1;
					float y = inkinectRgb.at<float>(1, 0) * drawLine0.at(i).x + inkinectRgb.at<float>(1, 1) * drawLine0.at(i).y + inkinectRgb.at<float>(1, 2) * 1;
					float z = inkinectRgb.at<float>(2, 0) * drawLine0.at(i).x + inkinectRgb.at<float>(2, 1) * drawLine0.at(i).y + inkinectRgb.at<float>(2, 2) * 1;
					
					float xx = calibR.at<float>(0, 0) * x + calibR.at<float>(0, 1) * y + calibR.at<float>(0, 2) * z + calibT.at<float>(0, 0);
					float yy = calibR.at<float>(1, 0) * x + calibR.at<float>(1, 1) * y + calibR.at<float>(1, 2) * z + calibT.at<float>(1, 0);
					float zz = calibR.at<float>(2, 0) * x + calibR.at<float>(2, 1) * y + calibR.at<float>(2, 2) * z + calibT.at<float>(1, 0);

					float xxx = kinectDepth.at<float>(0, 0) * xx + kinectDepth.at<float>(0, 1) * yy + kinectDepth.at<float>(0, 2) * zz;
					float yyy = kinectDepth.at<float>(1, 0) * xx + kinectDepth.at<float>(1, 1) * yy + kinectDepth.at<float>(1, 2) * zz;
					float zzz = kinectDepth.at<float>(2, 0) * xx + kinectDepth.at<float>(2, 1) * yy + kinectDepth.at<float>(2, 2) * zz;

					float x2 = inkinectRgb.at<float>(0, 0) * drawLine1.at(i).x + inkinectRgb.at<float>(0, 1) * drawLine1.at(i).y + inkinectRgb.at<float>(0, 2) * 1;
					float y2 = inkinectRgb.at<float>(1, 0) * drawLine1.at(i).x + inkinectRgb.at<float>(1, 1) * drawLine1.at(i).y + inkinectRgb.at<float>(1, 2) * 1;
					float z2 = inkinectRgb.at<float>(2, 0) * drawLine1.at(i).x + inkinectRgb.at<float>(2, 1) * drawLine1.at(i).y + inkinectRgb.at<float>(2, 2) * 1;

					float xx2 = calibR.at<float>(0, 0) * x + calibR.at<float>(0, 1) * y + calibR.at<float>(0, 2) * z + calibT.at<float>(0, 0);
					float yy2 = calibR.at<float>(1, 0) * x + calibR.at<float>(1, 1) * y + calibR.at<float>(1, 2) * z + calibT.at<float>(1, 0);
					float zz2 = calibR.at<float>(2, 0) * x + calibR.at<float>(2, 1) * y + calibR.at<float>(2, 2) * z + calibT.at<float>(1, 0);

					float xxx2 = kinectDepth.at<float>(0, 0) * xx + kinectDepth.at<float>(0, 1) * yy + kinectDepth.at<float>(0, 2) * zz;
					float yyy2 = kinectDepth.at<float>(1, 0) * xx + kinectDepth.at<float>(1, 1) * yy + kinectDepth.at<float>(1, 2) * zz;
					float zzz2 = kinectDepth.at<float>(2, 0) * xx + kinectDepth.at<float>(2, 1) * yy + kinectDepth.at<float>(2, 2) * zz;
					cout << zzz2 << endl;
					cv::circle(transDepth, cv::Point2f(xxx2, yyy2), 2, cv::Scalar(255, 0, 0));
					//cv::line(transDepth, cv::Point2f(xxx, yyy), cv::Point2f(xxx2,yyy2), cv::Scalar(255, 0, 0), 2);*/
				}

				if ((char)keyboardInput == ' '){
					cout << "generate map" << endl;
					map.genMap();
					//nextFrame = false;
				}
				map.clearSuccessFeature();
			}
			
			cout << map.getMatchNum() <<endl;
			if (map.getMatchNum() < 30){
				nextFrame = false;
			}
		}
		
		cv::imshow("test", inputImage);
	}
	return 0;
}

/*
void main(){
	IplImage* image = 0;
	CvCapture* capture = cvCaptureFromCAM(0);
	cvNamedWindow("T9-camera", 0);

	cvResizeWindow("T9-camera", 640, 480);

	while (1) {
		cvGrabFrame(capture);
		image = cvRetrieveFrame(capture);

		cvShowImage("T9-camera", image);

		if (cvWaitKey(10) >= 0)
			break;
	}


	cvReleaseCapture(&capture);
	cvDestroyWindow("T9-camera");
}*/




/*

using namespace std;


int main(){
	cv::Mat img;
	img = cv::Mat(4, 4, CV_32F);
	img.at<float>(0, 0) = 3;
	img.at<float>(0, 1) = -2;
	img.at<float>(0, 2) = -2;
	img.at<float>(0, 3) = 3;
	img.at<float>(1, 0) = 0;
	img.at<float>(1, 1) = 3;
	img.at<float>(1, 2) = -4;

	return 0;
}*/


